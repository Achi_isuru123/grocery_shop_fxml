/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package add_secton;

import DB.dbclass;
import DB.systemcomfigdata;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import tableview.ctegory_table_controller;

/**
 * FXML Controller class
 *
 * @author isuru
 */
public class CaegoryController implements Initializable {

    String List2;
    Date d = new Date();
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
    @FXML
    private JFXTextField catid;
    @FXML
    private JFXTextField catname;
    @FXML
    private JFXTextField subcatname;
    @FXML
    private JFXButton add;
    @FXML
    private JFXButton update;
    @FXML
    private JFXTextField search;
    @FXML
    private JFXTextField rackno;
    @FXML
    private JFXButton searchbutton;
    @FXML
    private JFXListView<String> subcatlist;
    @FXML
    private JFXButton addlist;
    @FXML
    private JFXTextField updatename;
    @FXML
    private JFXButton listupdate;
    @FXML
    private JFXButton cancel;
    @FXML
    private TableView<tableview.ctegory_table_controller> table6;
    @FXML
    private TableColumn<tableview.ctegory_table_controller, String> c_catid;
    @FXML
    private TableColumn<tableview.ctegory_table_controller, String> c_catname;
    @FXML
    private TableColumn<tableview.ctegory_table_controller, String> c_recname;
    @FXML
    private TableColumn<tableview.ctegory_table_controller, String> c_enterdate;
    @FXML
    private TableColumn<tableview.ctegory_table_controller, String> c_states;
    @FXML
    private TableColumn<tableview.ctegory_table_controller, String> c_empyname;
    
    ObservableList<tableview.ctegory_table_controller> TableList = FXCollections.observableArrayList();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ganaretid();
        subcatname.setVisible(false);
        addlist.setVisible(false);
        listupdate.setVisible(false);
        updatename.setVisible(false);
        cancel.setVisible(false);
        String rec = "RACK NO: ";
        rackno.setText(rec);
        LoadTable();
        SetCell();
    }

    @FXML
    private void btnadd(ActionEvent event) {
        try {
            add.setVisible(false);
            if (!catname.getText().trim().equals("") && !rackno.getText().trim().equals("")) {
                dbclass.push("insert into category values ('" + catid.getText().toUpperCase()
                        + "','" + catname.getText().toUpperCase()
                        + "','" + rackno.getText() + "','" + date.format(d)
                        + "','1','" + systemcomfigdata.getEmpid() + "')");
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Category add Information");
                alert.setHeaderText("Category added Success..!");
                alert.showAndWait();
                subcatname.setVisible(true);
                addlist.setVisible(true);
                listupdate.setVisible(true);
                updatename.setVisible(true);
                cancel.setVisible(true);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btnupdate(ActionEvent event) {
        categoryupdate();
    }

    @FXML
    private void btnsearch(ActionEvent event) {
        SEARCH();
    }

    @FXML
    private void btnaddlist(ActionEvent event) {
        try {
            dbclass.push("insert into add_subcategory (categoryid,subcategoryname,enterdate) values ('" + catid.getText().toUpperCase()
                    + "','" + subcatname.getText().toUpperCase() + "','" + date.format(d) + "')");
            subcatlist.getItems().add(subcatname.getText());
            subcatname.setText(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ganaretid() {
        try {
            ResultSet rs = dbclass.search("select count(*) as cat_id from category");
            if (rs.next()) {
                int Count = rs.getInt("cat_id");
                String id = "CATEGORY" + (++Count);
                catid.setText(id);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void editlist(javafx.scene.input.MouseEvent event) {
        String List = subcatlist.getSelectionModel().getSelectedItem();
        List2 = subcatlist.getSelectionModel().getSelectedItem();
        updatename.setText(List);
    }

    @FXML
    private void btnlistupdate(ActionEvent event) {
        try {
            dbclass.push("UPDATE add_subcategory SET subcategoryname = '" + updatename.getText().toUpperCase() + "' WHERE subcategoryname = '" + List2.toUpperCase() + "'");
            int selectid = subcatlist.getSelectionModel().getSelectedIndex();
            subcatlist.getItems().remove(selectid);
            subcatlist.getItems().add(updatename.getText());
            updatename.setText(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void enterlistadd(ActionEvent event) {
        try {
            dbclass.push("insert into add_subcategory (categoryid,subcategoryname,enterdate) values ('" + catid.getText().toUpperCase()
                    + "','" + subcatname.getText().toUpperCase() + "','" + date.format(d) + "')");
            subcatlist.getItems().add(subcatname.getText());
            subcatname.setText(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void enterupdatelist(ActionEvent event) {
        try {
            dbclass.push("UPDATE add_subcategory SET subcategoryname = '" + updatename.getText().toUpperCase() + "' WHERE subcategoryname = '" + List2.toUpperCase() + "'");
            int selectid = subcatlist.getSelectionModel().getSelectedIndex();
            subcatlist.getItems().remove(selectid);
            subcatlist.getItems().add(updatename.getText());
            updatename.setText(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btncancel(ActionEvent event) {
        Clear();
    }

    private void Clear() {
        subcatname.setVisible(false);
        addlist.setVisible(false);
        listupdate.setVisible(false);
        updatename.setVisible(false);
        cancel.setVisible(false);
        add.setVisible(true);
        ganaretid();
        catname.setText(null);
        String rec = "RACK NO: ";
        rackno.setText(rec);
        int selectid = subcatlist.getSelectionModel().getSelectedIndex();
        subcatlist.getItems().clear();
        search.setText(null);

    }

    public void SEARCH() {
        try {
            int selectid = subcatlist.getSelectionModel().getSelectedIndex();
            subcatlist.getItems().clear();
            ResultSet rs = dbclass.search("select * from category where category_id = '" + search.getText() + "'");
            if (rs.next()) {
                catid.setText(rs.getString("category_id"));
                catname.setText(rs.getString("categoryname"));
                rackno.setText(rs.getString("racknumber"));
                ResultSet rs2 = dbclass.search("select * from add_subcategory where categoryid = '" + catid.getText() + "'");
                while (rs2.next()) {
                    String Lists = rs2.getString("subcategoryname");
                    subcatlist.getItems().add(Lists);
                    add.setVisible(false);
                    cancel.setVisible(true);
                    addlist.setVisible(true);
                    subcatname.setVisible(true);
                    listupdate.setVisible(true);
                    updatename.setVisible(true);
                }

            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Category id Error");
                alert.setHeaderText("Category ID Invalied..!");
                alert.showAndWait();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void searchenter(ActionEvent event) {
        SEARCH();
    }

    public void categoryupdate() {
        try {
            if (!search.getText().trim().equals("")) {
                dbclass.push("update category set categoryname = '" + catname.getText().toUpperCase()
                        + "', racknumber = '" + rackno.getText().toUpperCase() + "' where category_id = '" + catid.getText().toUpperCase() + "'");
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Category Update Information");
                alert.setHeaderText("Category Update Success..!");
                alert.showAndWait();
                Clear();
            }
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Category Update Error");
            alert.setHeaderText("Category Update Invalied..!");
            alert.showAndWait();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void LoadTable(){
        try {
            ResultSet rs = dbclass.search("select * from category");
            while(rs.next()){
                TableList.add(new ctegory_table_controller(
                        rs.getString("category_id"),
                        rs.getString("categoryname"),
                        rs.getString("racknumber"), 
                        rs.getString("enterdate"),
                        rs.getString("status"), 
                        rs.getString("empid")));
            }
            table6.setItems(TableList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void SetCell(){
        c_catid.setCellValueFactory(new PropertyValueFactory<>("Categoryid"));
        c_catname.setCellValueFactory(new PropertyValueFactory<>("Categoryname"));
        c_recname.setCellValueFactory(new PropertyValueFactory<>("Categoryrackno"));
        c_enterdate.setCellValueFactory(new PropertyValueFactory<>("Enterdate"));
        c_states.setCellValueFactory(new PropertyValueFactory<>("Status"));
        c_empyname.setCellValueFactory(new PropertyValueFactory<>("Employeeid"));
    }
}
