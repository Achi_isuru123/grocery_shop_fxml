/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package add_secton;

import DB.dbclass;
import DB.systemcomfigdata;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import tableview.custome_table_controler;

/**
 * FXML Controller class
 *
 * @author isuru
 */
public class Add_customerController implements Initializable {

    Date d = new Date();
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
    String select;

    @FXML
    private JFXTextField cusid;
    @FXML
    private JFXTextField cusname;
    @FXML
    private JFXTextField cusaddress;
    @FXML
    private JFXComboBox<String> custype;
    @FXML
    private JFXTextField cusnumber;
    @FXML
    private JFXButton cusadd;
    @FXML
    private JFXButton cusupdate;
    @FXML
    private JFXTextField amountlimit;
    @FXML
    private TextField search;
    @FXML
    private JFXTextField selecttype;
    @FXML
    private TableView<tableview.custome_table_controler> table1;
    @FXML
    private TableColumn<tableview.custome_table_controler, String> col_cusid;
    @FXML
    private TableColumn<tableview.custome_table_controler, String> col_cusname;
    @FXML
    private TableColumn<tableview.custome_table_controler, String> col_address;
    @FXML
    private TableColumn<tableview.custome_table_controler, String> col_mobno;
    @FXML
    private TableColumn<tableview.custome_table_controler, String> col_custype;
    @FXML
    private TableColumn<tableview.custome_table_controler, Double> col_amount;
    @FXML
    private TableColumn<tableview.custome_table_controler, Double> c_av_amount;
    @FXML
    private TableColumn<tableview.custome_table_controler, String> col_enterdate;
    @FXML
    private TableColumn<tableview.custome_table_controler, String> col_update;
    @FXML
    private TableColumn<tableview.custome_table_controler, String> col_status;
    @FXML
    private TableColumn<tableview.custome_table_controler, String> col_empname;
    ObservableList<tableview.custome_table_controler> TableList = FXCollections.observableArrayList();
    @FXML
    private TextField search_list;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ganaretid();
        amountlimit.setVisible(false);
        amountlimit.setText("LKR  : ");
        cusnumber.setText("(+94) ");
        setList();
        amountlimit.setText("0.00");
        selecttype.setVisible(false);
        LoadTable();
        setCellTable();
    }

    @FXML
    private void btncusadd(ActionEvent event) {
        try {

            if (!cusname.getText().trim().equals("") && !cusaddress.getText().trim().equals("") && !cusnumber.getText().trim().equals("")) {
                dbclass.push("insert into add_customer values ('" + cusid.getText().toUpperCase()
                        + "','" + cusname.getText().toUpperCase()
                        + "','" + cusaddress.getText().toUpperCase()
                        + "','" + cusnumber.getText()
                        + "','" + custype.getSelectionModel().getSelectedItem().toString().toUpperCase()
                        + "','" + Double.parseDouble(amountlimit.getText())
                        + "','" + 0
                        + "','" + date.format(d) + "',(Null),'1','" + systemcomfigdata.getEmpid() + "')");
                clear();
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Customer Infomation alert");
                alert.setHeaderText("Success..!");
                alert.showAndWait();
                table1.getItems().clear();
                LoadTable();
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Customer Infomation error");
                alert.setHeaderText("Text Field is Empty");
                alert.showAndWait();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void btncusupdate(ActionEvent event) {
        try {
            dbclass.push("update add_customer set name = '" + cusname.getText().toUpperCase()
                    + "', address = '" + cusaddress.getText().toUpperCase()
                    + "', number = '" + cusnumber.getText()
                    + "', type = '" + custype.getSelectionModel().getSelectedItem().toString().toUpperCase()
                    + "', Amountlimit = '" + Double.parseDouble(amountlimit.getText().split(":")[1])
                    + "', updatedate = '" + date.format(d) + "', status = '1', empid = '" + systemcomfigdata.getEmpid() + "' where customerid = '" + search.getText().toUpperCase() + "'");
            clear();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Customer Infomation alert");
            alert.setHeaderText("Update Success..!");
            alert.showAndWait();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ganaretid() {
        try {
            ResultSet rs = dbclass.search("select count(*) as cus_id from add_customer");
            if (rs.next()) {
                int Count = rs.getInt("cus_id");
                String id = "CUS" + (++Count);
                cusid.setText(id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setList() {
        ObservableList List = FXCollections.observableArrayList("Cash Customer", "Card Customer", "Credit Customer");
        custype.setItems(List);

    }

    public void clear() {
        ganaretid();
        cusname.setText(null);
        cusaddress.setText(null);
        cusnumber.setText(null);
        cusnumber.setText(null);
        selecttype.setVisible(false);
        amountlimit.setVisible(false);
        search.setText(null);
    }

    @FXML
    void cliktype(ActionEvent event) {
        select = custype.getSelectionModel().getSelectedItem().toString();
        if (select.trim().equals("Credit Customer")) {
            amountlimit.setVisible(true);

        } else {
            amountlimit.setVisible(false);
        }
    }

    @FXML
    void searchcus(ActionEvent event) {
        try {
            custype.setVisible(false);
            amountlimit.setVisible(true);
            ResultSet rs = dbclass.search("select * from add_customer where customerid = '" + search.getText() + "'");
            if (rs.next()) {
                cusid.setText(rs.getString("customerid"));
                cusname.setText(rs.getString("name"));
                cusaddress.setText(rs.getString("address"));
                cusnumber.setText(rs.getString("number"));
                String string3 = rs.getString("type");
                if (string3.trim().equals("CREDIT CUSTOMER")) {
                    selecttype.setVisible(true);
                    String string = rs.getString("Amountlimit");
                    String Rs = "LKR  : " + string;
                    amountlimit.setText(Rs);
                    selecttype.setText(string3);
                } else {
                    selecttype.setVisible(true);
                    amountlimit.setVisible(false);
                    String string = rs.getString("Amountlimit");
                    String Rs = "LKR  : " + string;
                    amountlimit.setText(Rs);
                    selecttype.setText(string3);
                    selecttype.setText(string3);
                }

            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Customer Id Infomation ");
                alert.setHeaderText("Customer Id Invalid");
                alert.showAndWait();
                search.setText(null);
                clear();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void btnsearch(ActionEvent event) {
        try {
            custype.setVisible(false);
            amountlimit.setVisible(true);
            ResultSet rs = dbclass.search("select * from add_customer where customerid = '" + search.getText() + "'");
            if (rs.next()) {
                cusid.setText(rs.getString("customerid"));
                cusname.setText(rs.getString("name"));
                cusaddress.setText(rs.getString("address"));
                cusnumber.setText(rs.getString("number"));
                String string3 = rs.getString("type");
                if (string3.trim().equals("CREDIT CUSTOMER")) {
                    selecttype.setVisible(true);
                    String string = rs.getString("Amountlimit");
                    String Rs = "LKR  : " + string;
                    amountlimit.setText(Rs);
                    selecttype.setText(string3);
                } else {
                    selecttype.setVisible(true);
                    amountlimit.setVisible(false);
                    String string = rs.getString("Amountlimit");
                    String Rs = "LKR  : " + string;
                    amountlimit.setText(Rs);
                    selecttype.setText(string3);
                    selecttype.setText(string3);
                }

            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Customer Id Infomation ");
                alert.setHeaderText("Customer Id Invalid");
                alert.showAndWait();
                search.setText(null);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void btnchack(ActionEvent event) {
        selecttype.setVisible(false);
        custype.setVisible(true);
    }

    public void LoadTable() {
        try {
            ResultSet rs = dbclass.search("select * from add_customer");
            while (rs.next()) {
                TableList.add(new custome_table_controler(
                        rs.getString("customerid"),
                        rs.getString("name"),
                        rs.getString("address"),
                        rs.getString("number"),
                        rs.getString("type"),
                        rs.getDouble("Amountlimit"),
                        rs.getDouble("Availableamount"),
                        rs.getString("enterdate"),
                        rs.getString("updatedate"),
                        rs.getString("status"),
                        rs.getString("empid")));

            }
            table1.setItems(TableList);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setCellTable() {
        col_cusid.setCellValueFactory(new PropertyValueFactory<>("custome_id"));
        col_cusname.setCellValueFactory(new PropertyValueFactory<>("custome_name"));
        col_address.setCellValueFactory(new PropertyValueFactory<>("custome_address"));
        col_mobno.setCellValueFactory(new PropertyValueFactory<>("custome_number"));
        col_custype.setCellValueFactory(new PropertyValueFactory<>("custome_type"));
        col_amount.setCellValueFactory(new PropertyValueFactory<>("Amount_limit"));
        c_av_amount.setCellValueFactory(new PropertyValueFactory<>("Amount_limit"));
        col_enterdate.setCellValueFactory(new PropertyValueFactory<>("enter_date"));
        col_update.setCellValueFactory(new PropertyValueFactory<>("up_date"));
        col_status.setCellValueFactory(new PropertyValueFactory<>("status"));
        col_empname.setCellValueFactory(new PropertyValueFactory<>("empname"));
    }

    @FXML
    private void search_customer(KeyEvent event) {
         try {
             System.out.println("Achi");
            FilteredList<tableview.custome_table_controler> filterlist = new FilteredList<>(TableList, b -> true);
            search_list.textProperty().addListener((observable, oldValue, newValue) -> {
                filterlist.setPredicate(customer -> {
                    if (newValue.isEmpty() || newValue.isEmpty() || newValue == null) {
                        return true;
                    }
                    String lowercasefilter = newValue.toLowerCase();

                    if (customer.getCustome_type().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(customer.getAmount_limit()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (customer.getCustome_address().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (customer.getCustome_id().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (customer.getCustome_name().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else {
                        return false;
                    }

                });
            });
            SortedList<tableview.custome_table_controler> sortedlist = new SortedList<>(filterlist);
            sortedlist.comparatorProperty().bind(table1.comparatorProperty());
            table1.setItems(sortedlist);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
