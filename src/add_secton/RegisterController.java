/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package add_secton;

import DB.MD5;
import DB.dbclass;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.io.File;
import java.net.URL;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.ImagePattern;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import tableview.employee_table_controller;

/**
 * FXML Controller class
 *
 * @author isuru
 */
public class RegisterController implements Initializable {

    String replaespath;
    Date d = new Date();
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    @FXML
    private JFXTextField fullname;
    @FXML
    private JFXTextField emailaddress;
    @FXML
    private JFXTextField username;
    @FXML
    private JFXPasswordField password;
    @FXML
    private JFXPasswordField repassword;
    @FXML
    private JFXTextField phoneno;
    @FXML
    private Circle userimage;
    @FXML
    private JFXComboBox<String> section;
    @FXML
    private JFXButton emoimage;
    @FXML
    private JFXButton cancel;
    @FXML
    private JFXButton save;
    @FXML
    private AnchorPane employeepane;
    @FXML
    private TableView<tableview.employee_table_controller> table5;
    @FXML
    private TableColumn<tableview.employee_table_controller, String> c_empid;
    @FXML
    private TableColumn<tableview.employee_table_controller, String> c_empname;
    @FXML
    private TableColumn<tableview.employee_table_controller, String> c_empemail;
    @FXML
    private TableColumn<tableview.employee_table_controller, String> cusername;
    @FXML
    private TableColumn<tableview.employee_table_controller, String> c_contectno;
    @FXML
    private TableColumn<tableview.employee_table_controller, String> c_section;
    @FXML
    private TableColumn<tableview.employee_table_controller, String> c_userimage;
    @FXML
    private TableColumn<tableview.employee_table_controller, String> c_regidt;
    @FXML
    private TableColumn<tableview.employee_table_controller, String> c_status;
    ObservableList<tableview.employee_table_controller> tableList = FXCollections.observableArrayList();
    private TextField search_list;
    @FXML
    private TextField searchtable;

    /**
     * Initializes the controller class.
     */
    public void saveimage() {
        FileChooser fileChooser = new FileChooser();
        Stage stage = (Stage) employeepane.getScene().getWindow();
        File file = fileChooser.showOpenDialog(stage);
        String imagepath = file.getAbsolutePath();
        replaespath = imagepath.replace("\\", "/");
        System.out.println(imagepath);
        if (file != null) {
            Image img = new Image(file.toURI().toString(), 187, 537, false, true);
            userimage.setFill(new ImagePattern(img));
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Not Selected Image");
            alert.setHeaderText("Image Not Working");
            alert.showAndWait();
        }
        
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setSection();
        Loadtable();
        Setcell();
    }

    @FXML
    private void btnempimage(ActionEvent event) {
        saveimage();
    }

    @FXML
    private void btncancel(ActionEvent event) {
       
    }

    @FXML
    private void btnsave(ActionEvent event) {
        try {
            String pass = new String(password.getText());
            String repass = new String(repassword.getText());
            String pwecrp = MD5.getMd5(pass);
            if (!fullname.getText().trim().equals("") && !emailaddress.getText().trim().equals("") && !username.getText().trim().equals("") && !phoneno.getText().trim().equals("")) {
                if (pass.trim().equals(repass)) {
                    dbclass.push("insert into employe_register (fullname,email,username,password,contectno,section,imagepath,registerdatetime) values ('" + fullname.getText().toUpperCase()
                            + "','" + emailaddress.getText()
                            + "','" + username.getText() + "','" + pwecrp + "','" + phoneno.getText() + "','" + (String) section.getSelectionModel().getSelectedItem()
                            + "','" + replaespath + "','" + date.format(d) + "')");
                    Clearfeild();
                    Alert aleat = new Alert(Alert.AlertType.INFORMATION);
                    aleat.setHeaderText("Employee save sccess..!");
                    aleat.setTitle("Employee Added");
                    aleat.showAndWait();
                    table5.getItems().clear();
                    Loadtable();
                } else {
                    Alert aleat = new Alert(Alert.AlertType.WARNING);
                    aleat.setHeaderText("Password dosen't mateced");
                    aleat.setTitle("Error");
                    aleat.showAndWait();
                    password.setText(null);
                    repassword.setText(null);
                }

            } else {
                Alert aleat = new Alert(Alert.AlertType.WARNING);
                aleat.setHeaderText("Text Feild is Empty");
                aleat.setTitle("Error");
                aleat.showAndWait();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Clearfeild() {
        fullname.setText(null);
        emailaddress.setText(null);
        username.setText(null);
        password.setText(null);
        repassword.setText(null);
        phoneno.setText(null);
        section.setValue(null);
        userimage.setFill(null);

    }

    public void setSection() {
        try {
            ResultSet rs = dbclass.search("select * from add_section");
            ObservableList list = FXCollections.observableArrayList();
            while (rs.next()) {

                list.add(new String(rs.getString("name")));
            }
            section.setItems(list);
        } catch (Exception e) {
        }
    }

    @FXML
    private void actionemail(ActionEvent event) {
        Pattern p = Pattern.compile("(a-za-z0-9)[a-zA-z0-9._] *@ [a-zA-z0-9]+([.][a-zA-z]+)+");
        Matcher m = p.matcher(emailaddress.getText());
        if (m.find() && m.group().equals(emailaddress.getText())) {
            System.out.println("AAAA");
        } else {
            Alert aleat = new Alert(Alert.AlertType.ERROR);
            aleat.setHeaderText("Please Enter Valid Email..!");
            aleat.setTitle("Validate Email");
            aleat.showAndWait();
        }
    }

    @FXML
    private void actionpass(ActionEvent event) {
        Pattern p = Pattern.compile("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{8,20}$");
        Matcher m = p.matcher(password.getText());
        if (m.matches()) {
            System.out.println("BBBB");

        } else {
            Alert aleat = new Alert(Alert.AlertType.ERROR);
            aleat.setHeaderText("Text Feild is Empty");
            aleat.setTitle("Error");
            aleat.showAndWait();
        }
    }

    @FXML
    private void actioncontectno(ActionEvent event) {
        Pattern p = Pattern.compile("(0|91)?[7-9] [0-9](9)");
        Matcher m = p.matcher(phoneno.getText());
        if (m.find() && m.group().equals(phoneno.getText())) {
            System.out.println("CCCCC");
        } else {
            Alert aleat = new Alert(Alert.AlertType.ERROR);
            aleat.setHeaderText("Phone Number Is invalied");
            aleat.setTitle("Validate Phone Number");
            aleat.showAndWait();
        }
    }

    public static boolean
            isValidPassword(String password) {

        // Regex to check valid password.
        String regex = "^(?=.*[0-9])"
                + "(?=.*[a-z])(?=.*[A-Z])"
                + "(?=.*[@#$%^&+=])"
                + "(?=\\S+$).{8,20}$";

        // Compile the ReGex
        Pattern p = Pattern.compile(regex);

        // If the password is empty
        // return false
        if (password == null) {
            return false;
        }

        // Pattern class contains matcher() method
        // to find matching between given password
        // and regular expression.
        Matcher m = p.matcher(password);

        // Return if the password
        // matched the ReGex
        return m.matches();
    }

    @FXML
    private void action_search(KeyEvent event) {
          try {
            FilteredList<tableview.employee_table_controller> filterlist = new FilteredList<>(tableList, a -> true);
            searchtable.textProperty().addListener((observable, oldValue, newValue) -> {
                filterlist.setPredicate(employee -> {
                    if (newValue.isEmpty() || newValue.isEmpty() || newValue == null) {
                        return true;
                    }
                    String lowercasefilter = newValue.toLowerCase();

                    if (employee.getEmpname().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (employee.getRegistertimedate().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (employee.getSection().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (employee.getEmpemail().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    }else if (employee.getUsername().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    }else if (String.valueOf(employee.getEmpid()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else {
                        return false;
                    }

                });
            });
            SortedList<tableview.employee_table_controller> sortedlist = new SortedList<>(filterlist);
            sortedlist.comparatorProperty().bind(table5.comparatorProperty());
            table5.setItems(sortedlist);

        } catch (Exception e) {
            e.printStackTrace();
        }
    

    }

    public void Loadtable() {
        try {
            ResultSet rs = dbclass.search("select * from employe_register");
            while (rs.next()) {
                boolean string = rs.getBoolean("status");
                if(string){
                     tableList.add(new employee_table_controller(
                        rs.getInt("employeeid"),
                        rs.getString("fullname"),
                        rs.getString("email"),
                        rs.getString("username"),
                        rs.getString("contectno"),
                        rs.getString("section"),
                        rs.getString("imagepath"),
                        rs.getString("registerdatetime"),
                        rs.getString("status")));
                }    
            }
            table5.setItems(tableList);
        } catch (Exception e) {
        }
    }

    public void Setcell() {
        c_empid.setCellValueFactory(new PropertyValueFactory<>("Empid"));
        c_empname.setCellValueFactory(new PropertyValueFactory<>("Empname"));
        c_empemail.setCellValueFactory(new PropertyValueFactory<>("Empemail"));
        cusername.setCellValueFactory(new PropertyValueFactory<>("Username"));
        c_contectno.setCellValueFactory(new PropertyValueFactory<>("Contectno"));
        c_section.setCellValueFactory(new PropertyValueFactory<>("Section"));
        c_userimage.setCellValueFactory(new PropertyValueFactory<>("Image"));
        c_regidt.setCellValueFactory(new PropertyValueFactory<>("Registertimedate"));
        c_status.setCellValueFactory(new PropertyValueFactory<>("status"));
        
    }
}
