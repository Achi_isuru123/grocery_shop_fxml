/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package add_secton;

import DB.dbclass;
import DB.systemcomfigdata;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;

/**
 * FXML Controller class
 *
 * @author isuru
 */
public class Add_supplierController implements Initializable {

    @FXML
    private TableView<tableview.suppler_table_controller> table2;
    @FXML
    private TableColumn<tableview.suppler_table_controller, String> c_supid;
    @FXML
    private TableColumn<tableview.suppler_table_controller, String> c_supname;
    @FXML
    private TableColumn<tableview.suppler_table_controller, String> c_comname;
    @FXML
    private TableColumn<tableview.suppler_table_controller, String> c_cmail;
    @FXML
    private TableColumn<tableview.suppler_table_controller, String> c_caddress;
    @FXML
    private TableColumn<tableview.suppler_table_controller, String> c_cnumber;
    @FXML
    private TableColumn<tableview.suppler_table_controller, String> c_empid;
    @FXML
    private TableColumn<tableview.suppler_table_controller, String> c_centerdate;
    @FXML
    private TableColumn<tableview.suppler_table_controller, String> c_cupdate;
    @FXML
    private TableColumn<tableview.suppler_table_controller, String> c_status;
    ObservableList<tableview.suppler_table_controller> TableList2 = FXCollections.observableArrayList();

    Date d = new Date();
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
    @FXML
    private JFXTextField supid;
    @FXML
    private JFXTextField supname;
    @FXML
    private JFXTextField camname;
    @FXML
    private JFXTextField cammail;
    @FXML
    private JFXTextField camaddress;
    @FXML
    private JFXTextField cntectno;
    @FXML
    private JFXButton addsup;
    @FXML
    private JFXButton updatesup;
    @FXML
    private JFXTextField search;
    @FXML
    private JFXButton searchsup;
    @FXML
    private TextField search_list;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ganareteid();
        cntectno.setText("(+94) ");
        LoadTablesupller();
        setCellTablesupller();
    }

    @FXML
    private void btnsup(ActionEvent event) {
        try {
            if (!supname.getText().trim().equals("") && !camname.getText().trim().equals("") && !cammail.getText().trim().equals("") && !camaddress.getText().trim().equals("") && !cntectno.getText().trim().equals("")) {
                dbclass.push("insert into add_supplier values ('" + supid.getText().toUpperCase()
                        + "','" + supname.getText().toUpperCase()
                        + "','" + camname.getText().toUpperCase()
                        + "','" + cammail.getText()
                        + "','" + camaddress.getText().toUpperCase()
                        + "','" + cntectno.getText()
                        + "','" + systemcomfigdata.getEmpid()
                        + "','" + date.format(d) + "',(Null),'1')");

                Clear();
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Supplier save Infomation");
                alert.setHeaderText("Supplier added Success..!");
                alert.showAndWait();
                table2.getItems().clear();
                LoadTablesupller();
            } else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Supplier save Warning");
                alert.setHeaderText("Text feild Is Empty..!");
                alert.showAndWait();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btnupdate(ActionEvent event) {
        try {
            dbclass.push("update add_supplier set fullname = '" + supname.getText().toUpperCase()
                    + "', companyname = '" + camname.getText().toUpperCase()
                    + "', companymail = '" + cammail.getText()
                    + "', companyaddress = '" + camaddress.getText().toUpperCase()
                    + "', contectnumber = '" + cntectno.getText()
                    + "', empid = '" + systemcomfigdata.getEmpid()
                    + "', updatesup = '" + date.format(d)
                    + "' where supplierid = '" + supid.getText().toUpperCase() + "'");
            Clear();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Supplier Update Infomation");
            alert.setHeaderText("Supplier Update Success..!");
            alert.showAndWait();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void searchenter(ActionEvent event) {
        try {
            ResultSet rs = dbclass.search("select * from add_supplier where supplierid = '" + search.getText() + "'");
            if (rs.next()) {
                supid.setText(rs.getString("supplierid"));
                supname.setText(rs.getString("fullname"));
                camname.setText(rs.getString("companyname"));
                cammail.setText(rs.getString("companymail"));
                camaddress.setText(rs.getString("companyaddress"));
                cntectno.setText(rs.getString("contectnumber"));

            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Supplier Search Error");
                alert.setHeaderText("Supplier Id is Invalied..!");
                alert.showAndWait();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btnsearch(ActionEvent event) {
        try {
            ResultSet rs = dbclass.search("select * from add_supplier where supplierid = '" + search.getText() + "'");
            if (rs.next()) {
                supname.setText(rs.getString("fullname"));
                camname.setText(rs.getString("companyname"));
                cammail.setText(rs.getString("companymail"));
                camaddress.setText(rs.getString("companyaddress"));
                String string = rs.getString("contectnumber");
                String No = "(+94) " + string;
                cntectno.setText(No);
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Supplier Search Error");
                alert.setHeaderText("Supplier Id is Invalied..!");
                alert.showAndWait();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ganareteid() {
        try {
            ResultSet rs = dbclass.search("select count(*) as Sup_id from add_supplier");
            if (rs.next()) {
                int Count = rs.getInt("Sup_id");
                String id = "SUP" + (++Count);
                supid.setText(id);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Clear() {
        ganareteid();
        camname.setText(null);
        supname.setText(null);
        cammail.setText(null);
        camaddress.setText(null);
        cntectno.setText(null);

    }

    public void LoadTablesupller() {
        try {
            ResultSet rs = dbclass.search("select * from add_supplier");
            while (rs.next()) {
                TableList2.add(new tableview.suppler_table_controller(
                        rs.getString("supplierid"),
                        rs.getString("fullname"),
                        rs.getString("companyname"),
                        rs.getString("companymail"),
                        rs.getString("companyaddress"),
                        rs.getString("contectnumber"),
                        rs.getString("enterdate"),
                        rs.getString("updatesup"),
                        rs.getString("status"),
                        rs.getString("empid")));

            }
            table2.setItems(TableList2);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setCellTablesupller() {
        c_supid.setCellValueFactory(new PropertyValueFactory<>("supplerid"));
        c_supname.setCellValueFactory(new PropertyValueFactory<>("supplername"));
        c_comname.setCellValueFactory(new PropertyValueFactory<>("companyname"));
        c_cmail.setCellValueFactory(new PropertyValueFactory<>("companymail"));
        c_caddress.setCellValueFactory(new PropertyValueFactory<>("companyaddress"));
        c_cnumber.setCellValueFactory(new PropertyValueFactory<>("contectnumber"));
        c_centerdate.setCellValueFactory(new PropertyValueFactory<>("enteddate"));
        c_cupdate.setCellValueFactory(new PropertyValueFactory<>("update"));
        c_status.setCellValueFactory(new PropertyValueFactory<>("status"));
        c_empid.setCellValueFactory(new PropertyValueFactory<>("employeeid"));
    }

    @FXML
    private void search_suppler(KeyEvent event) {
        try {
            FilteredList<tableview.suppler_table_controller> filterlist = new FilteredList<>(TableList2, b -> true);
            search_list.textProperty().addListener((observable, oldValue, newValue) -> {
                filterlist.setPredicate(suppler -> {
                    if (newValue.isEmpty() || newValue.isEmpty() || newValue == null) {
                        return true;
                    }
                    String lowercasefilter = newValue.toLowerCase();

                    if (suppler.getSupplername().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (suppler.getSupplerid().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (suppler.getCompanyname().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (suppler.getCompanyaddress().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else {
                        return false;
                    }

                });
            });
            SortedList<tableview.suppler_table_controller> sortedlist = new SortedList<>(filterlist);
            sortedlist.comparatorProperty().bind(table2.comparatorProperty());
            table2.setItems(sortedlist);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
