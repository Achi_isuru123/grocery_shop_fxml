/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package add_secton;

import DB.dbclass;
import DB.systemcomfigdata;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author isuru
 */
public class SectionController implements Initializable {

    @FXML
    private TableView<tableview.section_table_controller> table4;
    @FXML
    private TableColumn<tableview.section_table_controller, String> c_sectionid;
    @FXML
    private TableColumn<tableview.section_table_controller, String> c_sectionname;
    @FXML
    private TableColumn<tableview.section_table_controller, String> c_empid;
    @FXML
    private TableColumn<tableview.section_table_controller, String> c_status;
    @FXML
    private TableColumn<tableview.section_table_controller, String> c_enterdate;
    ObservableList<tableview.section_table_controller> TableList4 = FXCollections.observableArrayList();

    Date d = new Date();
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
    @FXML
    private JFXTextField sectionid;
    @FXML
    private JFXTextField sectionname;

    @FXML
    private JFXButton addsection;
    @FXML
    private JFXButton search;
    @FXML
    private JFXButton updatesection;
    @FXML
    private JFXTextField searchid;
    @FXML
    private ListView<String> sectionview;
    @FXML
    private TextField search_list;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ganaretid();
        Listview();
        sectionview.setVisible(false);
        LoadTablesection();
        setCellTablesection();
    }

    @FXML
    private void btnsection(ActionEvent event) {

        try {
            if (!sectionname.getText().trim().equals("")) {
                dbclass.push("insert into add_section (sectionid,name,empid,status,enterdate) values ('" + sectionid.getText() + "','" + sectionname.getText()
                        + "','" + systemcomfigdata.getEmpid() + "','1','" + date.format(d) + "')");
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Section Infomtion");
                alert.setHeaderText("Section Added Success..!");
                alert.showAndWait();
                sectionname.setText(null);
                sectionview.setVisible(false);
                ganaretid();
                table4.getItems().clear();
                LoadTablesection();
            } else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Section Infomtion");
                alert.setHeaderText("TextField is Empty..!");
                alert.showAndWait();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void ganaretid() {
        try {
            ResultSet rs = dbclass.search("select count(*) as section_id from add_section");
            if (rs.next()) {
                int Count = rs.getInt("section_id");
                String id = "SECTION " + (++Count);
                sectionid.setText(id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void search() {
        try {
            ResultSet rs = dbclass.search("select * from add_section where sectionid = '" + searchid.getText() + "'");
            if (rs.next()) {
                sectionid.setText(rs.getString("sectionid"));
                sectionname.setText(rs.getString("name"));
                sectionview.setVisible(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btnsearch(ActionEvent event) {
        search();
    }

    @FXML
    private void updatesection(ActionEvent event) {
        try {
            dbclass.push("update add_section set name = '" + sectionname.getText() + "' where sectionid = '" + searchid.getText().toUpperCase() + "'");
            sectionname.setText(null);
            ganaretid();
            sectionview.setVisible(false);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Section Update Infomtion");
            alert.setHeaderText("Section Update Success..!");
            alert.showAndWait();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void mouseclik(MouseEvent event) {
        String selectedItems = sectionview.getSelectionModel().getSelectedItems().toString();
        sectionname.setText(selectedItems);
        sectionview.setVisible(false);
    }

    @FXML
    private void searchenter(ActionEvent event) {
        search();
    }

    @FXML
    private void setsectionname(ActionEvent event) {
    }

    @FXML
    private void entername(KeyEvent event) {
        sectionview.setVisible(true);
    }

    public void Listview() {
        try {
            ResultSet rs = dbclass.search("select * from add_section");
            while (rs.next()) {
                String List = rs.getString("name");
                sectionview.getItems().add(List);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void LoadTablesection() {
        try {
            ResultSet rs = dbclass.search("select * from add_section");
            while (rs.next()) {
                TableList4.add(new tableview.section_table_controller(
                        rs.getString("sectionid"),
                        rs.getString("name"),
                        rs.getString("enterdate"),
                        rs.getString("status"),
                        rs.getString("empid")));

            }
            table4.setItems(TableList4);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setCellTablesection() {
        c_sectionid.setCellValueFactory(new PropertyValueFactory<>("Sectionid"));
        c_sectionname.setCellValueFactory(new PropertyValueFactory<>("Sectionname"));
        c_enterdate.setCellValueFactory(new PropertyValueFactory<>("Enterdate"));
        c_status.setCellValueFactory(new PropertyValueFactory<>("Status"));
        c_empid.setCellValueFactory(new PropertyValueFactory<>("Employeeid"));

    }

    @FXML
    private void search_section(KeyEvent event) {
        try {
            FilteredList<tableview.section_table_controller> filterlist = new FilteredList<>(TableList4, b -> true);
            search_list.textProperty().addListener((observable, oldValue, newValue) -> {
                filterlist.setPredicate(section -> {
                    if (newValue.isEmpty() || newValue.isEmpty() || newValue == null) {
                        return true;
                    }
                    String lowercasefilter = newValue.toLowerCase();

                    if (section.getSectionname().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (section.getEmployeeid().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (section.getSectionid().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (section.getStatus().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else {
                        return false;
                    }

                });
            });
            SortedList<tableview.section_table_controller> sortedlist = new SortedList<>(filterlist);
            sortedlist.comparatorProperty().bind(table4.comparatorProperty());
            table4.setItems(sortedlist);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
