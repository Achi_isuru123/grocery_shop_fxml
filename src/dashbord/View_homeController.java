/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dashbord;

import DB.dbclass;
import com.jfoenix.controls.JFXListView;
import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author fernando
 */
public class View_homeController implements Initializable {

    @FXML
    private TextField wallet;
    @FXML
    private TextField cardamo;
    @FXML
    private TextField creditamo;
    @FXML
    private JFXListView<String> storelist1;
    @FXML
    private JFXListView<String> stocklist1;
    @FXML
    private JFXListView<String> grnlist1;
    @FXML
    private CheckBox appronal;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        wallet();
        listload1();
        listload2();
        listload3();
    }

    @FXML
    private void btn_approval(ActionEvent event) {
        if (appronal.isSelected()) {
            try {
                ObservableList<String> Search_List = FXCollections.observableArrayList();
                ResultSet rs = dbclass.search("select * from grn");
                while (rs.next()) {
                    String appro = rs.getString("approvad");
                    if (appro.trim().equals("Appovaed")) {
                        Search_List.add("product Name :- " + rs.getString("productname") + "  Suppler Name :-  " + rs.getString("supplername") + "  Qty :-  " + rs.getString("qty") + "  Net Total :-  " + rs.getString("nettotal"));
                    } 
                    
                }
                grnlist1.setItems(Search_List);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            try {
                ObservableList<String> Search_List = FXCollections.observableArrayList();
                ResultSet rs2 = dbclass.search("select * from grn");
                while(rs2.next()){
                    String appro = rs2.getString("approvad");
                        if (appro.trim().equals("Nonapprovaed")) {
                            Search_List.add("product Name :- " + rs2.getString("productname") + "  Suppler Name :-  " + rs2.getString("supplername") + "  Qty :-  " + rs2.getString("qty") + "  Net Total :-  " + rs2.getString("nettotal"));
                        }
                }
                 grnlist1.setItems(Search_List);

            } catch (Exception e) {
                e.printStackTrace();
            }
            }
             }
    double cashamo1;
    double cardamo1;
    double creditamo1;

    public void wallet() {
        try {
            ResultSet rs = dbclass.search("select * from dally_collection");
            while (rs.next()) {
                cashamo1 += rs.getDouble("cashamount");
                cardamo1 += rs.getDouble("cardamount");
                creditamo1 += rs.getDouble("creditamount");
            }
            wallet.setText("" + cashamo1);
            cardamo.setText("" + cardamo1);
            creditamo.setText("" + creditamo1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void listload1() {
        try {
            ObservableList<String> Search_List = FXCollections.observableArrayList();
            ResultSet rs = dbclass.search("select * from store");
            while (rs.next()) {
                Search_List.add("Name :- " + rs.getString("name") + "  Exdate :-  " + rs.getString("Expired_date") + "  Qty :-  " + rs.getString("qty") + "  Status :-  " + rs.getString("status"));
            }
            storelist1.setItems(Search_List);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void listload2() {
        try {
            ObservableList<String> Search_List = FXCollections.observableArrayList();
            ResultSet rs = dbclass.search("select * from instock");
            while (rs.next()) {
                Search_List.add("Name :- " + rs.getString("productname") + "  Exdate :-  " + rs.getString("exdate") + "  Qty :-  " + rs.getString("qty") + "  Recod Lavel :-  " + rs.getString("recodelevel"));
            }
            stocklist1.setItems(Search_List);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
        public void listload3() {
        try {
            ObservableList<String> Search_List = FXCollections.observableArrayList();
            ResultSet rs = dbclass.search("select * from grn");
            while (rs.next()) {
                 Search_List.add("product Name :- " + rs.getString("productname") + "  Suppler Name :-  " + rs.getString("supplername") + "  Qty :-  " + rs.getString("qty") + "  Net Total :-  " + rs.getString("nettotal"));
            }
            grnlist1.setItems(Search_List);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
