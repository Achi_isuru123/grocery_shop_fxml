/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dashbord;

import DB.dbclass;
import DB.systemcomfigdata;
import java.io.File;
import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import tableview.employee_profile;
import tableview.employee_table_controller;

/**
 * FXML Controller class
 *
 * @author fernando
 */
public class SettingController implements Initializable {

    @FXML
    private Circle userimage;
    @FXML
    private AnchorPane setting;
    @FXML
    private Label name;
    @FXML
    private Label section;
    @FXML
    private CheckBox active;
    @FXML
    private CheckBox inactive;
    @FXML
    private TableView<employee_profile> profile_table;
    @FXML
    private TableColumn<employee_profile, String> c_empid;
    @FXML
    private TableColumn<employee_profile, String> c_empname;
    @FXML
    private TableColumn<employee_profile, String> c_section;
    @FXML
    private TableColumn<employee_profile, String> c_status;
    @FXML
    private TextField username;
    @FXML
    private TextField useremail;
    @FXML
    private TextField usernumber;
    @FXML
    private TextField empid;
    ObservableList<employee_profile> list1 = FXCollections.observableArrayList();
    @FXML
    private TextField userserch;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        employeeinactive();
        tablelist();
        loginemployee();
    }

    @FXML
    private void btn_tempry_active(ActionEvent event) {
        try {
            if (section.getText().equals("Admin")) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Bee");
                alert.setHeaderText("Bee");
                alert.setContentText("Bee");
                alert.showAndWait();
                active.setSelected(false);

            } else {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("");
                alert.setHeaderText("");
                alert.setContentText("");
                alert.showAndWait().ifPresent(consumer -> {
                    if (consumer == ButtonType.OK) {
                        try {
                            dbclass.push("update employe_register set status = '1' where employeeid ='" + empid.getText() + "' ");
                            profile_table.getItems().clear();
                            employeeinactive();
                            loginemployee();
                            active.setSelected(false);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        System.out.println("");
                    }

                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_tempry_inactive(ActionEvent event) {
        try {
            if (section.getText().equals("Admin")) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Bee");
                alert.setHeaderText("Bee");
                alert.setContentText("Bee");
                alert.showAndWait();
                inactive.setSelected(false);

            } else {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("");
                alert.setHeaderText("");
                alert.setContentText("");
                alert.showAndWait().ifPresent(consumer -> {
                    if (consumer == ButtonType.OK) {
                        try {
                            dbclass.push("update employe_register set status = '0' where employeeid ='" + empid.getText() + "' ");
                            profile_table.getItems().clear();
                            employeeinactive();
                            inactive.setSelected(false);
                            loginemployee();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        inactive.setSelected(false);
                    }

                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void employeeinactive() {
        try {
            ResultSet rs = dbclass.search("select * from employe_register");
            while (rs.next()) {
                list1.add(new employee_profile(
                        rs.getString("employeeid"),
                        rs.getString("fullname"),
                        rs.getString("section"),
                        rs.getString("status")));
            }
            profile_table.setItems(list1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void tablelist() {
        c_empid.setCellValueFactory(new PropertyValueFactory<>("Empid"));
        c_empname.setCellValueFactory(new PropertyValueFactory<>("Empname"));
        c_section.setCellValueFactory(new PropertyValueFactory<>("Empsection"));
        c_status.setCellValueFactory(new PropertyValueFactory<>("empstatus"));
    }

    public void loginemployee() {
        String imagepath = systemcomfigdata.getImagepath();
        File file = new File(imagepath);
        Image img = new Image(file.toURI().toString(), 388, 991, false, true);
        System.out.println(imagepath);
        userimage.setFill(new ImagePattern(img));
        name.setText(systemcomfigdata.getFullname());
        section.setText(systemcomfigdata.getUsertype());
        empid.setText(systemcomfigdata.getEmpid());
        username.setText(systemcomfigdata.getActiveuser());
        try {
            ResultSet rs = dbclass.search("select * from employe_register where employeeid = '" + empid.getText() + "'");
            if (rs.next()) {
                useremail.setText(rs.getString("email"));
                usernumber.setText(rs.getString("contectno"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void mouse_clik(MouseEvent event) {
        if (event.getClickCount() == 2) {
            employee_profile employeeid = profile_table.getSelectionModel().getSelectedItem();
            empid.setText(employeeid.getEmpid());
            try {
                ResultSet rs = dbclass.search("select * from employe_register where employeeid = '" + empid.getText() + "'");
                if (rs.next()) {
                    section.setText(rs.getString("section"));
                    name.setText(rs.getString("fullname"));
                    username.setText(rs.getString("username"));
                    useremail.setText(rs.getString("email"));
                    usernumber.setText(rs.getString("contectno"));
                    String imagepath = rs.getString("imagepath");
                    File file = new File(imagepath);
                    Image img = new Image(file.toURI().toString(), 388, 991, false, true);
                    userimage.setFill(new ImagePattern(img));
                } else {

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void search(KeyEvent event) {
        try {
            FilteredList<tableview.employee_profile> filterlist = new FilteredList<>(list1, b -> true);
            userserch.textProperty().addListener((observable, oldValue, newValue) -> {
                filterlist.setPredicate(collection -> {
                    if (newValue.isEmpty() || newValue.isEmpty() || newValue == null) {
                        return true;
                    }
                    String lowercasefilter = newValue.toLowerCase();

                    if (collection.getEmpid().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (collection.getEmpname().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (collection.getEmpsection().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (collection.getEmpstatus().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else {
                        return false;
                    }

                });
            });
            SortedList<tableview.employee_profile> sortedlist = new SortedList<>(filterlist);
            sortedlist.comparatorProperty().bind(profile_table.comparatorProperty());
            profile_table.setItems(sortedlist);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    String replaespath;

    @FXML
    private void change_image(MouseEvent event) {
        if (event.getClickCount() == 1) {
            FileChooser fileChooser = new FileChooser();
            Stage stage = (Stage) userimage.getScene().getWindow();
            File file = fileChooser.showOpenDialog(stage);
            String imagepath = file.getAbsolutePath();
            replaespath = imagepath.replace("\\", "/");
            System.out.println(imagepath);
            if (file != null) {
                Image img = new Image(file.toURI().toString(), 388, 991, false, true);
                userimage.setFill(new ImagePattern(img));
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("");
                alert.setHeaderText("");
                alert.setContentText("");
                alert.showAndWait().ifPresent(consumer -> {

                    if (consumer == ButtonType.OK) {
                        try {
                            dbclass.push("update employe_register set imagepath = '" + replaespath + "' where employeeid = '" + empid.getText() + "'");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        loginemployee();
                    }

                });
            } else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Not Selected Image");
                alert.setHeaderText("Image Not Working");
                alert.showAndWait();
            }

        }
    }

    boolean uname() {
        int length = username.getText().length();
        if (length <= 6) {
            return true;
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Validate User Name");
            alert.setHeaderText(null);
            alert.setContentText("Please Enter Valid User Name");
            alert.showAndWait();
            
            return false;
        }

    }

    boolean uemail() {
        Pattern p = Pattern.compile("(a-za-z0-9)[a-zA-z0-9._] *@ [a-zA-z0-9]+([.][a-zA-z]+)+");
        Matcher m = p.matcher(useremail.getText());
        if (m.find() && m.group().equals(useremail.getText())) {
            return true;
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Validate Email");
            alert.setHeaderText(null);
            alert.setContentText("Please Enter Valid Email");
            alert.showAndWait();

            return false;
        }
    }

    boolean unumber() {
        Pattern p = Pattern.compile("(0|91)?[7-9] [0-9](9)");
        Matcher m = p.matcher(usernumber.getText());
        if (m.find() && m.group().equals(usernumber.getText())) {
            return true;
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Validate Phone Number");
            alert.setHeaderText(null);
            alert.setContentText("Please Enter Valid Phone Number");
            alert.showAndWait();
            return false;
        }
    }

    @FXML
    private void btn_employee_update(ActionEvent event) {
        if (uname() & uemail() & unumber()) {
            try {
                dbclass.push("update employe_register set username = '" + username.getText() + "', email = '" + useremail.getText() + "', contectno = '" + usernumber.getText() + "' where employeeid = '" + empid.getText() + "'");
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("");
                alert.setContentText("");
                alert.setHeaderText("");
                alert.showAndWait();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }
    }

    @FXML
    private void btn_restart(ActionEvent event) {

    }
}
