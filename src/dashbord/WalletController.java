/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dashbord;

import DB.dbclass;
import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

/**
 * FXML Controller class
 *
 * @author isuru
 */
public class WalletController implements Initializable {

    @FXML
    private LineChart<String, Double> linechart;
    @FXML
    private NumberAxis y;
    @FXML
    private CategoryAxis x;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        loadchart();
    }

    public void loadchart() {
        try {
            ResultSet rs = dbclass.search("select enterdate,totalamount  from dally_collection");
            XYChart.Series<String, Double> series = new XYChart.Series();
            series.setName("Dally Collection");
            while(rs.next()){
                series.getData().add(new XYChart.Data<>(rs.getString(1),rs.getDouble(2)));
            }
            linechart.getData().add(series);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
