/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dashbord;

import DB.dbclass;
import java.io.File;
import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;


/**
 * FXML Controller class
 *
 * @author isuru
 */
public class Log_tableController implements Initializable {

    @FXML
    private TextField empid;
    @FXML
    private TextField empname;
    @FXML
    private TextField empsection;
    @FXML
    private TextField emplogdate;
    @FXML
    private TextField emplogtime;
    @FXML
    private Circle empimage;
    @FXML
    private TableView<tableview.log_table_controller> table_log;
    @FXML
    private TableColumn<tableview.log_table_controller, String> c_empid;
    @FXML
    private TableColumn<tableview.log_table_controller, String> c_empname;
    @FXML
    private TableColumn<tableview.log_table_controller, String> c_logdate;
    @FXML
    private TableColumn<tableview.log_table_controller, String> c_logtime;
    @FXML
    private TableColumn<tableview.log_table_controller, String> c_logout;
    ObservableList<tableview.log_table_controller> TableList = FXCollections.observableArrayList();
    @FXML
    private TextField search;
    @FXML
    private Label empstatus;
    @FXML
    private TextField empcontectno;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        LoadTable();
        Setcoll();
    }

    @FXML
    private void emplook(ActionEvent event) {
        try {
            if (!empid.getText().trim().equals("")) {
               
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Employee Blocked");
                alert.setHeaderText("Employee Blocked");
                alert.setContentText("Success");
                alert.showAndWait().ifPresent(consumer ->{
                if(consumer == ButtonType.OK){
                    try {
                         dbclass.push("update employe_register set status = '0' where employeeid = '" + empid.getText() + "'");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    
                }else{
                     
                }
                });
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Employee Blocked");
                alert.setHeaderText("Employee Blocked..");
                alert.setContentText("Employee Id is Empty");
                alert.showAndWait();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void search_log(KeyEvent event) {
        try {
            FilteredList<tableview.log_table_controller> filterlist = new FilteredList<>(TableList, b -> true);
            search.textProperty().addListener((observable, oldValue, newValue) -> {
                filterlist.setPredicate(emplog -> {
                    if (newValue.isEmpty() || newValue.isEmpty() || newValue == null) {
                        return true;
                    }
                    String lowercasefilter = newValue.toLowerCase();

                    if (emplog.getEmpname().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(emplog.getEmpid()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (emplog.getLogdate().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (emplog.getLogtime().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else {
                        return false;
                    }

                });
            });
            SortedList<tableview.log_table_controller> sortedlist = new SortedList<>(filterlist);
            sortedlist.comparatorProperty().bind(table_log.comparatorProperty());
            table_log.setItems(sortedlist);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void LoadTable() {
        try {
            ResultSet rs = dbclass.search("select * from login_ditels");
            while (rs.next()) {
                TableList.add(new tableview.log_table_controller(
                        rs.getString("empid"),
                        rs.getString("fullname"),
                        rs.getString("logdate"),
                        rs.getString("logtime")));
            }
            table_log.setItems(TableList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Setcoll() {
        c_empid.setCellValueFactory(new PropertyValueFactory<>("Empid"));
        c_empname.setCellValueFactory(new PropertyValueFactory<>("Empname"));
        c_logdate.setCellValueFactory(new PropertyValueFactory<>("Logdate"));
        c_logtime.setCellValueFactory(new PropertyValueFactory<>("Logtime"));
        c_logout.setCellValueFactory(new PropertyValueFactory<>("Logout"));
    }

    @FXML
    private void setempdata(MouseEvent event) {
        try {
            if (event.getClickCount() == 2) {
                empid.setText(table_log.getSelectionModel().getSelectedItem().getEmpid());
                emplogdate.setText(table_log.getSelectionModel().getSelectedItem().getLogdate());
                emplogtime.setText(table_log.getSelectionModel().getSelectedItem().getLogtime());
                ResultSet rs = dbclass.search("select * from employe_register where employeeid = '"+empid.getText()+"'");
                if (rs.next()) {
                    boolean Status = rs.getBoolean("status");
                    if(Status){
                        empstatus.setText("Active Employee");
                    }else{
                        empstatus.setText("Employee is Blocked");
                    }
                    empname.setText(rs.getString("fullname"));
                    empsection.setText(rs.getString("section"));
                    empcontectno.setText(rs.getString("contectno"));
                    String imagepath = rs.getString("imagepath");
                    File file = new File(imagepath);
                    Image img = new Image(file.toURI().toString(), 388, 991, false, true);
                    empimage.setFill(new ImagePattern(img));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
