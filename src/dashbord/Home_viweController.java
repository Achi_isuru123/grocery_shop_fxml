/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dashbord;

import DB.dbclass;
import DB.systemcomfigdata;
import com.jfoenix.controls.JFXButton;
import java.io.File;
import java.net.URL;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author isuru
 */
public class Home_viweController implements Initializable{

    @FXML
    private BorderPane home_viwe_boder;
    @FXML
    private Circle userimage;

    @FXML
    private Label fname;

    @FXML
    private Label utype;
    String usertype;
    @FXML
    private JFXButton wallet_chart;
    @FXML
    private JFXButton dashbord;
    @FXML
    private JFXButton logtable;

    @FXML
    private JFXButton mainunit;
    @FXML
    private JFXButton commonsection;
    @FXML
    private JFXButton setting;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        String fullname = systemcomfigdata.getFullname();
        usertype = systemcomfigdata.getUsertype();
        String imagepath = systemcomfigdata.getImagepath();
        File file = new File(imagepath);
        Image img = new Image(file.toURI().toString(), 388, 991, false, true);
        System.out.println(imagepath);
        userimage.setFill(new ImagePattern(img));
        fname.setText(fullname);
        utype.setText(usertype);
        mainpage_loder();
        chacksection();
    }

    @FXML
    private void btn_dashbord(ActionEvent event) {
        try {
            AnchorPane root = FXMLLoader.load(getClass().getResource("view_home.fxml"));
            home_viwe_boder.setCenter(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_wall_chart(ActionEvent event) {
        try {
            AnchorPane root = FXMLLoader.load(getClass().getResource("wallet.fxml"));
            home_viwe_boder.setCenter(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_log_table(ActionEvent event) {
        try {
            AnchorPane root = FXMLLoader.load(getClass().getResource("log_table.fxml"));
            home_viwe_boder.setCenter(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_main_unit(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/main_unit/main_interface.fxml"));
            Stage stage = new Stage();
//            stage.initStyle(StageStyle.UTILITY);
            Scene scene = new Scene(root);
            stage.setTitle("Main Infomation Unit");
            Image icon = new Image("/image/img1.jpg");
            stage.getIcons().add(icon);
            stage.setScene(scene);
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_common_section(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/common/common_section.fxml"));
            Stage stage = new Stage();
//            stage.initStyle(StageStyle.UTILITY);
            Scene scene = new Scene(root);
            stage.setTitle("Add Section Unit");
            Image icon = new Image("/image/img1.jpg");
            stage.getIcons().add(icon);
            stage.setScene(scene);
            stage.setScene(scene);
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_setting(ActionEvent event) {
        try {
            AnchorPane pane = FXMLLoader.load(getClass().getResource("setting.fxml"));
            home_viwe_boder.setCenter(pane);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    Date d = new Date();
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat date2 = new SimpleDateFormat("HH:MM:ss");

    @FXML
    private void btn_log_out(ActionEvent event) {
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void mainpage_loder() {
        try {
            AnchorPane root = FXMLLoader.load(getClass().getResource("view_home.fxml"));
            home_viwe_boder.setCenter(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void chacksection(){
        if(usertype.trim().equals("Admin")){
            
        }else if(usertype.trim().equals("Employee")){
            setting.setDisable(true);
            mainunit.setDisable(false);
        }else if(usertype.trim().equals("Stock Manager")){
            dashbord.setDisable(false);
            logtable.setDisable(true);
            commonsection.setDisable(true);
            setting.setDisable(true);
            wallet_chart.setDisable(true);
        }else if(usertype.trim().equals("Cashier")){
            dashbord.setDisable(false);
            logtable.setDisable(true);
            commonsection.setDisable(true);
            setting.setDisable(true);
            wallet_chart.setDisable(true);
        }
    }

}
