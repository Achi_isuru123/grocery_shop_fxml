/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableview;


/**
 *
 * @author isuru
 */
public class custome_table_controler {

   



    private String custome_id;
    private String custome_name;
    private String custome_address;
    private String custome_number;
    private String custome_type;
    private Double Amount_limit;
    private Double AvalibaleAmount;
    private String enter_date;
    private String up_date;
    private String status;
    private String empname;

    public custome_table_controler(String custome_id, String custome_name, String custome_address, String custome_number, String custome_type, Double Amount_limit, Double AvalibaleAmount, String enter_date, String up_date, String status, String empname) {
        this.custome_id = custome_id;
        this.custome_name = custome_name;
        this.custome_address = custome_address;
        this.custome_number = custome_number;
        this.custome_type = custome_type;
        this.Amount_limit = Amount_limit;
        this.AvalibaleAmount = AvalibaleAmount;
        this.enter_date = enter_date;
        this.up_date = up_date;
        this.status = status;
        this.empname = empname;
    }

    public String getCustome_id() {
        return custome_id;
    }

    public void setCustome_id(String custome_id) {
        this.custome_id = custome_id;
    }

    public String getCustome_name() {
        return custome_name;
    }

    public void setCustome_name(String custome_name) {
        this.custome_name = custome_name;
    }

    public String getCustome_address() {
        return custome_address;
    }

    public void setCustome_address(String custome_address) {
        this.custome_address = custome_address;
    }

    public String getCustome_number() {
        return custome_number;
    }

    public void setCustome_number(String custome_number) {
        this.custome_number = custome_number;
    }

    public String getCustome_type() {
        return custome_type;
    }

    public void setCustome_type(String custome_type) {
        this.custome_type = custome_type;
    }

    public Double getAmount_limit() {
        return Amount_limit;
    }

    public void setAmount_limit(Double Amount_limit) {
        this.Amount_limit = Amount_limit;
    }

    public Double getAvalibaleAmount() {
        return AvalibaleAmount;
    }

    public void setAvalibaleAmount(Double AvalibaleAmount) {
        this.AvalibaleAmount = AvalibaleAmount;
    }

    public String getEnter_date() {
        return enter_date;
    }

    public void setEnter_date(String enter_date) {
        this.enter_date = enter_date;
    }

    public String getUp_date() {
        return up_date;
    }

    public void setUp_date(String up_date) {
        this.up_date = up_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmpname() {
        return empname;
    }

    public void setEmpname(String empname) {
        this.empname = empname;
    }
}
