/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableview;

/**
 *
 * @author isuru
 */
public class collection_table_controller {

    private String Invoiceno;
    private String Customerid;
    private String Date;
    private Double Total;
    private Double Nettotal;
    private Double Payment;
    private Double Balance;

    public collection_table_controller(String Invoiceno, String Customerid, String Date, Double Total, Double Nettotal, Double Payment, Double Balance) {
        this.Invoiceno = Invoiceno;
        this.Customerid = Customerid;
        this.Date = Date;
        this.Total = Total;
        this.Nettotal = Nettotal;
        this.Payment = Payment;
        this.Balance = Balance;
    }

    public String getInvoiceno() {
        return Invoiceno;
    }

    public void setInvoiceno(String Invoiceno) {
        this.Invoiceno = Invoiceno;
    }

    public String getCustomerid() {
        return Customerid;
    }

    public void setCustomerid(String Customerid) {
        this.Customerid = Customerid;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public Double getTotal() {
        return Total;
    }

    public void setTotal(Double Total) {
        this.Total = Total;
    }

    public Double getNettotal() {
        return Nettotal;
    }

    public void setNettotal(Double Nettotal) {
        this.Nettotal = Nettotal;
    }

    public Double getPayment() {
        return Payment;
    }

    public void setPayment(Double Payment) {
        this.Payment = Payment;
    }

    public Double getBalance() {
        return Balance;
    }

    public void setBalance(Double Balance) {
        this.Balance = Balance;
    }

    
}
