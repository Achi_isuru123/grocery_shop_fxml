/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableview;

/**
 *
 * @author isuru
 */
public class productlist_table_controller {

    public productlist_table_controller(long Barcodeid, String productname, double Price) {
        this.Barcodeid = Barcodeid;
        this.productname = productname;
        this.Price = Price;
    }
    private long Barcodeid;
    private String productname;
    private double Price;

    public long getBarcodeid() {
        return Barcodeid;
    }

    public void setBarcodeid(long Barcodeid) {
        this.Barcodeid = Barcodeid;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double Price) {
        this.Price = Price;
    }

}
