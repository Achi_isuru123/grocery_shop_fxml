/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableview;

/**
 *
 * @author isuru
 */
public class suppler_table_controller {

    public String supplerid;
    public String supplername;
    public String companyname;
    public String companymail;
    public String companyaddress;
    public String contectnumber;
    public String enteddate;
    public String update;
    public String status;
    public String employeeid;

    public String getSupplerid() {
        return supplerid;
    }

    public void setSupplerid(String supplerid) {
        this.supplerid = supplerid;
    }

    public String getSupplername() {
        return supplername;
    }

    public void setSupplername(String supplername) {
        this.supplername = supplername;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getCompanymail() {
        return companymail;
    }

    public void setCompanymail(String companymail) {
        this.companymail = companymail;
    }

    public String getCompanyaddress() {
        return companyaddress;
    }

    public void setCompanyaddress(String companyaddress) {
        this.companyaddress = companyaddress;
    }

    public String getContectnumber() {
        return contectnumber;
    }

    public void setContectnumber(String contectnumber) {
        this.contectnumber = contectnumber;
    }

    public String getEnteddate() {
        return enteddate;
    }

    public void setEnteddate(String enteddate) {
        this.enteddate = enteddate;
    }

    public String getUpdate() {
        return update;
    }

    public void setUpdate(String update) {
        this.update = update;
    }

    public String getStatus() {
        if (status.equals("1")) {
            String name = "Active";
            return name;
        } else {
            String name = "Inactive";
            return name;
        }

    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmployeeid() {
        return employeeid;
    }

    public void setEmployeeid(String employeeid) {
        this.employeeid = employeeid;
    }

    public suppler_table_controller(String supplerid, String supplername, String companyname, String companymail, String companyaddress, String contectnumber, String enteddate, String update, String status, String employeeid) {
        this.supplerid = supplerid;
        this.supplername = supplername;
        this.companyname = companyname;
        this.companymail = companymail;
        this.companyaddress = companyaddress;
        this.contectnumber = contectnumber;
        this.enteddate = enteddate;
        this.update = update;
        this.status = status;
        this.employeeid = employeeid;
    }

}
