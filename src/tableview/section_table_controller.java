/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableview;

/**
 *
 * @author isuru
 */
public class section_table_controller {

    private String Sectionid;
    private String Sectionname;
    private String Enterdate;
     private String Status;
     private String Employeeid;

    public section_table_controller(String Sectionid, String Sectionname, String Enterdate, String Status, String Employeeid) {
        this.Sectionid = Sectionid;
        this.Sectionname = Sectionname;
        this.Enterdate = Enterdate;
        this.Status = Status;
        this.Employeeid = Employeeid;
    }

    public String getSectionid() {
        return Sectionid;
    }

    public void setSectionid(String Sectionid) {
        this.Sectionid = Sectionid;
    }

    public String getSectionname() {
        return Sectionname;
    }

    public void setSectionname(String Sectionname) {
        this.Sectionname = Sectionname;
    }

    public String getEnterdate() {
        return Enterdate;
    }

    public void setEnterdate(String Enterdate) {
        this.Enterdate = Enterdate;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getEmployeeid() {
        return Employeeid;
    }

    public void setEmployeeid(String Employeeid) {
        this.Employeeid = Employeeid;
    }

    
}
