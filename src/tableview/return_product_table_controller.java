/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableview;

/**
 *
 * @author isuru
 */
public class return_product_table_controller {

    private int Invoiceitemid;
    private int Productid;
    private String Invoiceid;
    private String Productname;
    private String Qty;
    private double Subtotal;

    public return_product_table_controller(Integer Invoiceitemid, Integer Productid, String Invoiceid, String Productname, String Qty, double Subtotal) {
        this.Invoiceitemid = Invoiceitemid;
        this.Productid = Productid;
        this.Invoiceid = Invoiceid;
        this.Productname = Productname;
        this.Qty = Qty;
        this.Subtotal = Subtotal;
    }

    public int getInvoiceitemid() {
        return Invoiceitemid;
    }

    public void setInvoiitemceid(int Invoiceitemid) {
        this.Invoiceitemid = Invoiceitemid;
    }

    public int getProductid() {
        return Productid;
    }

    public void setProductid(int Productid) {
        this.Productid = Productid;
    }

    public String getInvoiceid() {
        return Invoiceid;
    }

    public void setInvoiceid(String Invoiceid) {
        this.Invoiceid = Invoiceid;
    }

    public String getProductname() {
        return Productname;
    }

    public void setProductname(String Productname) {
        this.Productname = Productname;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String Qty) {
        this.Qty = Qty;
    }

    public double getSubtotal() {
        return Subtotal;
    }

    public void setSubtotal(double Subtotal) {
        this.Subtotal = Subtotal;
    }

}
