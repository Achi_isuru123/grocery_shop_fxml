/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableview;

import com.jfoenix.controls.JFXTextField;

/**
 *
 * @author isuru
 */
public class price_table_controller {
    private  String Productid;
    private  String Barcodeid;
    private  String Productname;
    private  String Unitprice;
    private  String productqty;
    private  String Producttotal;
    private  String Productdiscount;
    private  String Subtotal;

    public price_table_controller(String Productid, String Barcodeid, String Productname, String Unitprice, String productqty, String Producttotal, String Productdiscount, String Subtotal) {
        this.Productid = Productid;
        this.Barcodeid = Barcodeid;
        this.Productname = Productname;
        this.Unitprice = Unitprice;
        this.productqty = productqty;
        this.Producttotal = Producttotal;
        this.Productdiscount = Productdiscount;
        this.Subtotal = Subtotal;
    }

    public String getProductid() {
        return Productid;
    }

    public void setProductid(String Productid) {
        this.Productid = Productid;
    }

    public String getBarcodeid() {
        return Barcodeid;
    }

    public void setBarcodeid(String Barcodeid) {
        this.Barcodeid = Barcodeid;
    }

    public String getProductname() {
        return Productname;
    }

    public void setProductname(String Productname) {
        this.Productname = Productname;
    }

    public String getUnitprice() {
        return Unitprice;
    }

    public void setUnitprice(String Unitprice) {
        this.Unitprice = Unitprice;
    }

    public String getProductqty() {
        return productqty;
    }

    public void setProductqty(String productqty) {
        this.productqty = productqty;
    }

    public String getProducttotal() {
        return Producttotal;
    }

    public void setProducttotal(String Producttotal) {
        this.Producttotal = Producttotal;
    }

    public String getProductdiscount() {
        return Productdiscount;
    }

    public void setProductdiscount(String Productdiscount) {
        this.Productdiscount = Productdiscount;
    }

    public String getSubtotal() {
        return Subtotal;
    }

    public void setSubtotal(String Subtotal) {
        this.Subtotal = Subtotal;
    }

    
}
