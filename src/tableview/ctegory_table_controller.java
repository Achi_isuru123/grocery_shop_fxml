/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableview;

/**
 *
 * @author isuru
 */
public class ctegory_table_controller {
    private String Categoryid;
    private String Categoryname;
    private String Categoryrackno;
    private String Enterdate;
    private String Status;
    private String Employeeid;

    public ctegory_table_controller(String Categoryid, String Categoryname, String Categoryrackno, String Enterdate, String Status, String Employeeid) {
        this.Categoryid = Categoryid;
        this.Categoryname = Categoryname;
        this.Categoryrackno = Categoryrackno;
        this.Enterdate = Enterdate;
        this.Status = Status;
        this.Employeeid = Employeeid;
    }

    public String getEmployeeid() {
        return Employeeid;
    }

    public void setEmployeeid(String Employeeid) {
        this.Employeeid = Employeeid;
    }

    public String getCategoryid() {
        return Categoryid;
    }

    public void setCategoryid(String Categoryid) {
        this.Categoryid = Categoryid;
    }

    public String getCategoryname() {
        return Categoryname;
    }

    public void setCategoryname(String Categoryname) {
        this.Categoryname = Categoryname;
    }

    public String getCategoryrackno() {
        return Categoryrackno;
    }

    public void setCategoryrackno(String Categoryrackno) {
        this.Categoryrackno = Categoryrackno;
    }

    public String getEnterdate() {
        return Enterdate;
    }

    public void setEnterdate(String Enterdate) {
        this.Enterdate = Enterdate;
    }

    public String getStatus() {
         if (Status.equals("1")) {
            String name = "Active";
            return name;
        } else {
            String name = "Inactive";
            return name;
        }
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }
    
}
