/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableview;

/**
 *
 * @author isuru
 */
public class employee_table_controller {
    private int Empid;
    private String Empname;
    private String Empemail;
    private String Username;
    private String Contectno;
    private String Section;
    private String Image;
    private String Registertimedate;
    private String status;

    public employee_table_controller(int Empid, String Empname, String Empemail, String Username, String Contectno, String Section, String Image, String Registertimedate, String status) {
        this.Empid = Empid;
        this.Empname = Empname;
        this.Empemail = Empemail;
        this.Username = Username;
        this.Contectno = Contectno;
        this.Section = Section;
        this.Image = Image;
        this.Registertimedate = Registertimedate;
        this.status = status;
    }

    public int getEmpid() {
        return Empid;
    }

    public void setEmpid(int Empid) {
        this.Empid = Empid;
    }

    public String getEmpname() {
        return Empname;
    }

    public void setEmpname(String Empname) {
        this.Empname = Empname;
    }

    public String getEmpemail() {
        return Empemail;
    }

    public void setEmpemail(String Empemail) {
        this.Empemail = Empemail;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getContectno() {
        return Contectno;
    }

    public void setContectno(String Contectno) {
        this.Contectno = Contectno;
    }

    public String getSection() {
        return Section;
    }

    public void setSection(String Section) {
        this.Section = Section;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String Image) {
        this.Image = Image;
    }

    public String getRegistertimedate() {
        return Registertimedate;
    }

    public void setRegistertimedate(String Registertimedate) {
        this.Registertimedate = Registertimedate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

   
}
