/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableview;

/**
 *
 * @author isuru
 */
public class expenditure_table_controller {
    private int Insid;
    private Double Totamount;
    private Double Spentamount;
    private String Description;
    private String Spentdate;
    private int Empid;

    public expenditure_table_controller(int Insid, Double Totamount, Double Spentamount, String Description, String Spentdate, int Empid) {
        this.Insid = Insid;
        this.Totamount = Totamount;
        this.Spentamount = Spentamount;
        this.Description = Description;
        this.Spentdate = Spentdate;
        this.Empid = Empid;
    }

    public int getInsid() {
        return Insid;
    }

    public void setInsid(int Insid) {
        this.Insid = Insid;
    }

    public Double getTotamount() {
        return Totamount;
    }

    public void setTotamount(Double Totamount) {
        this.Totamount = Totamount;
    }

    public Double getSpentamount() {
        return Spentamount;
    }

    public void setSpentamount(Double Spentamount) {
        this.Spentamount = Spentamount;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getSpentdate() {
        return Spentdate;
    }

    public void setSpentdate(String Spentdate) {
        this.Spentdate = Spentdate;
    }

    public int getEmpid() {
        return Empid;
    }

    public void setEmpid(int Empid) {
        this.Empid = Empid;
    }
}
