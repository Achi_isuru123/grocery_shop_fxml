/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableview;

/**
 *
 * @author isuru
 */
public class store_table_controller {

    private String Barcodeno;
    private String Productname;
    private String Description;
    private String Measurements;
    private Double Purchaseprice;
    private Double Standardcost;
    private String Unitinstock;
    private String Recoadlevel;
    private String Supplerid;
    private String Categoryid;
    private String Storesubcategoryname;
    private String Storerackno;
    private String Domdate;
    private String Exdate;
    private Double Unitprice;
    private String Qty;
    private Double Total;
    private String Enterdate;
    private String Enterupdate;
    private String Status;
    private String Empname;

    public store_table_controller(String Barcodeno, String Productname, String Description, String Measurements, Double Purchaseprice, Double Standardcost, String Unitinstock, String Recoadlevel, String Supplerid, String Categoryid, String Storesubcategoryname, String Storerackno, String Domdate, String Exdate, Double Unitprice, String Qty, Double Total, String Enterdate, String Enterupdate, String Status, String Empname) {
        this.Barcodeno = Barcodeno;
        this.Productname = Productname;
        this.Description = Description;
        this.Measurements = Measurements;
        this.Purchaseprice = Purchaseprice;
        this.Standardcost = Standardcost;
        this.Unitinstock = Unitinstock;
        this.Recoadlevel = Recoadlevel;
        this.Supplerid = Supplerid;
        this.Categoryid = Categoryid;
        this.Storesubcategoryname = Storesubcategoryname;
        this.Storerackno = Storerackno;
        this.Domdate = Domdate;
        this.Exdate = Exdate;
        this.Unitprice = Unitprice;
        this.Qty = Qty;
        this.Total = Total;
        this.Enterdate = Enterdate;
        this.Enterupdate = Enterupdate;
        this.Status = Status;
        this.Empname = Empname;
    }

    public String getBarcodeno() {
        return Barcodeno;
    }

    public void setBarcodeno(String Barcodeno) {
        this.Barcodeno = Barcodeno;
    }

    public String getProductname() {
        return Productname;
    }

    public void setProductname(String Productname) {
        this.Productname = Productname;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getMeasurements() {
        return Measurements;
    }

    public void setMeasurements(String Measurements) {
        this.Measurements = Measurements;
    }

    public Double getPurchaseprice() {
        return Purchaseprice;
    }

    public void setPurchaseprice(Double Purchaseprice) {
        this.Purchaseprice = Purchaseprice;
    }

    public Double getStandardcost() {
        return Standardcost;
    }

    public void setStandardcost(Double Standardcost) {
        this.Standardcost = Standardcost;
    }

    public String getUnitinstock() {
        return Unitinstock;
    }

    public void setUnitinstock(String Unitinstock) {
        this.Unitinstock = Unitinstock;
    }

    public String getRecoadlevel() {
        return Recoadlevel;
    }

    public void setRecoadlevel(String Recoadlevel) {
        this.Recoadlevel = Recoadlevel;
    }

    public String getSupplerid() {
        return Supplerid;
    }

    public void setSupplerid(String Supplerid) {
        this.Supplerid = Supplerid;
    }

    public String getCategoryid() {
        return Categoryid;
    }

    public void setCategoryid(String Categoryid) {
        this.Categoryid = Categoryid;
    }

    public String getStoresubcategoryname() {
        return Storesubcategoryname;
    }

    public void setStoresubcategoryname(String Storesubcategoryname) {
        this.Storesubcategoryname = Storesubcategoryname;
    }

    public String getStorerackno() {
        return Storerackno;
    }

    public void setStorerackno(String Storerackno) {
        this.Storerackno = Storerackno;
    }

    public String getDomdate() {
        return Domdate;
    }

    public void setDomdate(String Domdate) {
        this.Domdate = Domdate;
    }

    public String getExdate() {
        return Exdate;
    }

    public void setExdate(String Exdate) {
        this.Exdate = Exdate;
    }

    public Double getUnitprice() {
        return Unitprice;
    }

    public void setUnitprice(Double Unitprice) {
        this.Unitprice = Unitprice;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String Qty) {
        this.Qty = Qty;
    }

    public Double getTotal() {
        return Total;
    }

    public void setTotal(Double Total) {
        this.Total = Total;
    }

    public String getEnterdate() {
        return Enterdate;
    }

    public void setEnterdate(String Enterdate) {
        this.Enterdate = Enterdate;
    }

    public String getEnterupdate() {

        return Enterupdate;
    }

    public void setEnterupdate(String Enterupdate) {
        this.Enterupdate = Enterupdate;
    }

    public String getStatus() {
        if (Status.equals("1")) {
            String ac = "Active";
            return ac;
        } else {
            String ac = "Inactive";
            return ac;
        }

    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getEmpname() {
        return Empname;
    }

    public void setEmpname(String Empname) {
        this.Empname = Empname;
    }

}
