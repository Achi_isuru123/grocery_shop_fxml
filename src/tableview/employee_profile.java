/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableview;

/**
 *
 * @author fernando
 */
public class employee_profile {
    private String Empid;
    private String Empname;
    private String Empsection;
    private String empstatus;

    public employee_profile(String Empid, String Empname, String Empsection, String empstatus) {
        this.Empid = Empid;
        this.Empname = Empname;
        this.Empsection = Empsection;
        this.empstatus = empstatus;
    }

    public String getEmpid() {
        return Empid;
    }

    public void setEmpid(String Empid) {
        this.Empid = Empid;
    }

    public String getEmpname() {
        return Empname;
    }

    public void setEmpname(String Empname) {
        this.Empname = Empname;
    }

    public String getEmpsection() {
        return Empsection;
    }

    public void setEmpsection(String Empsection) {
        this.Empsection = Empsection;
    }

    public String getEmpstatus() {
        return empstatus;
    }

    public void setEmpstatus(String empstatus) {
        this.empstatus = empstatus;
    }
    
}
