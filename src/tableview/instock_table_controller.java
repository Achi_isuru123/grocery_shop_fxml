/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableview;

/**
 *
 * @author isuru
 */
public class instock_table_controller {

    private int Productid;
    private long Barcode;
    private String Productname;
    private String Mfdate;
    private String Exdate;
    private String Qty;
    private Double Retailprice;
    private Double Wholesaleprice;
    private Double Exterpirce;
    private String Transerdate;
    private String Enterupdate;
    private String Status;
    private int Empid;

    public instock_table_controller(int Productid, long Barcode, String Productname, String Mfdate, String Exdate, String Qty, Double Retailprice, Double Wholesaleprice, Double Exterpirce, String Transerdate, String Enterupdate, String Status, int Empid) {
        this.Productid = Productid;
        this.Barcode = Barcode;
        this.Productname = Productname;
        this.Mfdate = Mfdate;
        this.Exdate = Exdate;
        this.Qty = Qty;
        this.Retailprice = Retailprice;
        this.Wholesaleprice = Wholesaleprice;
        this.Exterpirce = Exterpirce;
        this.Transerdate = Transerdate;
        this.Enterupdate = Enterupdate;
        this.Status = Status;
        this.Empid = Empid;
    }

    public int getProductid() {
        return Productid;
    }

    public void setProductid(int Productid) {
        this.Productid = Productid;
    }

    public long getBarcode() {
        return Barcode;
    }

    public void setBarcode(long Barcode) {
        this.Barcode = Barcode;
    }

    public String getProductname() {
        return Productname;
    }

    public void setProductname(String Productname) {
        this.Productname = Productname;
    }

    public String getMfdate() {
        return Mfdate;
    }

    public void setMfdate(String Mfdate) {
        this.Mfdate = Mfdate;
    }

    public String getExdate() {
        return Exdate;
    }

    public void setExdate(String Exdate) {
        this.Exdate = Exdate;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String Qty) {
        this.Qty = Qty;
    }

    public Double getRetailprice() {
        return Retailprice;
    }

    public void setRetailprice(Double Retailprice) {
        this.Retailprice = Retailprice;
    }

    public Double getWholesaleprice() {
        return Wholesaleprice;
    }

    public void setWholesaleprice(Double Wholesaleprice) {
        this.Wholesaleprice = Wholesaleprice;
    }

    public Double getExterpirce() {
        return Exterpirce;
    }

    public void setExterpirce(Double Exterpirce) {
        this.Exterpirce = Exterpirce;
    }

    public String getTranserdate() {
        return Transerdate;
    }

    public void setTranserdate(String Transerdate) {
        this.Transerdate = Transerdate;
    }

    public String getEnterupdate() {
        return Enterupdate;
    }

    public void setEnterupdate(String Enterupdate) {
        this.Enterupdate = Enterupdate;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public int getEmpid() {
        return Empid;
    }

    public void setEmpid(int Empid) {
        this.Empid = Empid;
    }

}
