/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableview;

/**
 *
 * @author isuru
 */
public class selles_table_controller {
    private String Invoiceno;
    private String Customerid;
    private String Date;
    private Double Total;
    private Double Discount;
    private Double Nettotal;
    private Double Payment;
    private Double Balance;
    private String Paymenttype;
    private String paymentmethod;

    public selles_table_controller(String Invoiceno, String Customerid, String Date, Double Total, Double Discount, Double Nettotal, Double Payment, Double Balance, String Paymenttype, String paymentmethod) {
        this.Invoiceno = Invoiceno;
        this.Customerid = Customerid;
        this.Date = Date;
        this.Total = Total;
        this.Discount = Discount;
        this.Nettotal = Nettotal;
        this.Payment = Payment;
        this.Balance = Balance;
        this.Paymenttype = Paymenttype;
        this.paymentmethod = paymentmethod;
    }

    public selles_table_controller(String string, String string0, String string1, double aDouble, double aDouble0, double aDouble1, double aDouble2, double aDouble3, double aDouble4, String string2, String string3) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getInvoiceno() {
        return Invoiceno;
    }

    public void setInvoiceno(String Invoiceno) {
        this.Invoiceno = Invoiceno;
    }

    public String getCustomerid() {
        return Customerid;
    }

    public void setCustomerid(String Customerid) {
        this.Customerid = Customerid;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public Double getTotal() {
        return Total;
    }

    public void setTotal(Double Total) {
        this.Total = Total;
    }

    public Double getDiscount() {
        return Discount;
    }

    public void setDiscount(Double Discount) {
        this.Discount = Discount;
    }

    public Double getNettotal() {
        return Nettotal;
    }

    public void setNettotal(Double Nettotal) {
        this.Nettotal = Nettotal;
    }

    public Double getPayment() {
        return Payment;
    }

    public void setPayment(Double Payment) {
        this.Payment = Payment;
    }

    public Double getBalance() {
        return Balance;
    }

    public void setBalance(Double Balance) {
        this.Balance = Balance;
    }

    public String getPaymenttype() {
        return Paymenttype;
    }

    public void setPaymenttype(String Paymenttype) {
        this.Paymenttype = Paymenttype;
    }

    public String getPaymentmethod() {
        return paymentmethod;
    }

    public void setPaymentmethod(String paymentmethod) {
        this.paymentmethod = paymentmethod;
    }
    
}
