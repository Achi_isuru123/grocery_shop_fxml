/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableview;

/**
 *
 * @author isuru
 */
public class expired_product_table_controller {

    private int Productid;
    private String Productname;
    private String Exdate;
    private String Qty;
    private double Purchaseprice;
    private String Total;

    public expired_product_table_controller(int Productid, String Productname, String Exdate, String Qty, double Purchaseprice, String Total) {
        this.Productid = Productid;
        this.Productname = Productname;
        this.Exdate = Exdate;
        this.Qty = Qty;
        this.Purchaseprice = Purchaseprice;
        this.Total = Total;
    }

    public int getProductid() {
        return Productid;
    }

    public void setProductid(int Productid) {
        this.Productid = Productid;
    }

    public String getProductname() {
        return Productname;
    }

    public void setProductname(String Productname) {
        this.Productname = Productname;
    }

    public String getExdate() {
        return Exdate;
    }

    public void setExdate(String Exdate) {
        this.Exdate = Exdate;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String Qty) {
        this.Qty = Qty;
    }

    public double getPurchaseprice() {
        return Purchaseprice;
    }

    public void setPurchaseprice(double Purchaseprice) {
        this.Purchaseprice = Purchaseprice;
    }

    public String getTotal() {
        return Total;
    }

    public void setTotal(String Total) {
        this.Total = Total;
    }

    
   
}
