/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableview;

/**
 *
 * @author isuru
 */
public class log_table_controller {
    private String Empid;
    private String Empname;
    private String Logdate;
    private String Logtime;

    public log_table_controller(String Empid, String Empname, String Logdate, String Logtime) {
        this.Empid = Empid;
        this.Empname = Empname;
        this.Logdate = Logdate;
        this.Logtime = Logtime;
    }

    public String getEmpid() {
        return Empid;
    }

    public void setEmpid(String Empid) {
        this.Empid = Empid;
    }

    public String getEmpname() {
        return Empname;
    }

    public void setEmpname(String Empname) {
        this.Empname = Empname;
    }

    public String getLogdate() {
        return Logdate;
    }

    public void setLogdate(String Logdate) {
        this.Logdate = Logdate;
    }

    public String getLogtime() {
        return Logtime;
    }

    public void setLogtime(String Logtime) {
        this.Logtime = Logtime;
    }
    
}
