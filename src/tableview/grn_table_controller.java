/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableview;

/**
 *
 * @author isuru
 */
public class grn_table_controller {
    private int Grnid;
    private String Supplerid;
    private String Supplername;
    private int Productid;
    private String Productname;
    private String Unitinstock;
    private String Unitinstore;
    private String Categoryid;
    private String Categoryname;
    private String Qty;
    private double Price;
    private double Total;
    private double Descount;
    private double Nettotal;
    private String Enterdate;
    private String Approvad;
    private int Empid;

    public grn_table_controller(int Grnid, String Supplerid, String Supplername, int Productid, String Productname, String Unitinstock, String Unitinstore, String Categoryid, String Categoryname, String Qty, double Price, double Total, double Descount, double Nettotal, String Enterdate, String Approvad, int Empid) {
        this.Grnid = Grnid;
        this.Supplerid = Supplerid;
        this.Supplername = Supplername;
        this.Productid = Productid;
        this.Productname = Productname;
        this.Unitinstock = Unitinstock;
        this.Unitinstore = Unitinstore;
        this.Categoryid = Categoryid;
        this.Categoryname = Categoryname;
        this.Qty = Qty;
        this.Price = Price;
        this.Total = Total;
        this.Descount = Descount;
        this.Nettotal = Nettotal;
        this.Enterdate = Enterdate;
        this.Approvad = Approvad;
        this.Empid = Empid;
    }

    public int getGrnid() {
        return Grnid;
    }

    public void setGrnid(int Grnid) {
        this.Grnid = Grnid;
    }

    public String getSupplerid() {
        return Supplerid;
    }

    public void setSupplerid(String Supplerid) {
        this.Supplerid = Supplerid;
    }

    public String getSupplername() {
        return Supplername;
    }

    public void setSupplername(String Supplername) {
        this.Supplername = Supplername;
    }

    public int getProductid() {
        return Productid;
    }

    public void setProductid(int Productid) {
        this.Productid = Productid;
    }

    public String getProductname() {
        return Productname;
    }

    public void setProductname(String Productname) {
        this.Productname = Productname;
    }

    public String getUnitinstock() {
        return Unitinstock;
    }

    public void setUnitinstock(String Unitinstock) {
        this.Unitinstock = Unitinstock;
    }

    public String getUnitinstore() {
        return Unitinstore;
    }

    public void setUnitinstore(String Unitinstore) {
        this.Unitinstore = Unitinstore;
    }

    public String getCategoryid() {
        return Categoryid;
    }

    public void setCategoryid(String Categoryid) {
        this.Categoryid = Categoryid;
    }

    public String getCategoryname() {
        return Categoryname;
    }

    public void setCategoryname(String Categoryname) {
        this.Categoryname = Categoryname;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String Qty) {
        this.Qty = Qty;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double Price) {
        this.Price = Price;
    }

    public double getTotal() {
        return Total;
    }

    public void setTotal(double Total) {
        this.Total = Total;
    }

    public double getDescount() {
        return Descount;
    }

    public void setDescount(double Descount) {
        this.Descount = Descount;
    }

    public double getNettotal() {
        return Nettotal;
    }

    public void setNettotal(double Nettotal) {
        this.Nettotal = Nettotal;
    }

    public String getEnterdate() {
        return Enterdate;
    }

    public void setEnterdate(String Enterdate) {
        this.Enterdate = Enterdate;
    }

    public String getApprovad() {
        return Approvad;
    }

    public void setApprovad(String Approvad) {
        this.Approvad = Approvad;
    }

    public int getEmpid() {
        return Empid;
    }

    public void setEmpid(int Empid) {
        this.Empid = Empid;
    }

   
    
}
