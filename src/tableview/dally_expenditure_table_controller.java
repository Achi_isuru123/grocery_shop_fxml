/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableview;

/**
 *
 * @author isuru
 */
public class dally_expenditure_table_controller {
    private int Number;
    private Double Spentamont;
    private int Empid;

    public dally_expenditure_table_controller(int Number, Double Spentamont, int Empid) {
        this.Number = Number;
        this.Spentamont = Spentamont;
        this.Empid = Empid;
    }

    public int getNumber() {
        return Number;
    }

    public void setNumber(int Number) {
        this.Number = Number;
    }

    public Double getSpentamont() {
        return Spentamont;
    }

    public void setSpentamont(Double Spentamont) {
        this.Spentamont = Spentamont;
    }

    public int getEmpid() {
        return Empid;
    }

    public void setEmpid(int Empid) {
        this.Empid = Empid;
    }
    
}
