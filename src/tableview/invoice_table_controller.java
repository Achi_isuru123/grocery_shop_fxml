/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableview;

/**
 *
 * @author isuru
 */
public class invoice_table_controller {
    private String Invoiceno;
    private String Customerid;
    private String Enterdate;
    private Double Total;
    private Double Descount2;
    private Double Nettotal;
    private Double Payment;
    private Double Blance;
    private String Pricetype;
    private String Paymentmethod;
    private int Empid;

    public invoice_table_controller(String Invoiceno, String Customerid, String Enterdate, Double Total, Double Descount2, Double Nettotal, Double Payment, Double Blance, String Pricetype, String Paymentmethod, int Empid) {
        this.Invoiceno = Invoiceno;
        this.Customerid = Customerid;
        this.Enterdate = Enterdate;
        this.Total = Total;
        this.Descount2 = Descount2;
        this.Nettotal = Nettotal;
        this.Payment = Payment;
        this.Blance = Blance;
        this.Pricetype = Pricetype;
        this.Paymentmethod = Paymentmethod;
        this.Empid = Empid;
    }

    public String getInvoiceno() {
        return Invoiceno;
    }

    public void setInvoiceno(String Invoiceno) {
        this.Invoiceno = Invoiceno;
    }

    public String getCustomerid() {
        return Customerid;
    }

    public void setCustomerid(String Customerid) {
        this.Customerid = Customerid;
    }

    public String getEnterdate() {
        return Enterdate;
    }

    public void setEnterdate(String Enterdate) {
        this.Enterdate = Enterdate;
    }

    public Double getTotal() {
        return Total;
    }

    public void setTotal(Double Total) {
        this.Total = Total;
    }

    public Double getDescount2() {
        return Descount2;
    }

    public void setDescount2(Double Descount2) {
        this.Descount2 = Descount2;
    }

    public Double getNettotal() {
        return Nettotal;
    }

    public void setNettotal(Double Nettotal) {
        this.Nettotal = Nettotal;
    }

    public Double getPayment() {
        return Payment;
    }

    public void setPayment(Double Payment) {
        this.Payment = Payment;
    }

    public Double getBlance() {
        return Blance;
    }

    public void setBlance(Double Blance) {
        this.Blance = Blance;
    }

    public String getPricetype() {
        return Pricetype;
    }

    public void setPricetype(String Pricetype) {
        this.Pricetype = Pricetype;
    }

    public String getPaymentmethod() {
        return Paymentmethod;
    }

    public void setPaymentmethod(String Paymentmethod) {
        this.Paymentmethod = Paymentmethod;
    }

    public int getEmpid() {
        return Empid;
    }

    public void setEmpid(int Empid) {
        this.Empid = Empid;
    }
}
