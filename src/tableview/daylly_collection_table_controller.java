/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableview;

/**
 *
 * @author isuru
 */
public class daylly_collection_table_controller {
    private String Collectiondate;
    private double Cashamount;
    private double Cardamount;
    private double Creditamount;
    private double InstitutionalAmount;
    private double Changeitemamount;
    private double Damageitemamount;
    private double EXitemamount;
    private double Totalamount;
    private double Enteramount;
    private int Empid;

    public daylly_collection_table_controller(String Collectiondate, double Cashamount, double Cardamount, double Creditamount, double InstitutionalAmount, double Changeitemamount, double Damageitemamount, double EXitemamount, double Totalamount, double Enteramount, int Empid) {
        this.Collectiondate = Collectiondate;
        this.Cashamount = Cashamount;
        this.Cardamount = Cardamount;
        this.Creditamount = Creditamount;
        this.InstitutionalAmount = InstitutionalAmount;
        this.Changeitemamount = Changeitemamount;
        this.Damageitemamount = Damageitemamount;
        this.EXitemamount = EXitemamount;
        this.Totalamount = Totalamount;
        this.Enteramount = Enteramount;
        this.Empid = Empid;
    }

    public String getCollectiondate() {
        return Collectiondate;
    }

    public void setCollectiondate(String Collectiondate) {
        this.Collectiondate = Collectiondate;
    }

    public double getCashamount() {
        return Cashamount;
    }

    public void setCashamount(double Cashamount) {
        this.Cashamount = Cashamount;
    }

    public double getCardamount() {
        return Cardamount;
    }

    public void setCardamount(double Cardamount) {
        this.Cardamount = Cardamount;
    }

    public double getCreditamount() {
        return Creditamount;
    }

    public void setCreditamount(double Creditamount) {
        this.Creditamount = Creditamount;
    }

    public double getInstitutionalAmount() {
        return InstitutionalAmount;
    }

    public void setInstitutionalAmount(double InstitutionalAmount) {
        this.InstitutionalAmount = InstitutionalAmount;
    }

    public double getChangeitemamount() {
        return Changeitemamount;
    }

    public void setChangeitemamount(double Changeitemamount) {
        this.Changeitemamount = Changeitemamount;
    }

    public double getDamageitemamount() {
        return Damageitemamount;
    }

    public void setDamageitemamount(double Damageitemamount) {
        this.Damageitemamount = Damageitemamount;
    }

    public double getEXitemamount() {
        return EXitemamount;
    }

    public void setEXitemamount(double EXitemamount) {
        this.EXitemamount = EXitemamount;
    }

    public double getTotalamount() {
        return Totalamount;
    }

    public void setTotalamount(double Totalamount) {
        this.Totalamount = Totalamount;
    }

    public double getEnteramount() {
        return Enteramount;
    }

    public void setEnteramount(double Enteramount) {
        this.Enteramount = Enteramount;
    }

    public int getEmpid() {
        return Empid;
    }

    public void setEmpid(int Empid) {
        this.Empid = Empid;
    }

  
}
