/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableview;

/**
 *
 * @author isuru
 */
public class carddally_collection_table_controller {

    private String Invoiceno;
    private Double Nettotal;
    private String Paymentmethod;

    public carddally_collection_table_controller(String Invoiceno, Double Nettotal, String Paymentmethod) {
        this.Invoiceno = Invoiceno;
        this.Nettotal = Nettotal;
        this.Paymentmethod = Paymentmethod;
    }

    public String getInvoiceno() {
        return Invoiceno;
    }

    public void setInvoiceno(String Invoiceno) {
        this.Invoiceno = Invoiceno;
    }

    public Double getNettotal() {
        return Nettotal;
    }

    public void setNettotal(Double Nettotal) {
        this.Nettotal = Nettotal;
    }

    public String getPaymentmethod() {
        return Paymentmethod;
    }

    public void setPaymentmethod(String Paymentmethod) {
        this.Paymentmethod = Paymentmethod;
    }
}
