/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableview;

/**
 *
 * @author isuru
 */
public class invoiceitem_table_controller {
    private int Invoiceitemid;
    private String Invoiceid;
    private long Barcodeid;
    private String Itemname;
    private Double Unitprice;
    private String Qty;
    private Double Itemtotal;
    private Double Discount;
    private Double Subtotal;

    public invoiceitem_table_controller(int Invoiceitemid, String Invoiceid, long Barcodeid, String Itemname, Double Unitprice, String Qty, Double Itemtotal, Double Discount, Double Subtotal) {
        this.Invoiceitemid = Invoiceitemid;
        this.Invoiceid = Invoiceid;
        this.Barcodeid = Barcodeid;
        this.Itemname = Itemname;
        this.Unitprice = Unitprice;
        this.Qty = Qty;
        this.Itemtotal = Itemtotal;
        this.Discount = Discount;
        this.Subtotal = Subtotal;
    }

    public int getInvoiceitemid() {
        return Invoiceitemid;
    }

    public void setInvoiceitemid(int Invoiceitemid) {
        this.Invoiceitemid = Invoiceitemid;
    }

    public String getInvoiceid() {
        return Invoiceid;
    }

    public void setInvoiceid(String Invoiceid) {
        this.Invoiceid = Invoiceid;
    }

    public long getProductid() {
        return Barcodeid;
    }

    public void setProductid(long Productid) {
        this.Barcodeid = Barcodeid;
    }

    public String getItemname() {
        return Itemname;
    }

    public void setItemname(String Itemname) {
        this.Itemname = Itemname;
    }

    public Double getUnitprice() {
        return Unitprice;
    }

    public void setUnitprice(Double Unitprice) {
        this.Unitprice = Unitprice;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String Qty) {
        this.Qty = Qty;
    }

    public Double getItemtotal() {
        return Itemtotal;
    }

    public void setItemtotal(Double Itemtotal) {
        this.Itemtotal = Itemtotal;
    }

    public Double getDiscount() {
        return Discount;
    }

    public void setDiscount(Double Discount) {
        this.Discount = Discount;
    }

    public Double getSubtotal() {
        return Subtotal;
    }

    public void setSubtotal(Double Subtotal) {
        this.Subtotal = Subtotal;
    }
    
    
}
