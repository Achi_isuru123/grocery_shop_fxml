/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

/**
 *
 * @author isuru
 */
public class systemcomfigdata {
    private static String empid;
    private static String Activeuser;
    private static String usertype;
    private static String fullname;
    private static String imagepath;

    public static String getActiveuser() {
        return Activeuser;
    }

    public static void setActiveuser(String aActiveuser) {
        Activeuser = aActiveuser;
    }

    public static String getUsertype() {
        return usertype;
    }

    public static void setUsertype(String aUsertype) {
        usertype = aUsertype;
    }

    public static String getFullname() {
        return fullname;
    }

    public static void setFullname(String aFullname) {
        fullname = aFullname;
    }

    public static String getImagepath() {
        return imagepath;
    }

    public static void setImagepath(String aImagepath) {
        imagepath = aImagepath;
    }

    public static String getEmpid() {
        return empid;
    }

    public static void setEmpid(String aEmpid) {
        empid = aEmpid;
    }
}
