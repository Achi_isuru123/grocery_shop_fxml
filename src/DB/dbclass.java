/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author isuru
 */
public class dbclass {

    private static Connection connection = null;

    public static void init_db() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3307/grocery_shop_fxml", "root", "achi19980815");
    }

    public static void push(String sql) throws Exception {
        if (connection == null) {
            init_db();
        }
        connection.createStatement().executeUpdate(sql);
    }

    public static ResultSet search(String sql) throws Exception {
        if (connection == null) {
            init_db();
        }
        return connection.createStatement().executeQuery(sql);
    }

    public static Connection getnewconnection() throws Exception {
        if (connection == null) {
            init_db();
        }
        return connection;
    }
}
