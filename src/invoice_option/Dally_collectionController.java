/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package invoice_option;

import DB.dbclass;
import DB.systemcomfigdata;
import java.net.URL;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import tableview.carddally_collection_table_controller;
import tableview.creditdally_table_collection;
import tableview.dally_expenditure_table_controller;
import tableview.dallycollection_table_controller;

/**
 * FXML Controller class
 *
 * @author isuru
 */
public class Dally_collectionController implements Initializable {

    @FXML
    private TableView<tableview.dallycollection_table_controller> table1;
    @FXML
    private TableColumn<tableview.dallycollection_table_controller, String> inv1;
    @FXML
    private TableColumn<tableview.dallycollection_table_controller, Double> net1;
    @FXML
    private TableColumn<tableview.dallycollection_table_controller, String> pay1;
    ObservableList<tableview.dallycollection_table_controller> Tblelist1 = FXCollections.observableArrayList();
    @FXML
    private TableView<tableview.carddally_collection_table_controller> table2;
    @FXML
    private TableColumn<tableview.carddally_collection_table_controller, String> inv2;
    @FXML
    private TableColumn<tableview.carddally_collection_table_controller, Double> net2;
    @FXML
    private TableColumn<tableview.carddally_collection_table_controller, String> pay2;
    ObservableList<tableview.carddally_collection_table_controller> Tblelist2 = FXCollections.observableArrayList();
    @FXML
    private TableView<tableview.creditdally_table_collection> table3;
    @FXML
    private TableColumn<tableview.creditdally_table_collection, String> inv3;
    @FXML
    private TableColumn<tableview.creditdally_table_collection, Double> net3;
    @FXML
    private TableColumn<tableview.creditdally_table_collection, String> pay3;
    ObservableList<tableview.creditdally_table_collection> Tblelist3 = FXCollections.observableArrayList();
    @FXML
    private TextField cashpayment;
    @FXML
    private TextField cardpayment;
    @FXML
    private TextField cerditpayment;
    @FXML
    private TextField total;
    @FXML
    private TextField entercollection;

    Date d = new Date();
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
    @FXML
    private TextField spentamount;
    @FXML
    private TableView<tableview.dally_expenditure_table_controller> table4;
    @FXML
    private TableColumn<tableview.dally_expenditure_table_controller, Integer> c_num;
    @FXML
    private TableColumn<tableview.dally_expenditure_table_controller, Double> c_spent;
    @FXML
    private TableColumn<tableview.dally_expenditure_table_controller, Integer> c_emp;
    ObservableList<tableview.dally_expenditure_table_controller> Tblelist4 = FXCollections.observableArrayList();
    @FXML
    private TextField examount;
    @FXML
    private TextField damageamount;
    @FXML
    private TextField cahgeamount;
    @FXML
    private DatePicker date_piker;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        table1load();
        table2load();
        table3load();
        table4load();
        setcell1();
        setcell2();
        setcell3();
        setcell4();
        sumvalue();
        ReturnAmount();
        ExpiredProduct();
    }

    @FXML
    private void btnsubmit(ActionEvent event) {
        if (chackdate()) {
            Alert aleet = new Alert(Alert.AlertType.INFORMATION);
            aleet.setTitle("Daylly Collection");
            aleet.setHeaderText("All transactions made today have been not saved");
            aleet.setContentText(date.format(d));
            aleet.showAndWait();
        } else {
            dateinsert();
        }

    }

    public void table1load() {
        try {
            ResultSet rs = dbclass.search("select * from invoice");
            while (rs.next()) {
                String DAte1 = rs.getString("enterdate");
                String DAte2 = date.format(d);
                DAte1 += " 00:00:00";
                DAte2 += " 00:00:00";
                if (DAte1.trim().equals(DAte2)) {
                    System.out.println(DAte1);
                    System.out.println(DAte2);
                    String Paymethod = rs.getString("paymentmethod");
                    if (Paymethod.trim().equals("Cash")) {
                        Tblelist1.add(new dallycollection_table_controller(
                                rs.getString("invoicid"),
                                rs.getDouble("nettotal"),
                                rs.getString("paymentmethod")));
                    }
                }

                table1.setItems(Tblelist1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setcell1() {
        inv1.setCellValueFactory(new PropertyValueFactory<>("Invoiceno"));
        net1.setCellValueFactory(new PropertyValueFactory<>("Nettotal"));
        pay1.setCellValueFactory(new PropertyValueFactory<>("Paymentmethod"));
    }

    public void table2load() {
        try {
            ResultSet rs = dbclass.search("select * from invoice");
            while (rs.next()) {
                String DAte1 = rs.getString("enterdate");
                String DAte2 = date.format(d);
                DAte1 += " 00:00:00";
                DAte2 += " 00:00:00";
                if (DAte1.trim().equals(DAte2)) {
                    String Paymethod = rs.getString("paymentmethod");
                    if (Paymethod.trim().equals("Card")) {
                        Tblelist2.add(new carddally_collection_table_controller(
                                rs.getString("invoicid"),
                                rs.getDouble("nettotal"),
                                rs.getString("paymentmethod")));
                    }
                }

                table2.setItems(Tblelist2);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setcell2() {
        inv2.setCellValueFactory(new PropertyValueFactory<>("Invoiceno"));
        net2.setCellValueFactory(new PropertyValueFactory<>("Nettotal"));
        pay2.setCellValueFactory(new PropertyValueFactory<>("Paymentmethod"));
    }

    public void table3load() {
        try {
            ResultSet rs = dbclass.search("select * from invoice");
            while (rs.next()) {
                String DAte1 = rs.getString("enterdate");
                String DAte2 = date.format(d);
                DAte1 += " 00:00:00";
                DAte2 += " 00:00:00";
                if (DAte1.trim().equals(DAte2)) {
                    String Paymethod = rs.getString("paymentmethod");
                    if (Paymethod.trim().equals("Credit Seller")) {
                        Tblelist3.add(new creditdally_table_collection(
                                rs.getString("invoicid"),
                                rs.getDouble("nettotal"),
                                rs.getString("paymentmethod")));
                    }
                }
                table3.setItems(Tblelist3);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setcell3() {
        inv3.setCellValueFactory(new PropertyValueFactory<>("Invoiceno"));
        net3.setCellValueFactory(new PropertyValueFactory<>("Nettotal"));
        pay3.setCellValueFactory(new PropertyValueFactory<>("Paymentmethod"));
    }

    public void table4load() {
        try {
            ResultSet rs = dbclass.search("select * from institutional");
            while (rs.next()) {
                String DAte1 = rs.getString("spentdate");
                String DAte2 = date.format(d);
                DAte1 += " 00:00:00";
                DAte2 += " 00:00:00";
                if (DAte1.trim().equals(DAte2)) {
                    Tblelist4.add(new dally_expenditure_table_controller(
                            rs.getInt("Institutionalid"),
                            rs.getDouble("spentamount"),
                            rs.getInt("empid")));

                }
                table4.setItems(Tblelist4);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setcell4() {
        c_num.setCellValueFactory(new PropertyValueFactory<>("Number"));
        c_spent.setCellValueFactory(new PropertyValueFactory<>("Spentamont"));
        c_emp.setCellValueFactory(new PropertyValueFactory<>("Empid"));
    }

    public void sumvalue() {
        try {
            table1.getSelectionModel().selectFirst();
            table2.getSelectionModel().selectFirst();
            table3.getSelectionModel().selectFirst();
            table4.getSelectionModel().selectFirst();
            double sum1 = 0;
            double sum2 = 0;
            double sum3 = 0;
            double sum4 = 0;
            int Item1 = table1.getItems().size();
            int Item2 = table2.getItems().size();
            int Item3 = table3.getItems().size();
            int Item4 = table4.getItems().size();
            for (int tab1 = 0; tab1 < Item1; tab1++) {
                table1.getSelectionModel().select(tab1);
                double selectitem1 = table1.getSelectionModel().getSelectedItem().getNettotal();
                sum1 = sum1 + selectitem1;
            }
            cashpayment.setText("LKR " + sum1);
            for (int tab2 = 0; tab2 < Item2; tab2++) {
                table2.getSelectionModel().select(tab2);
                double selectitem2 = table2.getSelectionModel().getSelectedItem().getNettotal();
                sum2 = sum2 + selectitem2;
            }
            cardpayment.setText("LKR " + sum2);
            for (int tab3 = 0; tab3 < Item3; tab3++) {
                table3.getSelectionModel().select(tab3);
                double selectitem3 = table3.getSelectionModel().getSelectedItem().getNettotal();
                sum3 = sum3 + selectitem3;
            }
            cerditpayment.setText("LKR " + sum3);
            for (int tab4 = 0; tab4 < Item4; tab4++) {
                table4.getSelectionModel().select(tab4);
                double selectitem4 = table4.getSelectionModel().getSelectedItem().getSpentamont();
                sum4 = sum4 + selectitem4;
            }
            spentamount.setText("LKR " + sum4);
            double value = (sum1);
            DecimalFormat df = new DecimalFormat("#.##");
            String format = df.format(value);
            total.setText("LKR " + format);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ReturnAmount() {
        try {
            double sum1 = 0;
            double sum2 = 0;
            System.out.println("AB");
            ResultSet rs = dbclass.search("select * from return_product");
            while (rs.next()) {
                String DAte1 = rs.getString("enterdate");
                String DAte2 = date.format(d);
                DAte1 += " 00:00:00";
                DAte2 += " 00:00:00";
                System.out.println(DAte1 + " " + DAte2);
                if (DAte1.trim().equals(DAte2)) {
                    String Returntype = rs.getString("returntype");
                    if (Returntype.trim().equals("Change Item")) {
                        double Changeproamount = rs.getDouble("itemtotal");
                        sum1 = sum1 + Changeproamount;
                        DecimalFormat df = new DecimalFormat("#.##");
                        String fromat = df.format(sum1);
                        cahgeamount.setText("LKR " + fromat);
                        System.out.println("A");
                    } else {
                        double Changeproamount = rs.getDouble("itemtotal");
                        sum1 = sum1 + Changeproamount;
                        DecimalFormat df = new DecimalFormat("#.##");
                        String fromat = df.format(sum1);
                        damageamount.setText("LKR " + fromat);
                        System.out.println("AA");
                    }
                } else {
                    cahgeamount.setText("LKR 0.00");
                    damageamount.setText("LKR 0.00");
                    System.out.println("AAA");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ExpiredProduct() {
        try {
            double sum1 = 0;
            ResultSet rs = dbclass.search("select * from expierd_product");
            while (rs.next()) {
                String DAte1 = rs.getString("enterdate");
                String DAte2 = date.format(d);
                DAte1 += " 00:00:00";
                DAte2 += " 00:00:00";
                if (DAte1.trim().equals(DAte2)) {
                    double ExAmount = rs.getDouble("qtytotal");
                    sum1 = sum1 + ExAmount;
                    DecimalFormat df = new DecimalFormat("#.##");
                    String fom = df.format(sum1);
                    examount.setText("LKR " + fom);
                    System.out.println("BB");
                } else {
                    examount.setText("LKR 0.00");
                    System.out.println("BBB");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_search(ActionEvent event) {
        table1.getItems().clear();
        table2.getItems().clear();
        table3.getItems().clear();
        table4.getItems().clear();

        try {
            ResultSet rs = dbclass.search("select * from invoice");
            while (rs.next()) {
                String DAte1 = rs.getString("enterdate");
                String DAte2 = date_piker.getValue().toString();
                DAte1 += " 00:00:00";
                DAte2 += " 00:00:00";
                if (DAte1.trim().equals(DAte2)) {
                    System.out.println(DAte1);
                    System.out.println(DAte2);
                    String Paymethod = rs.getString("paymentmethod");
                    if (Paymethod.trim().equals("Cash")) {
                        Tblelist1.add(new dallycollection_table_controller(
                                rs.getString("invoicid"),
                                rs.getDouble("nettotal"),
                                rs.getString("paymentmethod")));
                    }
                }

                table1.setItems(Tblelist1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            ResultSet rs = dbclass.search("select * from invoice");
            while (rs.next()) {
                String DAte1 = rs.getString("enterdate");
                String DAte2 = date_piker.getValue().toString();
                DAte1 += " 00:00:00";
                DAte2 += " 00:00:00";
                if (DAte1.trim().equals(DAte2)) {
                    String Paymethod = rs.getString("paymentmethod");
                    if (Paymethod.trim().equals("Card")) {
                        Tblelist2.add(new carddally_collection_table_controller(
                                rs.getString("invoicid"),
                                rs.getDouble("nettotal"),
                                rs.getString("paymentmethod")));
                    }
                }

                table2.setItems(Tblelist2);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            ResultSet rs = dbclass.search("select * from invoice");
            while (rs.next()) {
                String DAte1 = rs.getString("enterdate");
                String DAte2 = date_piker.getValue().toString();
                DAte1 += " 00:00:00";
                DAte2 += " 00:00:00";
                if (DAte1.trim().equals(DAte2)) {
                    String Paymethod = rs.getString("paymentmethod");
                    if (Paymethod.trim().equals("Credit Seller")) {
                        Tblelist3.add(new creditdally_table_collection(
                                rs.getString("invoicid"),
                                rs.getDouble("nettotal"),
                                rs.getString("paymentmethod")));
                    }
                }
                table3.setItems(Tblelist3);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            ResultSet rs = dbclass.search("select * from institutional");
            while (rs.next()) {
                String DAte1 = rs.getString("spentdate");
                String DAte2 = date_piker.getValue().toString();
                DAte1 += " 00:00:00";
                DAte2 += " 00:00:00";
                if (DAte1.trim().equals(DAte2)) {
                    Tblelist4.add(new dally_expenditure_table_controller(
                            rs.getInt("Institutionalid"),
                            rs.getDouble("spentamount"),
                            rs.getInt("empid")));

                }
                table4.setItems(Tblelist4);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            double sum1 = 0;
            double sum2 = 0;
            System.out.println("AB");
            ResultSet rs = dbclass.search("select * from return_product");
            while (rs.next()) {
                String DAte1 = rs.getString("enterdate");
                String DAte2 = date_piker.getValue().toString();
                DAte1 += " 00:00:00";
                DAte2 += " 00:00:00";
                System.out.println(DAte1 + " " + DAte2);
                if (DAte1.trim().equals(DAte2)) {
                    String Returntype = rs.getString("returntype");
                    if (Returntype.trim().equals("Change Item")) {
                        double Changeproamount = rs.getDouble("itemtotal");
                        sum1 = sum1 + Changeproamount;
                        DecimalFormat df = new DecimalFormat("#.##");
                        String fromat = df.format(sum1);
                        cahgeamount.setText("LKR " + fromat);
                        System.out.println("A");
                    } else {
                        double Changeproamount = rs.getDouble("itemtotal");
                        sum1 = sum1 + Changeproamount;
                        DecimalFormat df = new DecimalFormat("#.##");
                        String fromat = df.format(sum1);
                        damageamount.setText("LKR " + fromat);
                        System.out.println("AA");
                    }
                } else {
                    cahgeamount.setText("LKR 0.00");
                    damageamount.setText("LKR 0.00");
                    System.out.println("AAA");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            double sum1 = 0;
            ResultSet rs = dbclass.search("select * from expierd_product");
            while (rs.next()) {
                String DAte1 = rs.getString("enterdate");
                String DAte2 = date_piker.getValue().toString();
                DAte1 += " 00:00:00";
                DAte2 += " 00:00:00";
                if (DAte1.trim().equals(DAte2)) {
                    double ExAmount = rs.getDouble("qtytotal");
                    sum1 = sum1 + ExAmount;
                    DecimalFormat df = new DecimalFormat("#.##");
                    String fom = df.format(sum1);
                    examount.setText("LKR " + fom);
                    System.out.println("BB");
                } else {
                    examount.setText("LKR 0.00");
                    System.out.println("BBB");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        sumvalue();
    }

    @FXML
    private void btn_clear(ActionEvent event) {
        date_piker.setValue(null);
        table1.getItems().clear();
        table2.getItems().clear();
        table3.getItems().clear();
        table4.getItems().clear();
        table1load();
        table2load();
        table3load();
        table4load();
        setcell1();
        setcell2();
        setcell3();
        setcell4();
        sumvalue();
        ReturnAmount();
        ExpiredProduct();
    }

    public void dateinsert() {
        try {
            if (!entercollection.getText().trim().equals("")) {
                dbclass.push("insert into dally_collection (cashamount,cardamount,creditamount,Institutionalamount,changeinvamount,damageinvamount,exproamount,totalamount,enteramount,enterdate,empid) values ('" + cashpayment.getText().split(" ")[1] + "','" + cardpayment.getText().split(" ")[1] + "','" + cerditpayment.getText().split(" ")[1] + "','" + spentamount.getText().split(" ")[1] + "','" + cahgeamount.getText().split(" ")[1] + "','" + damageamount.getText().split(" ")[1] + "','" + examount.getText().split(" ")[1] + "','" + total.getText().split(" ")[1] + "','" + entercollection.getText() + "','" + date.format(d) + "','" + systemcomfigdata.getEmpid() + "')");
                Alert aleet = new Alert(Alert.AlertType.INFORMATION);
                aleet.setTitle("Daylly Collection");
                aleet.setHeaderText("All transactions made today have been saved");
                aleet.setContentText("");
                aleet.showAndWait();
            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean chackdate() {
        try {
            ResultSet rs = dbclass.search("select * from dally_collection");
            while (rs.next()) {
                String Date1 = rs.getString("enterdate");
                String Date2 = date.format(d);
                Date1 += "00:00:00";
                Date2 += "00:00:00";
                System.out.println(Date1);
                System.out.println(Date2);
                if (Date1.trim().equals(Date2)) {
                    return true;
                } else {
                  
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
