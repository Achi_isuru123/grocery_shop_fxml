/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package invoice_option;

import DB.dbclass;
import DB.systemcomfigdata;
import java.net.URL;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import tableview.expired_product_table_controller;

/**
 * FXML Controller class
 *
 * @author isuru
 */
public class Expired_productController implements Initializable {

    @FXML
    private TableView<tableview.expired_product_table_controller> table1;
    @FXML
    private TableColumn<tableview.expired_product_table_controller, Integer> c_proid;
    @FXML
    private TableColumn<tableview.expired_product_table_controller, String> c_proname;
    @FXML
    private TableColumn<tableview.expired_product_table_controller, String> c_exda;
    @FXML
    private TableColumn<tableview.expired_product_table_controller, String> c_qty;
    @FXML
    private TableColumn<tableview.expired_product_table_controller, Double> c_purp;
    @FXML
    private TableColumn<tableview.expired_product_table_controller, String> c_tot;

    ObservableList<tableview.expired_product_table_controller> Tablelist = FXCollections.observableArrayList();
    @FXML
    private TextField amount;

    Date d = new Date();
    SimpleDateFormat Dtime1 = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Searchdb();
        Loadsell();
        sumtotal();
    }

    @FXML
    private void btn_exproduct_save(ActionEvent event) {
        try {
            int items = table1.getItems().size();
            for (int row = 0; row < items; row++) {
                table1.getSelectionModel().select(row);
                int productid = table1.getSelectionModel().getSelectedItem().getProductid();
                String productname = table1.getSelectionModel().getSelectedItem().getProductname();
                String exdate = table1.getSelectionModel().getSelectedItem().getExdate();
                String qty = table1.getSelectionModel().getSelectedItem().getQty();
                double purchaseprice = table1.getSelectionModel().getSelectedItem().getPurchaseprice();
                String total = table1.getSelectionModel().getSelectedItem().getTotal();
                dbclass.push("insert into expierd_product (productid,productname,qty,purchprice,qtytotal,exdate,enterdate,empid) values ('" + productid + "','" + productname + "','" + qty + "','" + purchaseprice + "','" + total + "','" + exdate + "','" + Dtime1.format(d) + "','" + systemcomfigdata.getEmpid() + "')");
                dbclass.push("update store set status = '0' where code = '" + productid + "'");
                dbclass.push("update instock set status = '0' where productid = '" + productid + "'");
            }
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Expired Product");
            alert.setHeaderText("Saving the expired product as done correctly");
            alert.setContentText("");
            alert.showAndWait();
            table1.getItems().clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exdate(int proid) {

    }
    int Productid;

    public void Searchdb() {
        try {
            ResultSet rs = dbclass.search("select * from store where status = '1'");
            while (rs.next()) {

                long time1 = Dtime1.parse(Dtime1.format(d)).getTime();
                long deffaraaa = time1 / (1000 * 60 * 60 * 24);
                Date Dtime = rs.getDate("Expired_date");
                long time2 = Dtime1.parse(Dtime1.format(Dtime)).getTime();
                long deffaraaa2 = time2 / (1000 * 60 * 60 * 24);
                if (deffaraaa > deffaraaa2) {
                    Productid = rs.getInt("code");
                    ResultSet rs2 = dbclass.search("select * from instock where productid = '" + Productid + "'");
                    if (rs2.next()) {
                        String Qty1 = rs.getString("qty").split("-")[0];
                        Qty3 = rs.getString("qty").split("-")[1];
                        String Qty2 = rs2.getString("qty").split("-")[0];
                        purch = rs.getDouble("purchase_price");
                        Sumqty(Qty1, Qty2);

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Loadsell() {
        c_proid.setCellValueFactory(new PropertyValueFactory<>("Productid"));
        c_proname.setCellValueFactory(new PropertyValueFactory<>("Productname"));
        c_exda.setCellValueFactory(new PropertyValueFactory<>("Exdate"));
        c_qty.setCellValueFactory(new PropertyValueFactory<>("Qty"));
        c_purp.setCellValueFactory(new PropertyValueFactory<>("Purchaseprice"));
        c_tot.setCellValueFactory(new PropertyValueFactory<>("Total"));
    }

    double purch;
    String Qty3;

    public void Sumqty(String Qty1, String Qty2) {
        double qtysum1 = Double.parseDouble(Qty1);
        double qtysum2 = Double.parseDouble(Qty2);
        sum = qtysum1 + qtysum2;
        double Tot = sum * purch;
        DecimalFormat df = new DecimalFormat("#.##");
        Format = df.format(Tot);
        chekqtytype(Qty3);
    }
    double sum;
    String Format;

    public void chekqtytype(String fomat) {
        try {
            if (fomat.trim().equals("(Kg)")) {
                type = fomat + "-(Kg)";
                Loadtable();
            } else if (fomat.trim().equals("(g)")) {
                type = sum + "-(g)";
                Loadtable();
            } else if (fomat.trim().equals("(l)")) {
                type = sum + "-(l)";
                Loadtable();
            } else if (fomat.trim().equals("(ml)")) {
                type = sum + "-(ml)";
                Loadtable();
            } else if (fomat.trim().equals("(Pcs)")) {
                type = sum + "-(Pcs)";
                Loadtable();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    String type;

    public void Loadtable() {
        try {
            ResultSet rs = dbclass.search("select * from store where code = '" + Productid + "'");
            if (rs.next()) {
                int proid = rs.getInt("code");
                String Proname = rs.getString("name");
                String Date = rs.getString("Expired_date");
                double Purchase = rs.getDouble("purchase_price");
                expired_product_table_controller product_list = new expired_product_table_controller(
                        proid,
                        Proname,
                        Date,
                        type,
                        Purchase,
                        Format);
                Tablelist.add(product_list);
                table1.setItems(Tablelist);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sumtotal() {
        int items = table1.getItems().size();
        double sum1 = 0;
        for (int row = 0; row < items; row++) {
            table1.getSelectionModel().select(row);
            String total = table1.getSelectionModel().getSelectedItem().getTotal();
            double Sum = Double.parseDouble(total);
            sum1 = sum1 + Sum;
            DecimalFormat df = new DecimalFormat("#.##");
            String fromat = df.format(sum1);
            amount.setText(fromat);
        }
    }
}
