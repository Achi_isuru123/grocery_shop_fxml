/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package invoice_option;

import DB.dbclass;
import DB.systemcomfigdata;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;
import tableview.collection_table_controller;
import tableview.selles_table_controller;

/**
 * FXML Controller class
 *
 * @author isuru
 */
public class CollectionController implements Initializable {

    @FXML
    private TextField cusid;
    @FXML
    private TextField cusname;
    @FXML
    private TextField cusamount;
    @FXML
    private TextField cuscreditamount;
    @FXML
    private TextField cuspayment;
    @FXML
    private TextField cusbalance;
    @FXML
    private TableView<tableview.collection_table_controller> collection_table;
    @FXML
    private TableColumn<tableview.collection_table_controller, String> c_invoice;
    @FXML
    private TableColumn<tableview.collection_table_controller, String> c_customer;
    @FXML
    private TableColumn<tableview.collection_table_controller, String> C_date;
    @FXML
    private TableColumn<tableview.collection_table_controller, Double> c_tota;
    private TableColumn<tableview.collection_table_controller, Double> C_discount;
    @FXML
    private TableColumn<tableview.collection_table_controller, Double> c_payment;
    @FXML
    private TableColumn<tableview.collection_table_controller, Double> C_balance;
    ObservableList<tableview.collection_table_controller> Tblelist = FXCollections.observableArrayList();
    @FXML
    private TextField search;
    @FXML
    private TableColumn<?, ?> C_nettotal;

    Date d = new Date();
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
    @FXML
    private TextField amount_given;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        loadtable();
        setcell();
    }

    @FXML
    private void btnprint(ActionEvent event) {
        double amo1 = Double.parseDouble(cuscreditamount.getText());
        double amo2 = Double.parseDouble(cuspayment.getText());
        if (amo1 >= amo2) {
            try {
                double Cuspayment = Double.parseDouble(cuspayment.getText());
                double CusCredit = Double.parseDouble(cuscreditamount.getText());
                double sum = CusCredit - Cuspayment;
                if (Cuspayment >= CusCredit) {
                    dbclass.push("insert into credit_collection (customer_id,cus_name,credit_amount,payment_given,payment_amount,balnce,arrears_amount,colldate,empid) values ('" + cusid.getText()
                            + "','" + cusname.getText()
                            + "','" + cuscreditamount.getText()
                            + "','" + amount_given.getText() + "','" + cuspayment.getText()
                            + "','" + cusbalance.getText()
                            + "','" + 0.00 + "','" + date.format(d) + "','" + systemcomfigdata.getEmpid() + "')");
                    dbclass.push("update add_customer set Availableamount = Availableamount - '" + cuspayment.getText() + "' where customerid = '" + cusid.getText() + "'");
                    printrepot();
                    Clear();
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Credit Customer");
                    alert.setHeaderText("Credit Amount is " + sum);
                    alert.setContentText("Payment Should be made at or below the relevent amount");
                    alert.showAndWait();
                } else {
                    dbclass.push("insert into credit_collection (customer_id,cus_name,credit_amount,payment_given,payment_amount,balnce,arrears_amount,colldate,empid) values ('" + cusid.getText()
                            + "','" + cusname.getText()
                            + "','" + cuscreditamount.getText()
                            + "','" + amount_given.getText() + "','" + cuspayment.getText()
                            + "','" + cusbalance.getText()
                            + "','" + sum + "','" + date.format(d) + "','" + systemcomfigdata.getEmpid() + "')");
                    dbclass.push("update add_customer set Availableamount = Availableamount - '" + cuspayment.getText() + "' where customerid = '" + cusid.getText() + "'");
                    printrepot();
                    Clear();
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Credit Customer");
                    alert.setHeaderText("Credit Amount is " + sum);
                    alert.setContentText("Payment Should be made at or below the relevent amount");
                    alert.showAndWait();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("");
            alert.setHeaderText("");
            alert.setContentText("");
            alert.showAndWait();
        }

    }

    @FXML
    private void collectionclik(MouseEvent event) {
        if (event.getClickCount() == 2) {
            collection_table_controller p = collection_table.getSelectionModel().getSelectedItem();
            cusid.setText(p.getCustomerid());
        }
    }

    @FXML
    private void search_collection(KeyEvent event) {
        try {
            FilteredList<tableview.collection_table_controller> filterlist = new FilteredList<>(Tblelist, b -> true);
            search.textProperty().addListener((observable, oldValue, newValue) -> {
                filterlist.setPredicate(collection -> {
                    if (newValue.isEmpty() || newValue.isEmpty() || newValue == null) {
                        return true;
                    }
                    String lowercasefilter = newValue.toLowerCase();

                    if (collection.getInvoiceno().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(collection.getPayment()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(collection.getBalance()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(collection.getNettotal()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(collection.getTotal()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (collection.getCustomerid().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (collection.getDate().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else {
                        return false;
                    }

                });
            });
            SortedList<tableview.collection_table_controller> sortedlist = new SortedList<>(filterlist);
            sortedlist.comparatorProperty().bind(collection_table.comparatorProperty());
            collection_table.setItems(sortedlist);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void loadtable() {
        try {
            ResultSet rs = dbclass.search("select * from invoice");
            while (rs.next()) {
                String Paymethod = rs.getString("paymentmethod");
                if (Paymethod.trim().equals("Credit Seller")) {
                    Tblelist.add(new collection_table_controller(
                            rs.getString("invoicid"),
                            rs.getString("customer_id"),
                            rs.getString("enterdate"),
                            rs.getDouble("total"),
                            rs.getDouble("nettotal"),
                            rs.getDouble("payment"),
                            rs.getDouble("blance")));
                }
            }
            collection_table.setItems(Tblelist);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setcell() {
        c_invoice.setCellValueFactory(new PropertyValueFactory<>("Invoiceno"));
        c_customer.setCellValueFactory(new PropertyValueFactory<>("Customerid"));
        C_date.setCellValueFactory(new PropertyValueFactory<>("Date"));
        c_tota.setCellValueFactory(new PropertyValueFactory<>("Total"));
        C_nettotal.setCellValueFactory(new PropertyValueFactory<>("Nettotal"));
        c_payment.setCellValueFactory(new PropertyValueFactory<>("Payment"));
        C_balance.setCellValueFactory(new PropertyValueFactory<>("Balance"));

    }

    @FXML
    private void entercusid(ActionEvent event) {
        try {
            ResultSet rs = dbclass.search("select * from add_customer where customerid = '" + cusid.getText() + "'");
            if (rs.next()) {
                cusname.setText(rs.getString("name"));
                double AmountLimit = rs.getDouble("Amountlimit");
                cusamount.setText("" + AmountLimit);
                double Availableamount = rs.getDouble("Availableamount");
                cuscreditamount.setText("" + Availableamount);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void pay_amount(ActionEvent event) {
        double Amount = Double.parseDouble(amount_given.getText());
        double EnterAmount = Double.parseDouble(cuspayment.getText());
        if (Amount >= EnterAmount) {
            double sum = Amount - EnterAmount;
            cusbalance.setText("" + sum);
        } else if (Amount < EnterAmount) {
            double sum = Amount - EnterAmount;
            cusbalance.setText("" + sum);
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Credit Customer");
            alert.setHeaderText("Credit Amount is " + Amount);
            alert.setContentText("Payment Should be made at or below the relevent amount");
            alert.showAndWait();
        }
    }

    private void Clear() {
        cusid.setText(null);
        cusname.setText(null);
        cusamount.setText(null);
        cuscreditamount.setText(null);
        cuspayment.setText(null);
        amount_given.setText(null);
        cusbalance.setText(null);
    }
    String id;
    double amo_sum;

    public void printrepot() {

        try {
            ResultSet rs2 = dbclass.search("select * from add_customer where customerid = '" + cusid.getText() + "'");
            if (rs2.next()) {
                double amo1 = rs2.getDouble("Amountlimit");
                double amo2 = rs2.getDouble("Availableamount");
                amo_sum = amo1 - amo2;
                System.out.println("test amount :"+amo_sum);
            }
            ResultSet rs = dbclass.search("select count(*) as customerid from credit_collection");
            if (rs.next()) {
                int count = rs.getInt("customerid");
                id = "" + (count);
                System.out.println("Achi" + id);
            }
            String Path = "D:\\report\\groceryShop\\credit_customer.jasper";
            InputStream fileinput = new FileInputStream(Path);
            Map<String, Object> params = new HashMap<>();
            params.put("credititemid", id);
            params.put("Defference", amo_sum);
            JasperPrint printrepots = JasperFillManager.fillReport(fileinput, params, dbclass.getnewconnection());
            JasperViewer.viewReport(printrepots, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
