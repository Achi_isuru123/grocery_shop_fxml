/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package invoice_option;

import DB.dbclass;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import tableview.return_product_table_controller;

/**
 * FXML Controller class
 *
 * @author isuru
 */
public class Return_productController implements Initializable {

    @FXML
    private JFXTextField invid;
    @FXML
    private JFXTextField nettot;
    @FXML
    private JFXTextField pay;
    @FXML
    private JFXTextField bal;
    @FXML
    private JFXTextField payrype;
    @FXML
    private TableView<tableview.return_product_table_controller> table1;
    @FXML
    private TableColumn<tableview.return_product_table_controller, Integer> c_itemid;
    @FXML
    private TableColumn<tableview.return_product_table_controller, Integer> c_proid;
    @FXML
    private TableColumn<tableview.return_product_table_controller, String> c_invid;
    @FXML
    private TableColumn<tableview.return_product_table_controller, String> c_proname;
    @FXML
    private TableColumn<tableview.return_product_table_controller, String> c_qty;
    @FXML
    private TableColumn<tableview.return_product_table_controller, Double> c_subtot;

    ObservableList<tableview.return_product_table_controller> tablelist = FXCollections.observableArrayList();
    @FXML
    private TextField returnamo;
    @FXML
    private CheckBox return_invoice;
    @FXML
    private CheckBox return_items;
    @FXML
    private JFXTextField proid;
    @FXML
    private JFXTextField itemid;
    @FXML
    private JFXTextField invoiceid;

    Date d = new Date();
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
    @FXML
    private TextField statsinv;
    @FXML
    private CheckBox change_items;
    @FXML
    private CheckBox damage_item;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Setcell();
    }

    @FXML
    private void btn_return_product(ActionEvent event) {
    }

    public void LoadTableandtextfelied() {
        try {
            ResultSet rs = dbclass.search("select * from invoice where invoicid = '" + invid.getText() + "'");
            if (rs.next()) {
                String DAte1 = rs.getString("enterdate");
                String DAte2 = date.format(d);
                DAte1 += " 00:00:00";
                DAte2 += " 00:00:00";
                if (DAte1.trim().equals(DAte2)) {
                    nettot.setText(rs.getString("nettotal"));
                    pay.setText(rs.getString("payment"));
                    bal.setText(rs.getString("blance"));
                    payrype.setText(rs.getString("paymentmethod"));
                    ResultSet rs2 = dbclass.search("select * from invoiceitem where invoicid = '" + invid.getText() + "'");
                    while (rs2.next()) {
                        tablelist.add(new return_product_table_controller(
                                rs2.getInt("invoiceitemid"),
                                rs2.getInt("productid"),
                                rs2.getString("invoicid"),
                                rs2.getString("itemname"),
                                rs2.getString("qty"),
                                rs2.getDouble("subtotal")));
                    }
                    table1.setItems(tablelist);
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Return Product");
                    alert.setHeaderText("This invoice did not go out today. Select the day below");
                    alert.getContentText();
                    alert.showAndWait();
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Return Product");
                alert.setHeaderText("This invoiceid did not go out. Enter the correct invoiceid");
                alert.getContentText();
                alert.showAndWait();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Setcell() {
        c_itemid.setCellValueFactory(new PropertyValueFactory<>("Invoiceitemid"));
        c_proid.setCellValueFactory(new PropertyValueFactory<>("Productid"));
        c_invid.setCellValueFactory(new PropertyValueFactory<>("Invoiceid"));
        c_proname.setCellValueFactory(new PropertyValueFactory<>("Productname"));
        c_qty.setCellValueFactory(new PropertyValueFactory<>("Qty"));
        c_subtot.setCellValueFactory(new PropertyValueFactory<>("Subtotal"));
    }

    @FXML
    private void serachinvid(ActionEvent event) {
        table1.getItems().clear();
        LoadTableandtextfelied();
    }

    @FXML
    private void btn_return_invoice(ActionEvent event) {
        if (!invid.getText().trim().equals("")) {
            return_items.setSelected(false);
            table1.setDisable(true);
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Return Product");
            alert.setHeaderText("Search invoice Id, Please Enter invoice Id");
            alert.getContentText();
            alert.showAndWait();
            return_invoice.setSelected(false);
            return_items.setSelected(false);
        }

    }

    @FXML
    private void btn_return_items(ActionEvent event) {
        if (!invid.getText().trim().equals("")) {
            return_invoice.setSelected(false);
            table1.setDisable(false);
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Return Product");
            alert.setHeaderText("Search invoice Id, Please Enter invoice Id");
            alert.getContentText();
            alert.showAndWait();
            return_invoice.setSelected(false);
            return_items.setSelected(false);
        }
    }

    private void Clear() {
        invid.setText(null);
        nettot.setText(null);
        pay.setText(null);
        bal.setText(null);
        bal.setText(null);
        payrype.setText(null);
        proid.setText(null);
        itemid.setText(null);
        invoiceid.setText(null);
        returnamo.setText(null);
        table1.getItems().clear();
        return_invoice.setSelected(false);
        return_items.setSelected(false);
    }
    double sum1 = 0;

    @FXML
    private void select_items(MouseEvent event) {
        if (event.getClickCount() == 2) {
            if (change_items.isSelected() || damage_item.isSelected()) {
                productids = table1.getSelectionModel().getSelectedItem().getProductid();
                invoiceitemids = table1.getSelectionModel().getSelectedItem().getInvoiceitemid();
                invoiceid1s = table1.getSelectionModel().getSelectedItem().getInvoiceid();
                productnames = table1.getSelectionModel().getSelectedItem().getProductname();
                subtotals = table1.getSelectionModel().getSelectedItem().getSubtotal();
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Return Items");
                alert.setHeaderText("Do you wont to return this item");
                alert.setContentText("");
                alert.showAndWait().ifPresent(consumer -> {
                    if (consumer == ButtonType.OK) {
                        double sum = table1.getSelectionModel().getSelectedItem().getSubtotal();
                        sum1 += sum;
                        DecimalFormat df = new DecimalFormat("#.##");
                        String format = df.format(sum1);
                        returnamo.setText(format);
                        proid.setText("" + productids);
                        itemid.setText("" + invoiceitemids);
                        invoiceid.setText(invoiceid1s);
                        Qty = table1.getSelectionModel().getSelectedItem().getQty().split("-")[0];
                        mash = table1.getSelectionModel().getSelectedItem().getQty().split("-")[1];
                        qtyretuen();
                    } else {
                        proid.setText("" + productids);
                        itemid.setText("" + invoiceitemids);
                        invoiceid.setText(invoiceid1s);
                    }
                });
            } else {

            }
        }
    }

    String Qty;
    String mash;

    public void qtyretuen() {
        try {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Return Items");
            alert.setHeaderText("Redady to reduce the number of items that are being returned");
            alert.setContentText("");
            alert.showAndWait().ifPresent((ButtonType consumer) -> {
                if (consumer == ButtonType.OK) {
                    try {
                        ResultSet rs = dbclass.search("select * from instock where productid = '" + proid.getText() + "'");
                        if (rs.next()) {
                            String Qtys = rs.getString("qty").split("-")[0];
                            double sum1 = Double.parseDouble(Qty);
                            double sum2 = Double.parseDouble(Qtys);
                            sum = sum1 + sum2;
                        }
                        mashiment();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    double sum;
    String type;
    String itemqty;

    public void mashiment() {
        if (mash.trim().equals("(Kg)")) {
            if (change_items.isSelected()) {
                if (return_invoice.isSelected()) {
                    type = "" + sum + "-(Kg)";
                    itemqty = "0-(Kg)";
                    invoicereturn();
                    changeitem();
                } else {
                    if (return_items.isSelected()) {
                        type = "" + sum + "-(Kg)";
                        itemqty = "0-(Kg)";
                        Decreasing();
                        changeitem();
                    }
                }

            } else {
                if (damage_item.isSelected()) {
                    itemqty = "0-(Kg)";
                    damgeitems();
                } else {

                }

            }

        } else if (mash.trim().equals("(g)")) {
            if (change_items.isSelected()) {
                if (return_invoice.isSelected()) {
                    type = "" + sum + "-(g)";
                    itemqty = "0-(g)";
                    invoicereturn();
                    changeitem();
                } else {
                    if (return_items.isSelected()) {
                        type = "" + sum + "-(g)";
                        itemqty = "0-(g)";
                        Decreasing();
                        changeitem();
                    }
                }
            } else {
                if (damage_item.isSelected()) {
                    itemqty = "0-(g)";
                    damgeitems();
                } else {

                }

            }
        } else if (mash.trim().equals("(l)")) {
            if (change_items.isSelected()) {
                if (return_invoice.isSelected()) {
                    type = "" + sum + "-(l)";
                    itemqty = "0-(l)";
                    invoicereturn();
                } else {
                    if (return_items.isSelected()) {
                        type = "" + sum + "-(l)";
                        itemqty = "0-(l)";
                        Decreasing();
                        changeitem();
                    }
                }
            } else {
                if (damage_item.isSelected()) {
                    itemqty = "0-(l)";
                    damgeitems();
                } else {

                }

            }
        } else if (mash.trim().equals("(ml)")) {
            if (change_items.isSelected()) {
                if (return_invoice.isSelected()) {
                    type = "" + sum + "-(ml)";
                    itemqty = "0-(ml)";
                    invoicereturn();
                    changeitem();
                } else {
                    if (return_items.isSelected()) {
                        type = "" + sum + "-(ml)";
                        itemqty = "0-(ml)";
                        Decreasing();
                        changeitem();
                    }
                }
            } else {
                if (damage_item.isSelected()) {
                    itemqty = "0-(ml)";
                    damgeitems();
                } else {

                }

            }
        } else if (mash.trim().equals("(Pcs)")) {
            if (change_items.isSelected()) {
                if (return_invoice.isSelected()) {
                    type = "" + sum + "-(Pcs)";
                    itemqty = "0-(Pcs)";
                    invoicereturn();
                    changeitem();
                } else {
                    if (return_items.isSelected()) {
                        type = "" + sum + "-(Pcs)";
                        itemqty = "0-(Pcs)";
                        Decreasing();
                        changeitem();
                    }
                }
            } else {
                if (damage_item.isSelected()) {
                    itemqty = "0-(Pcs)";
                    damgeitems();
                } else {
                   
                }

            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Return Product");
            alert.setHeaderText("Select the return invoice");
            alert.setContentText("");
            alert.showAndWait();
        }
    }

    public void Decreasing() {
        try {
            dbclass.push("update instock set qty = '" + type + "' where productid = '" + proid.getText() + "'");
            dbclass.push("update invoiceitem set qty = '" + itemqty + "', itemtotal = '0', discount1 = '0', subtotal = '0' where invoiceitemid = '" + itemid.getText() + "'");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    double sum5;
    String format;
    String invoiceid1;
    int productid;
    String productname;
    int invoiceitemid;
    double subtotal;

    private void getTabledata() {
        int item = table1.getItems().size();
        sum5 = 0;
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Return Items");
        alert.setHeaderText("Redady to reduce the number of items that are being returned");
        alert.setContentText("");
        alert.showAndWait().ifPresent((ButtonType consumer) -> {
            if (consumer == ButtonType.OK) {
                table1.setDisable(false);
                try {
                    for (int row = 0; row < item; row++) {
                        table1.getSelectionModel().select(row);
                        invoiceitemid = table1.getSelectionModel().getSelectedItem().getInvoiceitemid();
                        invoiceid1 = table1.getSelectionModel().getSelectedItem().getInvoiceid();
                        productid = table1.getSelectionModel().getSelectedItem().getProductid();
                        productname = table1.getSelectionModel().getSelectedItem().getProductname();
                        subtotal = table1.getSelectionModel().getSelectedItem().getSubtotal();
                        Qty = table1.getSelectionModel().getSelectedItem().getQty().split("-")[0];
                        mash = table1.getSelectionModel().getSelectedItem().getQty().split("-")[1];
                        try {
                            ResultSet rs = dbclass.search("select * from instock where productid = '" + productid + "'");
                            if (rs.next()) {
                                String Qtys = rs.getString("qty").split("-")[0];
                                double sum1 = Double.parseDouble(Qty);
                                double sum2 = Double.parseDouble(Qtys);
                                sum = sum1 + sum2;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (change_items.isSelected()) {
                            mashiment();
                        } else {
                            if (damage_item.isSelected()) {
                                damgeitems();
                            }
                        }

                        sum5 = sum5 + subtotal;
                        DecimalFormat df = new DecimalFormat("#.##");
                        format = df.format(sum5);

                    }
                    returnamo.setText(format);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {

            }
        });
    }

    public void invoicereturn() {
        try {
            ResultSet rs = dbclass.search("select * from invoice");
            if (rs.next()) {
                boolean Status = rs.getBoolean("status");
                if (Status) {
                    dbclass.push("update invoice set status = '0' where invoicid = '" + invoiceid1 + "'");
                } else {

                }
            }
            dbclass.push("update instock set qty = '" + type + "' where productid = '" + productid + "'");
            dbclass.push("update invoiceitem set qty = '" + itemqty + "', itemtotal = '0', discount1 = '0', subtotal = '0' where invoiceitemid = '" + invoiceitemid + "'");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void dbsave() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @FXML
    private void btn_change_item(ActionEvent event) {
        if (!invid.getText().trim().equals("")) {
            damage_item.setSelected(false);
            if (return_invoice.isSelected()) {
                getTabledata();
            } else {
                if (return_items.isSelected()) {
                    table1.setDisable(false);
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Return Product");
                    alert.setHeaderText("Please selected return invoice or return item");
                    alert.getContentText();
                    alert.showAndWait();
                    change_items.setSelected(false);
                    damage_item.setSelected(false);
                }
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Return Product");
            alert.setHeaderText("Search  invoice Id, Please Enter invoice Id");
            alert.getContentText();
            alert.showAndWait();
            change_items.setSelected(false);
            damage_item.setSelected(false);
        }
    }

    @FXML
    private void btn_damage_item(ActionEvent event) {
        if (!invid.getText().trim().equals("")) {
            change_items.setSelected(false);
            if (return_invoice.isSelected()) {
                getTabledata();
            } else {
                if (return_items.isSelected()) {

                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Return Product");
                    alert.setHeaderText("Please selected return invoice or return item");
                    alert.getContentText();
                    alert.showAndWait();
                    change_items.setSelected(false);
                    damage_item.setSelected(false);
                }
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Return Product");
            alert.setHeaderText("Search invoice Id, Please Enter invoice Id");
            alert.getContentText();
            alert.showAndWait();
            change_items.setSelected(false);
            damage_item.setSelected(false);
        }
    }
    int productids;
    int invoiceitemids;
    String invoiceid1s;
    String productnames;
    double subtotals;

    private void changeitem() {
        try { 
            if (return_items.isSelected()) {
                String name = "Change Item";
                dbclass.push("insert into return_product (invoiceid,productid,productname,itemtotal,returntype,enterdate) values ('" + invoiceid1s + "','" + productids + "','" + productnames + "','" + subtotals + "','" + name + "','" + date.format(d) + "')");
                 dbclass.push("update invoiceitem set qty = '" + itemqty + "', itemtotal = '0', discount1 = '0', subtotal = '0' where invoiceitemid = '" + invoiceitemids + "'");
            } else {
                if (return_invoice.isSelected()) {
                    String name = "Change Invoice";
                    dbclass.push("insert into return_product (invoiceid,productid,productname,itemtotal,returntype,enterdate) values ('" + invoiceid1 + "','" + productid + "','" + productname + "','" + subtotal + "','" + name + "','" + date.format(d) + "')");
                    dbclass.push("update invoiceitem set qty = '" + itemqty + "', itemtotal = '0', discount1 = '0', subtotal = '0' where invoiceitemid = '" + invoiceitemid + "'");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void damgeitems() {
        if (return_items.isSelected()) {
            try {
                String name = "Damage Item";
                dbclass.push("insert into return_product (invoiceid,productid,productname,itemtotal,returntype,enterdate) values ('" + invoiceid1s + "','" + productids + "','" + productnames + "','" + subtotals + "','" + name + "','" + date.format(d) + "')");
                dbclass.push("update invoiceitem set qty = '" + itemqty + "', itemtotal = '0', discount1 = '0', subtotal = '0' where invoiceitemid = '" + invoiceitemids + "'");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (return_invoice.isSelected()) {
                try {
                    String name = "Damage Invoice";
                    dbclass.push("insert into return_product (invoiceid,productid,productname,itemtotal,returntype,enterdate) values ('" + invoiceid1 + "','" + productid + "','" + productname + "','" + subtotal + "','" + name + "','" + date.format(d) + "')");
                    dbclass.push("update invoiceitem set qty = '" + itemqty + "', itemtotal = '0', discount1 = '0', subtotal = '0' where invoiceitemid = '" + invoiceitemid + "'");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
