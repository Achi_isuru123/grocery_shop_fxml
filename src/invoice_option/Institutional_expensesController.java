/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package invoice_option;

import DB.dbclass;
import DB.systemcomfigdata;
import java.net.URL;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import tableview.expenditure_table_controller;

/**
 * FXML Controller class
 *
 * @author isuru
 */
public class Institutional_expensesController implements Initializable {

    @FXML
    private TextField totalamount;
    @FXML
    private TextField spentamount;
    @FXML
    private TableView<tableview.expenditure_table_controller> table1;
    @FXML
    private TableColumn<tableview.expenditure_table_controller, Integer> c_institutional;
    @FXML
    private TableColumn<tableview.expenditure_table_controller, Double> c_totalamount;
    @FXML
    private TableColumn<tableview.expenditure_table_controller, Double> c_spentamount;
    @FXML
    private TableColumn<tableview.expenditure_table_controller, String> c_discription;
    @FXML
    private TableColumn<tableview.expenditure_table_controller, String> c_spentdate;
    @FXML
    private TableColumn<tableview.expenditure_table_controller, String> c_empid;
    ObservableList<tableview.expenditure_table_controller> tablelist = FXCollections.observableArrayList();

    @FXML
    private TextField search;
    @FXML
    private CheckBox checkbox;

    Date d = new Date();
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
    @FXML
    private TextArea description;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        loadtable();
        setcell();
        Amountsum();
    }

    @FXML
    private void btnsave(ActionEvent event) {
        try {
            if (!spentamount.getText().trim().equals("") && !description.getText().trim().equals("")) {
                dbclass.push("insert into institutional (totalamount,spentamount,discription,spentdate,empid) values ('" + totalamount.getText() + "','" + spentamount.getText() + "','" + description.getText() + "','" + date.format(d) + "','" + systemcomfigdata.getEmpid() + "')");
                Clear();
                Amountsum();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void searchkey(KeyEvent event) {
        try {
            FilteredList<tableview.expenditure_table_controller> filterlist = new FilteredList<>(tablelist, b -> true);
            search.textProperty().addListener((observable, oldValue, newValue) -> {
                filterlist.setPredicate(expenditure -> {
                    if (newValue.isEmpty() || newValue.isEmpty() || newValue == null) {
                        return true;
                    }
                    String lowercasefilter = newValue.toLowerCase();

                    if (String.valueOf(expenditure.getTotamount()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(expenditure.getEmpid()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(expenditure.getInsid()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(expenditure.getSpentamount()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (expenditure.getSpentdate().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else {
                        return false;
                    }

                });
            });
            SortedList<tableview.expenditure_table_controller> sortedlist = new SortedList<>(filterlist);
            sortedlist.comparatorProperty().bind(table1.comparatorProperty());
            table1.setItems(sortedlist);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void todyexp(ActionEvent event) {

        if (checkbox.isSelected()) {
            table1.getItems().clear();
            todayEx();
        } else {
            table1.getItems().clear();
            loadtable();
        }
    }

    public void loadtable() {
        try {
            ResultSet rs = dbclass.search("select * from institutional");
            while (rs.next()) {
                tablelist.add(new expenditure_table_controller(
                        rs.getInt("Institutionalid"),
                        rs.getDouble("totalamount"),
                        rs.getDouble("spentamount"),
                        rs.getString("discription"),
                        rs.getString("spentdate"),
                        rs.getInt("empid")));
            }
            table1.setItems(tablelist);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setcell() {
        c_institutional.setCellValueFactory(new PropertyValueFactory<>("Insid"));
        c_totalamount.setCellValueFactory(new PropertyValueFactory<>("Totamount"));
        c_spentamount.setCellValueFactory(new PropertyValueFactory<>("Spentamount"));
        c_discription.setCellValueFactory(new PropertyValueFactory<>("Description"));
        c_spentdate.setCellValueFactory(new PropertyValueFactory<>("Spentdate"));
        c_empid.setCellValueFactory(new PropertyValueFactory<>("Empid"));
    }

    public void todayEx() {
        try {
            ResultSet rs = dbclass.search("select * from institutional");
            while (rs.next()) {
                String DAte1 = rs.getString("spentdate");
                String DAte2 = date.format(d);
                DAte1 += " 00:00:00";
                DAte2 += " 00:00:00";
                if (DAte1.trim().equals(DAte2)) {
                    tablelist.add(new expenditure_table_controller(
                            rs.getInt("Institutionalid"),
                            rs.getDouble("totalamount"),
                            rs.getDouble("spentamount"),
                            rs.getString("discription"),
                            rs.getString("spentdate"),
                            rs.getInt("empid")));
                }
            }
            table1.setItems(tablelist);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Clear() {
        spentamount.setText(null);
        description.setText(null);
    }

    public void Amountsum() {
        try {
           double sum = 0;
            ResultSet rs = dbclass.search("select * from invoice");
            while (rs.next()) {
                String DAte1 = rs.getString("enterdate");
                String DAte2 = date.format(d);
                DAte1 += " 00:00:00";
                DAte2 += " 00:00:00";
                if (DAte1.trim().equals(DAte2)) {
                    double NetTotal = rs.getDouble("nettotal");
                     sum = sum + NetTotal;  
                }
            }
            totalamount.setText(""+sum);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
