/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package invoice_option;

import DB.dbclass;
import java.net.URL;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import tableview.selles_table_controller;

/**
 * FXML Controller class
 *
 * @author isuru
 */
public class SellesController implements Initializable {

    @FXML
    private TableView<tableview.selles_table_controller> table_selles;
    @FXML
    private TableColumn<tableview.selles_table_controller, String> c_invoicno;
    @FXML
    private TableColumn<tableview.selles_table_controller, String> c_customerid;
    @FXML
    private TableColumn<tableview.selles_table_controller, String> c_enterdate;
    @FXML
    private TableColumn<tableview.selles_table_controller, Double> c_total;
    @FXML
    private TableColumn<tableview.selles_table_controller, Double> c_discount;
    @FXML
    private TableColumn<tableview.selles_table_controller, Double> c_nettotal;
    @FXML
    private TableColumn<tableview.selles_table_controller, Double> c_payment;
    @FXML
    private TableColumn<tableview.selles_table_controller, Double> c_blance;
    @FXML
    private TableColumn<tableview.selles_table_controller, String> c_paymenttype;
    @FXML
    private TableColumn<tableview.selles_table_controller, String> c_paymentmethod;
    @FXML
    private TextField search;
    ObservableList<tableview.selles_table_controller> Tblelist = FXCollections.observableArrayList();

    Date d = new Date();
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        loadtable();
        setcell();
    }

    @FXML
    private void searchinvoice(KeyEvent event) {
        try {
            FilteredList<tableview.selles_table_controller> filterlist = new FilteredList<>(Tblelist, b -> true);
            search.textProperty().addListener((observable, oldValue, newValue) -> {
                filterlist.setPredicate(selles -> {
                    if (newValue.isEmpty() || newValue.isEmpty() || newValue == null) {
                        return true;
                    }
                    String lowercasefilter = newValue.toLowerCase();

                    if (selles.getInvoiceno().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(selles.getPayment()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(selles.getBalance()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(selles.getNettotal()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(selles.getTotal()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (selles.getCustomerid().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (selles.getPaymenttype().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (selles.getPaymentmethod().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (selles.getDate().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else {
                        return false;
                    }

                });
            });
            SortedList<tableview.selles_table_controller> sortedlist = new SortedList<>(filterlist);
            sortedlist.comparatorProperty().bind(table_selles.comparatorProperty());
            table_selles.setItems(sortedlist);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void loadtable() {
        try {
            ResultSet rs = dbclass.search("select * from invoice");
            while (rs.next()) {
                String DAte1 = rs.getString("enterdate");
                String DAte2 = date.format(d);
                DAte1 += " 00:00:00";
                DAte2 += " 00:00:00";
                if (DAte1.trim().equals(DAte2)) {
                    Tblelist.add(new selles_table_controller(
                            rs.getString("invoicid"),
                            rs.getString("customer_id"),
                            rs.getString("enterdate"),
                            rs.getDouble("total"),
                            rs.getDouble("descount2"),
                            rs.getDouble("nettotal"),
                            rs.getDouble("payment"),
                            rs.getDouble("blance"),
                            rs.getString("pricetype"),
                            rs.getString("paymentmethod")));
                }
            }
            table_selles.setItems(Tblelist);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setcell() {
        c_invoicno.setCellValueFactory(new PropertyValueFactory<>("Invoiceno"));
        c_customerid.setCellValueFactory(new PropertyValueFactory<>("Customerid"));
        c_enterdate.setCellValueFactory(new PropertyValueFactory<>("Date"));
        c_total.setCellValueFactory(new PropertyValueFactory<>("Total"));
        c_discount.setCellValueFactory(new PropertyValueFactory<>("Discount"));
        c_nettotal.setCellValueFactory(new PropertyValueFactory<>("Nettotal"));
        c_payment.setCellValueFactory(new PropertyValueFactory<>("Payment"));
        c_blance.setCellValueFactory(new PropertyValueFactory<>("Balance"));
        c_paymenttype.setCellValueFactory(new PropertyValueFactory<>("Paymenttype"));
        c_paymentmethod.setCellValueFactory(new PropertyValueFactory<>("paymentmethod"));
    }
}
