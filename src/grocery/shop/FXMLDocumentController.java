/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grocery.shop;

import DB.MD5;
import DB.dbclass;
import DB.systemcomfigdata;
import com.jfoenix.controls.JFXButton;
import com.mysql.jdbc.PreparedStatement;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

/**
 *
 * @author isuru
 */
public class FXMLDocumentController implements Initializable {

    Date d = new Date();
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat Time = new SimpleDateFormat("HH:mm:ss");
    @FXML
    private AnchorPane loginpane;
    @FXML
    private JFXButton login;
    @FXML
    private TextField user;
    @FXML
    private PasswordField password;
    @FXML
    private Label logo1, logo2;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Fontstily();
    }

    @FXML
    private void btnlogin(ActionEvent event) {
        try {
            String username = user.getText();
            String pass = password.getText();
            Connection connection = dbclass.getnewconnection();

            String sql = "select * from employe_register where username = ? and password = ?";
            PreparedStatement pst = (PreparedStatement) connection.prepareStatement(sql);
            pst.setString(1, username);
            String md5 = MD5.getMd5(pass);
            pst.setString(2, md5);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                boolean Status = rs.getBoolean("status");
                if (Status) {
                    systemcomfigdata.setEmpid(rs.getString("employeeid"));
                    systemcomfigdata.setFullname(rs.getString("fullname"));
                    systemcomfigdata.setActiveuser(rs.getString("username"));
                    systemcomfigdata.setUsertype(rs.getString("section"));
                    systemcomfigdata.setImagepath(rs.getString("imagepath"));
                    dbclass.push("insert into login_ditels (fullname,empid,section,logdate,logtime) values ('" + systemcomfigdata.getFullname() + "','" + systemcomfigdata.getEmpid() + "','" + systemcomfigdata.getUsertype() + "','" + date.format(d) + "','" + Time.format(d) + "')");
                    Stage stage1 = (Stage) loginpane.getScene().getWindow();
                    stage1.close(); 

                    Parent root = FXMLLoader.load(getClass().getResource("/dashbord/home_viwe.fxml"));
                    Stage stage = new Stage();
                    Scene sence = new Scene(root);

                    stage.setScene(sence);
                    stage.setTitle("Grocery Shop Admin Panel");
                    Image icon = new Image("/image/img1.jpg");
                    stage.getIcons().add(icon);
                    stage.showAndWait();

                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Login Panel");
                    alert.setHeaderText("Employee is not activeted..!");
                    alert.showAndWait();
                    user.setText(null);
                    password.setText(null);
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Login Panel");
                alert.setHeaderText("User name or password not matched..!");
                alert.showAndWait();
                user.setText(null);
                password.setText(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Fontstily() {
        DropShadow shadow = new DropShadow(40, Color.valueOf("#4fa3f1"));
        logo1.setEffect(shadow);
        DropShadow shadow1 = new DropShadow(20, Color.valueOf("#4fa3f1"));
        logo2.setEffect(shadow1);
    }

    @FXML
    private void btn_enter_login(ActionEvent event) {
        try {
            String username = user.getText();
            String pass = password.getText();
            Connection connection = dbclass.getnewconnection();

            String sql = "select * from employe_register where username = ? and password = ?";
            PreparedStatement pst = (PreparedStatement) connection.prepareStatement(sql);
            pst.setString(1, username);
            String md5 = MD5.getMd5(pass);
            pst.setString(2, md5);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                boolean Status = rs.getBoolean("status");
                if (Status) {
                    systemcomfigdata.setEmpid(rs.getString("employeeid"));
                    systemcomfigdata.setFullname(rs.getString("fullname"));
                    systemcomfigdata.setActiveuser(rs.getString("username"));
                    systemcomfigdata.setUsertype(rs.getString("section"));
                    systemcomfigdata.setImagepath(rs.getString("imagepath"));
                    dbclass.push("insert into login_ditels (fullname,empid,section,logdate,logtime) values ('" + systemcomfigdata.getFullname() + "','" + systemcomfigdata.getEmpid() + "','" + systemcomfigdata.getUsertype() + "','" + date.format(d) + "','" + Time.format(d) + "')");
                    Stage stage1 = (Stage) loginpane.getScene().getWindow();
                    stage1.close(); 

                    Parent root = FXMLLoader.load(getClass().getResource("/dashbord/home_viwe.fxml"));
                    Stage stage = new Stage();
                    Scene sence = new Scene(root);

                    stage.setScene(sence);
                    stage.setTitle("Grocery Shop Admin Panel");
                    Image icon = new Image("/image/img1.jpg");
                    stage.getIcons().add(icon);
                    stage.showAndWait();

                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Login Panel");
                    alert.setHeaderText("Employee is not activeted..!");
                    alert.showAndWait();
                    user.setText(null);
                    password.setText(null);
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Login Panel");
                alert.setHeaderText("User name or password not matched..!");
                alert.showAndWait();
                user.setText(null);
                password.setText(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
