/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main_unit;

import DB.dbclass;
import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import tableview.daylly_collection_table_controller;
import tableview.grn_table_controller;
import tableview.instock_table_controller;
import tableview.invoice_table_controller;
import tableview.invoiceitem_table_controller;
import tableview.store_table_controller;

/**
 * FXML Controller class
 *
 * @author isuru
 */
public class Table_viweController implements Initializable {

    @FXML
    private TableView<tableview.store_table_controller> store_table;
    @FXML
    private TableColumn<tableview.store_table_controller, String> barcodeid;
    @FXML
    private TableColumn<tableview.store_table_controller, String> proname;
    @FXML
    private TableColumn<tableview.store_table_controller, String> description;
    @FXML
    private TableColumn<tableview.store_table_controller, String> measu;
    @FXML
    private TableColumn<tableview.store_table_controller, Double> purchaseprice;
    @FXML
    private TableColumn<tableview.store_table_controller, Double> standardcost;
    @FXML
    private TableColumn<tableview.store_table_controller, String> unitinstock;
    @FXML
    private TableColumn<tableview.store_table_controller, String> recodelevel;
    @FXML
    private TableColumn<tableview.store_table_controller, String> supname;
    @FXML
    private TableColumn<tableview.store_table_controller, String> ctname;
    @FXML
    private TableColumn<tableview.store_table_controller, String> subctname;
    @FXML
    private TableColumn<tableview.store_table_controller, String> rackno;
    @FXML
    private TableColumn<tableview.store_table_controller, String> mfdate;
    @FXML
    private TableColumn<tableview.store_table_controller, String> exdate;
    @FXML
    private TableColumn<tableview.store_table_controller, Double> unitprice;
    @FXML
    private TableColumn<tableview.store_table_controller, String> qty;
    @FXML
    private TableColumn<tableview.store_table_controller, Double> total;
    @FXML
    private TableColumn<tableview.store_table_controller, String> enterdate;
    @FXML
    private TableColumn<tableview.store_table_controller, String> enterup;
    @FXML
    private TableColumn<tableview.store_table_controller, String> status;
    @FXML
    private TableColumn<tableview.store_table_controller, String> empname;

    ObservableList<store_table_controller> TableList = FXCollections.observableArrayList();
    @FXML
    private TableView<tableview.instock_table_controller> table2;
    @FXML
    private TableColumn<tableview.instock_table_controller, Integer> c_proid;
    @FXML
    private TableColumn<tableview.instock_table_controller, Long> c_barid;
    @FXML
    private TableColumn<tableview.instock_table_controller, String> c_proname;
    @FXML
    private TableColumn<tableview.instock_table_controller, String> c_mfdate;
    @FXML
    private TableColumn<tableview.instock_table_controller, String> c_exdate;
    @FXML
    private TableColumn<tableview.instock_table_controller, String> c_qty;
    @FXML
    private TableColumn<tableview.instock_table_controller, Double> c_rprice;
    @FXML
    private TableColumn<tableview.instock_table_controller, Double> c_wprice;
    @FXML
    private TableColumn<tableview.instock_table_controller, Double> c_eprice;
    @FXML
    private TableColumn<tableview.instock_table_controller, String> c_tradate;
    @FXML
    private TableColumn<tableview.instock_table_controller, String> c_proupdate;
    @FXML
    private TableColumn<tableview.instock_table_controller, String> c_status;
    @FXML
    private TableColumn<tableview.instock_table_controller, Integer> c_empid;

    ObservableList<tableview.instock_table_controller> Tablelist2 = FXCollections.observableArrayList();
    @FXML
    private TableView<tableview.invoiceitem_table_controller> table3;
    @FXML
    private TableColumn<tableview.invoiceitem_table_controller, Integer> c_invitemid;
    @FXML
    private TableColumn<tableview.invoiceitem_table_controller, String> c_invid;
    @FXML
    private TableColumn<tableview.invoiceitem_table_controller, Long> c_inbarid;
    @FXML
    private TableColumn<tableview.invoiceitem_table_controller, String> c_iproname;
    @FXML
    private TableColumn<tableview.invoiceitem_table_controller, Double> c_iunitprice;
    @FXML
    private TableColumn<tableview.invoiceitem_table_controller, String> c_iqty;
    @FXML
    private TableColumn<tableview.invoiceitem_table_controller, Double> c_protatol;
    @FXML
    private TableColumn<tableview.invoiceitem_table_controller, Double> c_prodescount;
    @FXML
    private TableColumn<tableview.invoiceitem_table_controller, Double> c_isubtotal;

    ObservableList<tableview.invoiceitem_table_controller> Tablelist3 = FXCollections.observableArrayList();
    @FXML
    private TableView<tableview.invoice_table_controller> table4;
    @FXML
    private TableColumn<tableview.invoice_table_controller, String> c_invno2;
    @FXML
    private TableColumn<tableview.invoice_table_controller, String> c_cusid2;
    @FXML
    private TableColumn<tableview.invoice_table_controller, String> c_idate2;
    @FXML
    private TableColumn<tableview.invoice_table_controller, Double> c_itotal2;
    @FXML
    private TableColumn<tableview.invoice_table_controller, Double> c_ides2;
    @FXML
    private TableColumn<tableview.invoice_table_controller, Double> c_inettotal2;
    @FXML
    private TableColumn<tableview.invoice_table_controller, Double> c_ipay2;
    @FXML
    private TableColumn<tableview.invoice_table_controller, Double> c_bal2;
    @FXML
    private TableColumn<tableview.invoice_table_controller, String> c_pricetype;
    @FXML
    private TableColumn<tableview.invoice_table_controller, String> c_pymentm;
    @FXML
    private TableColumn<tableview.invoice_table_controller, Integer> c_empid2;

    ObservableList<tableview.invoice_table_controller> Tablelist4 = FXCollections.observableArrayList();
    @FXML
    private TableView<tableview.daylly_collection_table_controller> table5;
    @FXML
    private TableColumn<tableview.daylly_collection_table_controller, String> c_colldate;
    @FXML
    private TableColumn<tableview.daylly_collection_table_controller, Double> c_collcash;
    @FXML
    private TableColumn<tableview.daylly_collection_table_controller, Double> c_collcard;
    @FXML
    private TableColumn<tableview.daylly_collection_table_controller, Double> c_collcredit;
    @FXML
    private TableColumn<tableview.daylly_collection_table_controller, Double> c_collins;
    @FXML
    private TableColumn<tableview.daylly_collection_table_controller, Double> c_chnge;
    @FXML
    private TableColumn<tableview.daylly_collection_table_controller, Double> c_damage;
    @FXML
    private TableColumn<tableview.daylly_collection_table_controller, Double> c_expro;
    @FXML
    private TableColumn<tableview.daylly_collection_table_controller, Double> c_collcollection;
    @FXML
    private TableColumn<tableview.daylly_collection_table_controller, Double> c_entcollect;
    @FXML
    private TableColumn<tableview.daylly_collection_table_controller, Integer> c_collempid;

    ObservableList<tableview.daylly_collection_table_controller> Tablelist5 = FXCollections.observableArrayList();
    @FXML
    private TableView<tableview.grn_table_controller> grn_table;
    @FXML
    private TableColumn<tableview.grn_table_controller, String> c_grnid;
    @FXML
    private TableColumn<tableview.grn_table_controller, String> c_supid;
    @FXML
    private TableColumn<tableview.grn_table_controller, Integer> c_supname;
    @FXML
    private TableColumn<tableview.grn_table_controller, Integer> c_proid1;
    @FXML
    private TableColumn<tableview.grn_table_controller, String> c_proname1;
    @FXML
    private TableColumn<tableview.grn_table_controller, String> c_stock;
    @FXML
    private TableColumn<tableview.grn_table_controller, String> c_store;
    @FXML
    private TableColumn<tableview.grn_table_controller, String> c_cat;
    @FXML
    private TableColumn<tableview.grn_table_controller, String> c_catname;
    @FXML
    private TableColumn<tableview.grn_table_controller, String> c_qty1;
    @FXML
    private TableColumn<tableview.grn_table_controller, Double> c_price;
    @FXML
    private TableColumn<tableview.grn_table_controller, Double> c_tot;
    @FXML
    private TableColumn<tableview.grn_table_controller, Double> c_descount;
    @FXML
    private TableColumn<tableview.grn_table_controller, Double> c_nettot;
    @FXML
    private TableColumn<tableview.grn_table_controller, String> c_edate;
    @FXML
    private TableColumn<tableview.grn_table_controller, String> c_approvad;
    @FXML
    private TableColumn<tableview.grn_table_controller, Integer> c_empid1;

    ObservableList<tableview.grn_table_controller> Tablelist6 = FXCollections.observableArrayList();
    @FXML
    private TextField search1;
    @FXML
    private TextField search2;
    @FXML
    private TextField search3;
    @FXML
    private TextField search4;
    @FXML
    private TextField search5;
    @FXML
    private TextField search6;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Loadtable();
        Loadtable2();
        Loadtable3();
        Loadtable4();
        Loadtable5();
        Loadtable6();
        SetCellTable();
        SetCellTable2();
        SetCellTable3();
        SetCellTable4();
        SetCellTable5();
        SetCellTable6();

    }

    public void Loadtable() {
        try {
            ResultSet rs = dbclass.search("select * from store");
            while (rs.next()) {
                TableList.add(new store_table_controller(
                        rs.getString("barcode"),
                        rs.getString("name"),
                        rs.getString("description"),
                        rs.getString("Measurements"),
                        rs.getDouble("purchase_price"),
                        rs.getDouble("standard_cost"),
                        rs.getString("unit_in_stock"),
                        rs.getString("recoadlevel"),
                        rs.getString("supplerid"),
                        rs.getString("category_id"),
                        rs.getString("store_subcategoryname"),
                        rs.getString("store_rackno"),
                        rs.getString("Date_of_manufacture"),
                        rs.getString("Expired_date"),
                        rs.getDouble("unit_price"),
                        rs.getString("qty"),
                        rs.getDouble("total"),
                        rs.getString("enterdate"),
                        rs.getString("enterupdate"),
                        rs.getString("status"),
                        rs.getString("empid")
                ));
            }
            store_table.setItems(TableList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void SetCellTable() {
        barcodeid.setCellValueFactory(new PropertyValueFactory<>("Barcodeno"));
        proname.setCellValueFactory(new PropertyValueFactory<>("Productname"));
        description.setCellValueFactory(new PropertyValueFactory<>("Description"));
        measu.setCellValueFactory(new PropertyValueFactory<>("Measurements"));
        purchaseprice.setCellValueFactory(new PropertyValueFactory<>("Purchaseprice"));
        standardcost.setCellValueFactory(new PropertyValueFactory<>("Standardcost"));
        unitinstock.setCellValueFactory(new PropertyValueFactory<>("Unitinstock"));
        recodelevel.setCellValueFactory(new PropertyValueFactory<>("Recoadlevel"));
        supname.setCellValueFactory(new PropertyValueFactory<>("Supplerid"));
        ctname.setCellValueFactory(new PropertyValueFactory<>("Categoryid"));
        subctname.setCellValueFactory(new PropertyValueFactory<>("Storesubcategoryname"));
        rackno.setCellValueFactory(new PropertyValueFactory<>("Storerackno"));
        mfdate.setCellValueFactory(new PropertyValueFactory<>("Domdate"));
        exdate.setCellValueFactory(new PropertyValueFactory<>("Exdate"));
        unitprice.setCellValueFactory(new PropertyValueFactory<>("Unitprice"));
        qty.setCellValueFactory(new PropertyValueFactory<>("Qty"));
        total.setCellValueFactory(new PropertyValueFactory<>("Total"));
        enterdate.setCellValueFactory(new PropertyValueFactory<>("Enterdate"));
        enterup.setCellValueFactory(new PropertyValueFactory<>("Enterupdate"));
        status.setCellValueFactory(new PropertyValueFactory<>("Status"));
        empname.setCellValueFactory(new PropertyValueFactory<>("Empname"));
    }

    public void Loadtable2() {
        try {
            ResultSet rs = dbclass.search("select * from instock");
            while (rs.next()) {
                Tablelist2.add(new instock_table_controller(
                        rs.getInt("productid"),
                        rs.getLong("barcode"),
                        rs.getString("productname"),
                        rs.getString("mfdate"),
                        rs.getString("exdate"),
                        rs.getString("qty"),
                        rs.getDouble("retailprice"),
                        rs.getDouble("wholesaleprice"),
                        rs.getDouble("exterpirce"),
                        rs.getString("transerdate"),
                        rs.getString("enterupdate"),
                        rs.getString("status"),
                        rs.getInt("empid")));
            }
            table2.setItems(Tablelist2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void SetCellTable2() {
        c_proid.setCellValueFactory(new PropertyValueFactory<>("Productid"));
        c_barid.setCellValueFactory(new PropertyValueFactory<>("Barcode"));
        c_proname.setCellValueFactory(new PropertyValueFactory<>("Productname"));
        c_mfdate.setCellValueFactory(new PropertyValueFactory<>("Mfdate"));
        c_exdate.setCellValueFactory(new PropertyValueFactory<>("Exdate"));
        c_qty.setCellValueFactory(new PropertyValueFactory<>("Qty"));
        c_rprice.setCellValueFactory(new PropertyValueFactory<>("Retailprice"));
        c_wprice.setCellValueFactory(new PropertyValueFactory<>("Wholesaleprice"));
        c_eprice.setCellValueFactory(new PropertyValueFactory<>("Exterpirce"));
        c_tradate.setCellValueFactory(new PropertyValueFactory<>("Transerdate"));
        c_proupdate.setCellValueFactory(new PropertyValueFactory<>("Enterupdate"));
        c_status.setCellValueFactory(new PropertyValueFactory<>("Status"));
        c_empid.setCellValueFactory(new PropertyValueFactory<>("Empid"));

    }

    public void Loadtable3() {
        try {
            ResultSet rs = dbclass.search("select * from invoiceitem");
            while (rs.next()) {
                Tablelist3.add(new invoiceitem_table_controller(
                        rs.getInt("invoiceitemid"),
                        rs.getString("invoicid"),
                        rs.getLong("barcode"),
                        rs.getString("itemname"),
                        rs.getDouble("unitprice"),
                        rs.getString("qty"),
                        rs.getDouble("itemtotal"),
                        rs.getDouble("discount1"),
                        rs.getDouble("subtotal")));
            }
            table3.setItems(Tablelist3);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void SetCellTable3() {
        c_invitemid.setCellValueFactory(new PropertyValueFactory<>("Invoiceitemid"));
        c_invid.setCellValueFactory(new PropertyValueFactory<>("Invoiceid"));
        c_inbarid.setCellValueFactory(new PropertyValueFactory<>("Productid"));
        c_iproname.setCellValueFactory(new PropertyValueFactory<>("Itemname"));
        c_iunitprice.setCellValueFactory(new PropertyValueFactory<>("Unitprice"));
        c_iqty.setCellValueFactory(new PropertyValueFactory<>("Qty"));
        c_isubtotal.setCellValueFactory(new PropertyValueFactory<>("Itemtotal"));
        c_prodescount.setCellValueFactory(new PropertyValueFactory<>("Discount"));
        c_isubtotal.setCellValueFactory(new PropertyValueFactory<>("Subtotal"));

    }

    public void Loadtable4() {
        try {
            ResultSet rs = dbclass.search("select * from invoice");
            while (rs.next()) {
                Tablelist4.add(new invoice_table_controller(
                        rs.getString("invoicid"),
                        rs.getString("customer_id"),
                        rs.getString("enterdate"),
                        rs.getDouble("total"),
                        rs.getDouble("descount2"),
                        rs.getDouble("nettotal"),
                        rs.getDouble("payment"),
                        rs.getDouble("blance"),
                        rs.getString("pricetype"),
                        rs.getString("paymentmethod"),
                        rs.getInt("empid")));
            }
            table4.setItems(Tablelist4);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void SetCellTable4() {
        c_invno2.setCellValueFactory(new PropertyValueFactory<>("Invoiceno"));
        c_cusid2.setCellValueFactory(new PropertyValueFactory<>("Customerid"));
        c_idate2.setCellValueFactory(new PropertyValueFactory<>("Enterdate"));
        c_itotal2.setCellValueFactory(new PropertyValueFactory<>("Total"));
        c_ides2.setCellValueFactory(new PropertyValueFactory<>("Descount2"));
        c_inettotal2.setCellValueFactory(new PropertyValueFactory<>("Nettotal"));
        c_ipay2.setCellValueFactory(new PropertyValueFactory<>("Payment"));
        c_bal2.setCellValueFactory(new PropertyValueFactory<>("Blance"));
        c_pricetype.setCellValueFactory(new PropertyValueFactory<>("Pricetype"));
        c_pymentm.setCellValueFactory(new PropertyValueFactory<>("Paymentmethod"));
        c_empid2.setCellValueFactory(new PropertyValueFactory<>("Empid"));

    }

    public void Loadtable5() {
        try {
            ResultSet rs = dbclass.search("select * from dally_collection");
            while (rs.next()) {
                Tablelist5.add(new daylly_collection_table_controller(
                        rs.getString("enterdate"),
                        rs.getDouble("cashamount"),
                        rs.getDouble("cardamount"),
                        rs.getDouble("creditamount"),
                        rs.getDouble("Institutionalamount"),
                        rs.getDouble("changeinvamount"),
                        rs.getDouble("damageinvamount"),
                        rs.getDouble("exproamount"),
                        rs.getDouble("totalamount"),
                        rs.getDouble("enteramount"),
                        rs.getInt("empid")));
            }
            table5.setItems(Tablelist5);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void SetCellTable5() {
        c_colldate.setCellValueFactory(new PropertyValueFactory<>("Collectiondate"));
        c_collcash.setCellValueFactory(new PropertyValueFactory<>("Cashamount"));
        c_collcard.setCellValueFactory(new PropertyValueFactory<>("Cardamount"));
        c_collcredit.setCellValueFactory(new PropertyValueFactory<>("Creditamount"));
        c_collins.setCellValueFactory(new PropertyValueFactory<>("InstitutionalAmount"));
        c_chnge.setCellValueFactory(new PropertyValueFactory<>("Changeitemamount"));
        c_damage.setCellValueFactory(new PropertyValueFactory<>("Damageitemamount"));
        c_expro.setCellValueFactory(new PropertyValueFactory<>("EXitemamount"));
        c_collcollection.setCellValueFactory(new PropertyValueFactory<>("Totalamount"));
        c_entcollect.setCellValueFactory(new PropertyValueFactory<>("Enteramount"));
        c_collempid.setCellValueFactory(new PropertyValueFactory<>("Empid"));

    }

    public void Loadtable6() {
        try {
            ResultSet rs = dbclass.search("select * from grn");
            {
                while (rs.next()) {

                    Tablelist6.add(new grn_table_controller(
                            rs.getInt("grnid"),
                            rs.getString("supplerid"),
                            rs.getString("supplername"),
                            rs.getInt("productid"),
                            rs.getString("productname"),
                            rs.getString("unitinstock"),
                            rs.getString("unitinstore"),
                            rs.getString("category"),
                            rs.getString("categoryname"),
                            rs.getString("qty"),
                            rs.getDouble("price"),
                            rs.getDouble("total"),
                            rs.getDouble("descount"),
                            rs.getDouble("nettotal"),
                            rs.getString("enterdate"),
                            rs.getString("approvad"),
                            rs.getInt("empid")));
                }
                grn_table.setItems(Tablelist6);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void SetCellTable6() {
        c_grnid.setCellValueFactory(new PropertyValueFactory<>("Grnid"));
        c_supid.setCellValueFactory(new PropertyValueFactory<>("Supplerid"));
        c_supname.setCellValueFactory(new PropertyValueFactory<>("Supplername"));
        c_proid1.setCellValueFactory(new PropertyValueFactory<>("Productid"));
        c_proname1.setCellValueFactory(new PropertyValueFactory<>("Productname"));
        c_stock.setCellValueFactory(new PropertyValueFactory<>("Unitinstock"));
        c_store.setCellValueFactory(new PropertyValueFactory<>("Unitinstore"));
        c_cat.setCellValueFactory(new PropertyValueFactory<>("Categoryid"));
        c_catname.setCellValueFactory(new PropertyValueFactory<>("Categoryname"));
        c_qty1.setCellValueFactory(new PropertyValueFactory<>("Qty"));
        c_price.setCellValueFactory(new PropertyValueFactory<>("Price"));
        c_tot.setCellValueFactory(new PropertyValueFactory<>("Total"));
        c_descount.setCellValueFactory(new PropertyValueFactory<>("Descount"));
        c_nettot.setCellValueFactory(new PropertyValueFactory<>("Nettotal"));
        c_edate.setCellValueFactory(new PropertyValueFactory<>("Enterdate"));
        c_approvad.setCellValueFactory(new PropertyValueFactory<>("Approvad"));
        c_empid1.setCellValueFactory(new PropertyValueFactory<>("Empid"));
    }

    @FXML
    private void search_product(KeyEvent event) {
        try {
            FilteredList<tableview.store_table_controller> filterlist = new FilteredList<>(TableList, b -> true);
            search1.textProperty().addListener((observable, oldValue, newValue) -> {
                filterlist.setPredicate(Store -> {
                    if (newValue.isEmpty() || newValue.isEmpty() || newValue == null) {
                        return true;
                    }
                    String lowercasefilter = newValue.toLowerCase();

                    if (Store.getBarcodeno().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(Store.getStandardcost()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(Store.getTotal()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(Store.getUnitprice()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(Store.getUnitinstock()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(Store.getPurchaseprice()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (Store.getEnterdate().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    }else if (Store.getDescription().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    }else if (Store.getQty() .toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    }else if (Store.getCategoryid() .toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (Store.getProductname() .toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (Store.getSupplerid() .toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else {
                        return false;
                    }

                });
            });
            SortedList<tableview.store_table_controller> sortedlist = new SortedList<>(filterlist);
            sortedlist.comparatorProperty().bind(store_table.comparatorProperty());
            store_table.setItems(sortedlist);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void search_stock(KeyEvent event) {
        try {
            FilteredList<tableview.instock_table_controller> filterlist = new FilteredList<>(Tablelist2, b -> true);
            search2.textProperty().addListener((observable, oldValue, newValue) -> {
                filterlist.setPredicate(Stock -> {
                    if (newValue.isEmpty() || newValue.isEmpty() || newValue == null) {
                        return true;
                    }
                    String lowercasefilter = newValue.toLowerCase();

                    if (Stock.getProductname().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(Stock.getBarcode()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(Stock.getProductid()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(Stock.getRetailprice()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(Stock.getWholesaleprice()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(Stock.getExterpirce()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (Stock.getExdate().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    }else if (Stock.getMfdate().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    }else if (Stock.getQty() .toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (Stock.getStatus() .toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (Stock.getTranserdate() .toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else {
                        return false;
                    }

                });
            });
            SortedList<tableview.instock_table_controller> sortedlist = new SortedList<>(filterlist);
            sortedlist.comparatorProperty().bind(table2.comparatorProperty());
            table2.setItems(sortedlist);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void search_invoice(KeyEvent event) {
         try {
            FilteredList<tableview.invoice_table_controller> filterlist = new FilteredList<>(Tablelist4, b -> true);
            search3.textProperty().addListener((observable, oldValue, newValue) -> {
                filterlist.setPredicate(Invoice -> {
                    if (newValue.isEmpty() || newValue.isEmpty() || newValue == null) {
                        return true;
                    }
                    String lowercasefilter = newValue.toLowerCase();

                    if (Invoice.getInvoiceno().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(Invoice.getNettotal()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (Invoice.getPricetype().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    }else if (Invoice.getPaymentmethod() .toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    }else if (Invoice.getEnterdate() .toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (Invoice.getCustomerid() .toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    }  else {
                        return false;
                    }

                });
            });
            SortedList<tableview.invoice_table_controller> sortedlist = new SortedList<>(filterlist);
            sortedlist.comparatorProperty().bind(table4.comparatorProperty());
            table4.setItems(sortedlist);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void search_invoice_items(KeyEvent event) {
         try {
            FilteredList<tableview.invoiceitem_table_controller> filterlist = new FilteredList<>(Tablelist3, b -> true);
            search4.textProperty().addListener((observable, oldValue, newValue) -> {
                filterlist.setPredicate(Invoice_Items -> {
                    if (newValue.isEmpty() || newValue.isEmpty() || newValue == null) {
                        return true;
                    }
                    String lowercasefilter = newValue.toLowerCase();

                    if (Invoice_Items.getInvoiceid() .toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(Invoice_Items.getItemtotal()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (Invoice_Items.getItemname() .toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    }else if (Invoice_Items.getQty() .toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    }  else {
                        return false;
                    }

                });
            });
            SortedList<tableview.invoiceitem_table_controller> sortedlist = new SortedList<>(filterlist);
            sortedlist.comparatorProperty().bind(table3.comparatorProperty());
            table3.setItems(sortedlist);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void search_dylly_collection(KeyEvent event) {
          try {
            FilteredList<tableview.daylly_collection_table_controller> filterlist = new FilteredList<>(Tablelist5, b -> true);
            search5.textProperty().addListener((observable, oldValue, newValue) -> {
                filterlist.setPredicate(Collection -> {
                    if (newValue.isEmpty() || newValue.isEmpty() || newValue == null) {
                        return true;
                    }
                    String lowercasefilter = newValue.toLowerCase();

                    if (Collection.getCollectiondate().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(Collection.getCashamount()).indexOf(lowercasefilter) > -1) {
                        return true;
                    }else if (String.valueOf(Collection.getCardamount()).indexOf(lowercasefilter) > -1) {
                        return true;
                    }else if (String.valueOf(Collection.getCreditamount()).indexOf(lowercasefilter) > -1) {
                        return true;
                    }else if (String.valueOf(Collection.getEXitemamount()).indexOf(lowercasefilter) > -1) {
                        return true;
                    }else if (String.valueOf(Collection.getTotalamount()).indexOf(lowercasefilter) > -1) {
                        return true;
                    }else if (String.valueOf(Collection.getInstitutionalAmount()).indexOf(lowercasefilter) > -1) {
                        return true;
                    }else if (String.valueOf(Collection.getDamageitemamount()).indexOf(lowercasefilter) > -1) {
                        return true;
                    }  else {
                        return false;
                    }

                });
            });
            SortedList<tableview.daylly_collection_table_controller> sortedlist = new SortedList<>(filterlist);
            sortedlist.comparatorProperty().bind(table5.comparatorProperty());
            table5.setItems(sortedlist);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void search_grn(KeyEvent event) {
         try {
            FilteredList<tableview.grn_table_controller> filterlist = new FilteredList<>(Tablelist6, b -> true);
            search6.textProperty().addListener((observable, oldValue, newValue) -> {
                filterlist.setPredicate(Grn -> {
                    if (newValue.isEmpty() || newValue.isEmpty() || newValue == null) {
                        return true;
                    }
                    String lowercasefilter = newValue.toLowerCase();

                    if (Grn.getApprovad().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(Grn.getNettotal()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(Grn.getTotal()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(Grn.getPrice()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(Grn.getUnitinstock()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(Grn.getDescount()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (Grn.getQty() .toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (Grn.getProductname() .toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (Grn.getSupplerid() .toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (Grn.getSupplername() .toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    }else {
                        return false;
                    }

                });
            });
            SortedList<tableview.grn_table_controller> sortedlist = new SortedList<>(filterlist);
            sortedlist.comparatorProperty().bind(grn_table.comparatorProperty());
            grn_table.setItems(sortedlist);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
