/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main_unit;

import DB.dbclass;
import DB.systemcomfigdata;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import tableview.grn_table_controller;

/**
 * FXML Controller class
 *
 * @author isuru
 */
public class Grn_viweController implements Initializable {

    @FXML
    private JFXTextField subid;
    @FXML
    private JFXTextField supname;
    @FXML
    private JFXTextField grnid;
    @FXML
    private JFXTextField proid;
    @FXML
    private JFXTextField proname;
    @FXML
    private JFXTextField unitinstock;
    @FXML
    private JFXTextField unitinstore;
    @FXML
    private JFXTextField category;
    @FXML
    private JFXTextField qty;
    @FXML
    private JFXTextField price;
    @FXML
    private JFXTextField descount;
    @FXML
    private JFXTextField total;
    @FXML
    private JFXCheckBox approvad;
    @FXML
    private TableView<tableview.grn_table_controller> grn_table;
    @FXML
    private TableColumn<tableview.grn_table_controller, String> c_supid;
    @FXML
    private TableColumn<tableview.grn_table_controller, String> c_supname;
    @FXML
    private TableColumn<tableview.grn_table_controller, Integer> c_grnid;
    @FXML
    private TableColumn<tableview.grn_table_controller, Integer> c_proid;
    @FXML
    private TableColumn<tableview.grn_table_controller, String> c_proname;
    @FXML
    private TableColumn<tableview.grn_table_controller, String> c_stock;
    @FXML
    private TableColumn<tableview.grn_table_controller, String> c_store;
    @FXML
    private TableColumn<tableview.grn_table_controller, String> c_cat;
    @FXML
    private TableColumn<tableview.grn_table_controller, String> c_catname;
    @FXML
    private TableColumn<tableview.grn_table_controller, String> c_qty;
    @FXML
    private TableColumn<tableview.grn_table_controller, Double> c_price;
    @FXML
    private TableColumn<tableview.grn_table_controller, Double> c_tot;
    @FXML
    private TableColumn<tableview.grn_table_controller, Double> c_descount;
    @FXML
    private TableColumn<tableview.grn_table_controller, Double> c_nettot;
    @FXML
    private TableColumn<tableview.grn_table_controller, String> c_edate;
    @FXML
    private TableColumn<tableview.grn_table_controller, String> c_approvad;
    @FXML
    private TableColumn<tableview.grn_table_controller, Integer> c_empid;

    ObservableList<tableview.grn_table_controller> Tablelist = FXCollections.observableArrayList();
    @FXML
    private JFXDatePicker today;
    @FXML
    private ListView<String> listsupid;
    private JFXTextField catname;
    @FXML
    private JFXTextField Cat_name;
    @FXML
    private JFXTextField nettotal;

    Date d = new Date();
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
    @FXML
    private ListView<String> grnlist;
    @FXML
    private JFXTextField search;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ganaretgrnid();
        listsupid.setVisible(false);
        grnlist.setVisible(false);
        Loadtable();
        Setcell();
    }

    String appro;
    String type;

    @FXML
    private void btn_grn_save(ActionEvent event) {
        try {
            if (!qty.getText().trim().equals("") && !price.getText().trim().equals("") && !total.getText().trim().equals("") && !descount.getText().trim().equals("") && !nettotal.getText().trim().equals("")) {
                if (approvad.isSelected()) {
                    appro = "Appovaed";

                } else {
                    appro = "Nonapprovaed";

                }

                selectmashiment();
                dbclass.push("insert into grn(supplerid,supplername,productid,productname,unitinstock,unitinstore,category,categoryname,qty,price,total,descount,nettotal,enterdate,approvad,empid) values ('" + subid.getText() + "','" + supname.getText() + "','" + proid.getText() + "','" + proname.getText() + "','" + unitinstock.getText() + "','" + unitinstore.getText() + "','" + category.getText() + "','" + Cat_name.getText() + "','" + type + "','" + price.getText() + "','" + total.getText() + "','" + descount.getText() + "','" + nettotal.getText() + "','" + date.format(d) + "','" + appro + "','" + systemcomfigdata.getEmpid() + "')");
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Product");
                alert.setHeaderText("Grn items  is saved");
                alert.setContentText("Grn report Is ok");
                alert.showAndWait();
                Clear();
                grn_table.getItems().clear();
                Loadtable();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_grn_update(ActionEvent event) {
        try {

            if (approvad.isSelected()) {
                appro = "Appovaed";

            } else {
                appro = "Nonapprovaed";

            }
            selectmashiment();
            dbclass.push("update grn set supplerid = '" + subid.getText() + "', supplername = '" + supname.getText() + "', productid = '" + proid.getText() + "', productname = '" + proname.getText() + "', unitinstock = '" + unitinstock.getText() + "', unitinstore = '" + unitinstore.getText() + "', category = '" + category.getText() + "', categoryname = '" + Cat_name.getText() + "', qty = '" + type + "', price = '" + price.getText() + "', total = '" + total.getText() + "', descount = '" + descount.getText() + "', nettotal = '" + nettotal.getText() + "', enterdate = '" + date.format(d) + "', approvad = '" + appro + "', empid = '" + systemcomfigdata.getEmpid() + "' where grnid = '" + grnid.getText() + "'");
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Product");
            alert.setHeaderText("Grn items  Update Success");
            alert.setContentText("Grn report updateed");
            grn_table.getItems().clear();
            Loadtable();
            alert.showAndWait();
            Clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_clear(ActionEvent event) {
        Clear();
    }

    public void ganaretgrnid() {
        try {
            ResultSet rs = dbclass.search("select count(*) as grnid from grn");
            if (rs.next()) {
                int COunt = rs.getInt("grnid");
                String Id = "" + (++COunt);
                grnid.setText(Id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setsupidlist() {
        try {
            ObservableList<String> listadd = FXCollections.observableArrayList();
            ResultSet rs = dbclass.search("select * from add_supplier");
            while (rs.next()) {
                listadd.add(rs.getString("supplierid"));
            }
            listsupid.setItems(listadd);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_action_supid(KeyEvent event) {
        setsupidlist();
        listsupid.setVisible(true);
    }

    @FXML
    private void btn_Action_enter(ActionEvent event) {
        try {
            ResultSet rs = dbclass.search("select * from add_supplier where supplierid = '" + subid.getText() + "'");
            if (rs.next()) {
                supname.setText(rs.getString("companyname"));
            } else {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Suppler ID");
                alert.setHeaderText("Subller Id is invalied");
                alert.setContentText("Enter the correct suppler id");
                alert.showAndWait();
            }
            listsupid.setVisible(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void mouse_clik_supidlist(MouseEvent event) {
        if (event.getClickCount() == 2) {
            ObservableList selectedItems = listsupid.getSelectionModel().getSelectedItems();
            subid.setText("" + selectedItems.get(0));
            try {
                ResultSet rs = dbclass.search("select * from add_supplier where supplierid = '" + subid.getText() + "'");
                if (rs.next()) {
                    supname.setText(rs.getString("companyname"));
                }
                listsupid.setVisible(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void action_proid(ActionEvent event) {
        try {
            ResultSet rs2 = dbclass.search("select * from store where code = '" + proid.getText() + "'");
            if (rs2.next()) {
                proname.setText(rs2.getString("name"));
                unitinstore.setText(rs2.getString("qty"));
                category.setText(rs2.getString("category_id"));
                SetValue();

            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Product ID");
                alert.setHeaderText("Product Id is invalied");
                alert.setContentText("Enter the correct Product id");
                alert.showAndWait();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void SetValue() {
        try {
            ResultSet rs = dbclass.search("select * from category where category_id = '" + category.getText() + "'");
            ResultSet rs2 = dbclass.search("select * from instock where productid = '" + proid.getText() + "'");

            if (rs.next()) {
                Cat_name.setText(rs.getString("categoryname"));
            }
            if (rs2.next()) {
                unitinstock.setText(rs2.getString("qty"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_full_total(ActionEvent event) {
        try {
            double Qty = Double.parseDouble(qty.getText());
            double Price = Double.parseDouble(price.getText());

            double sum = Qty * Price;
            DecimalFormat df = new DecimalFormat("#.##");
            String format = df.format(sum);
            total.setText("" + format);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_descount(ActionEvent event) {

        double Total = Double.parseDouble(total.getText());
        int disc1 = Integer.parseInt(descount.getText());

        double Discount2 = Total * (100 - disc1) / 100;
        DecimalFormat df = new DecimalFormat("#.##");
        String format = df.format(Discount2);
        nettotal.setText("" + format);
    }

    @FXML
    private void btn_approved(ActionEvent event) {
        if (approvad.isSelected()) {
            appro = "Appovaed";

        } else {
            appro = "Nonapprovaed";
        }
    }

    private void Clear() {
        subid.setText(null);
        supname.setText(null);
        ganaretgrnid();
        proid.setText(null);
        proname.setText(null);
        unitinstock.setText(null);
        unitinstore.setText(null);
        category.setText(null);
        Cat_name.setText(null);
        qty.setText(null);
        price.setText(null);
        total.setText(null);
        descount.setText(null);
        nettotal.setText(null);
        approvad.setSelected(false);
    }

    @FXML
    private void nullsizotext(MouseEvent event) {
        descount.setText(null);
    }

    public void selectmashiment() {
        String name = unitinstock.getText().split("-")[1];
        System.out.println(name);
        if (name.equals("(Kg)")) {
            type = "" + qty.getText() + "-(Kg)";
        } else if (name.equals("(g)")) {
            type = "" + qty.getText() + "-(g)";
        } else if (name.equals("(l)")) {
            type = "" + qty.getText() + "-(l)";
        } else if (name.equals("(ml)")) {
            type = "" + qty.getText() + "-(ml)";
        } else if (name.equals("(Pcs)")) {
            type = "" + qty.getText() + "-(Pcs)";
        }
    }

    @FXML
    private void listsearch(MouseEvent event) {
        if (event.getClickCount() == 2) {
            ObservableList<String> selectedItems = grnlist.getSelectionModel().getSelectedItems();
            search.setText("" + selectedItems.get(0).split(" ")[0]);

            try {
                ResultSet rs = dbclass.search("select * from grn where grnid = '" + search.getText() + "'");
                if (rs.next()) {
                    grnid.setText(rs.getString("grnid"));
                    subid.setText(rs.getString("supplerid"));
                    supname.setText(rs.getString("supplername"));
                    proid.setText(rs.getString("productid"));
                    proname.setText(rs.getString("productname"));
                    unitinstock.setText(rs.getString("unitinstock"));
                    unitinstore.setText(rs.getString("unitinstore"));
                    category.setText(rs.getString("category"));
                    Cat_name.setText(rs.getString("categoryname"));
                    qty.setText(rs.getString("qty").split("-")[0]);
                    price.setText(rs.getString("price"));
                    total.setText(rs.getString("total"));
                    descount.setText(rs.getString("descount"));
                    nettotal.setText(rs.getString("nettotal"));
                    String Status = rs.getString("approvad");

                    if (Status.equals("approvad ")) {
                        approvad.setSelected(true);
                    } else {
                        approvad.setSelected(false);
                    }
                     grnlist.setVisible(false);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void loadlist() {
        try {
            ObservableList List = FXCollections.observableArrayList();
            ResultSet rs = dbclass.search("select * from grn");
            while (rs.next()) {
                List.add(rs.getString("grnid") + " " + rs.getString("productname") + " " + rs.getString("supplername"));
            }
            grnlist.setItems(List);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_search_grn_list(ActionEvent event) {
        try {

            ResultSet rs = dbclass.search("select * from grn");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void listview(MouseEvent event) {
        loadlist();
        grnlist.setVisible(true);
    }

    public void Loadtable() {
        try {
            ResultSet rs = dbclass.search("select * from grn");
            {
                while (rs.next()) {
                    String date1 = rs.getString("enterdate");
                    String date2 = date.format(d);
                    date1 += " 00:00:00";
                    date2 += " 00:00:00";
                    if (date1.equals(date2)) {
                        Tablelist.add(new grn_table_controller(
                                rs.getInt("grnid"),
                                rs.getString("supplerid"),
                                rs.getString("supplername"),
                                rs.getInt("productid"),
                                rs.getString("productname"),
                                rs.getString("unitinstock"),
                                rs.getString("unitinstore"),
                                rs.getString("category"),
                                rs.getString("categoryname"),
                                rs.getString("qty"),
                                rs.getDouble("price"),
                                rs.getDouble("total"),
                                rs.getDouble("descount"),
                                rs.getDouble("nettotal"),
                                rs.getString("enterdate"),
                                rs.getString("approvad"),
                                rs.getInt("empid")));
                    }
                    grn_table.setItems(Tablelist);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void Setcell() {
        c_grnid.setCellValueFactory(new PropertyValueFactory<>("Grnid"));
        c_supid.setCellValueFactory(new PropertyValueFactory<>("Supplerid"));
        c_supname.setCellValueFactory(new PropertyValueFactory<>("Supplername"));
        c_proid.setCellValueFactory(new PropertyValueFactory<>("Productid"));
        c_proname.setCellValueFactory(new PropertyValueFactory<>("Productname"));
        c_stock.setCellValueFactory(new PropertyValueFactory<>("Unitinstock"));
        c_store.setCellValueFactory(new PropertyValueFactory<>("Unitinstore"));
        c_cat.setCellValueFactory(new PropertyValueFactory<>("Categoryid"));
        c_catname.setCellValueFactory(new PropertyValueFactory<>("Categoryname"));
        c_qty.setCellValueFactory(new PropertyValueFactory<>("Qty"));
        c_price.setCellValueFactory(new PropertyValueFactory<>("Price"));
        c_tot.setCellValueFactory(new PropertyValueFactory<>("Total"));
        c_descount.setCellValueFactory(new PropertyValueFactory<>("Descount"));
        c_nettot.setCellValueFactory(new PropertyValueFactory<>("Nettotal"));
        c_edate.setCellValueFactory(new PropertyValueFactory<>("Enterdate"));
        c_approvad.setCellValueFactory(new PropertyValueFactory<>("Approvad"));
        c_empid.setCellValueFactory(new PropertyValueFactory<>("Empid"));
    }

    @FXML
    private void tablemouseclick(MouseEvent event) {
        try {

            int grnid1 = grn_table.getSelectionModel().getSelectedItem().getGrnid();
            search.setText("" + grnid1);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
