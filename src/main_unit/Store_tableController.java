/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main_unit;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * FXML Controller class
 *
 * @author isuru
 */
public class Store_tableController implements Initializable {

    @FXML
    private TableView<?> store_table;
    @FXML
    private TableColumn<?, ?> barcodeid;
    @FXML
    private TableColumn<?, ?> proname;
    @FXML
    private TableColumn<?, ?> description;
    @FXML
    private TableColumn<?, ?> measu;
    @FXML
    private TableColumn<?, ?> purchaseprice;
    @FXML
    private TableColumn<?, ?> standardcost;
    @FXML
    private TableColumn<?, ?> unitinstock;
    @FXML
    private TableColumn<?, ?> recodelevel;
    @FXML
    private TableColumn<?, ?> supname;
    @FXML
    private TableColumn<?, ?> ctname;
    @FXML
    private TableColumn<?, ?> subctname;
    @FXML
    private TableColumn<?, ?> rackno;
    @FXML
    private TableColumn<?, ?> mfdate;
    @FXML
    private TableColumn<?, ?> exdate;
    @FXML
    private TableColumn<?, ?> unitprice;
    @FXML
    private TableColumn<?, ?> qty;
    @FXML
    private TableColumn<?, ?> total;
    @FXML
    private TableColumn<?, ?> enterdate;
    @FXML
    private TableColumn<?, ?> enterup;
    @FXML
    private TableColumn<?, ?> status;
    @FXML
    private TableColumn<?, ?> empname;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
