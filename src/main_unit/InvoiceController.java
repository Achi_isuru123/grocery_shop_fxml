/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main_unit;

import DB.dbclass;
import DB.systemcomfigdata;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.view.JasperViewer;
import tableview.price_table_controller;
import tableview.productlist_table_controller;

/**
 * FXML Controller class
 *
 * @author isuru
 */
public class InvoiceController implements Initializable {

    @FXML
    private JFXTextField invno;
    @FXML
    private JFXTextField today;
    @FXML
    private JFXTextField cusname;
    private JFXComboBox<String> cusidcombo;
    @FXML
    private JFXRadioButton retailprice;
    @FXML
    private JFXRadioButton wholesaleprice;
    @FXML
    private JFXRadioButton extraprice;
    @FXML
    private Pane paneloptin;
    @FXML
    private Label todaytime;

    Date d = new Date();
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MMM-dd");
    @FXML
    private FontAwesomeIconView showpane;
    @FXML
    private JFXTextField cusid;
    @FXML
    private ListView<String> cusidlist;
    @FXML
    private ToggleGroup price1;
    @FXML
    private JFXTextField proname;
    @FXML
    private JFXTextField price;
    @FXML
    private JFXTextField qtyAvB;
    @FXML
    private JFXTextField qty;
    @FXML
    private JFXTextField totalprice;
    @FXML
    private JFXTextField discount1;
    @FXML
    private JFXTextField subtotal;

    String Mashiment;
    @FXML
    private TableView<tableview.price_table_controller> pricetable;
    @FXML
    private TableColumn<tableview.price_table_controller, String> c_proid;
    @FXML
    private TableColumn<tableview.price_table_controller, String> c_barid;
    @FXML
    private TableColumn<tableview.price_table_controller, String> c_proname;
    @FXML
    private TableColumn<tableview.price_table_controller, String> c_unitprice;
    @FXML
    private TableColumn<tableview.price_table_controller, String> c_qty;
    @FXML
    private TableColumn<tableview.price_table_controller, String> c_discount1;
    @FXML
    private TableColumn<tableview.price_table_controller, String> c_subtotal;
    @FXML
    private TableColumn<tableview.price_table_controller, String> c_prototal;
    @FXML
    private TextField itemcount;
    @FXML
    private JFXTextField barcodid;
    @FXML
    private TextField newtotal;
    @FXML
    private ToggleGroup price2;
    @FXML
    private TextField discount2;
    @FXML
    private TextField nettotal;
    @FXML
    private TextField payment;
    @FXML
    private TextField balans;
    @FXML
    private JFXRadioButton cash;
    @FXML
    private JFXRadioButton card;
    @FXML
    private JFXRadioButton credit;
    @FXML
    private Label cregittotal;
    @FXML
    private Pane searchpane;
    @FXML
    private TableView<tableview.productlist_table_controller> table8;
    @FXML
    private TableColumn<tableview.productlist_table_controller, Long> col_barno;
    @FXML
    private TableColumn<tableview.productlist_table_controller, String> col_barname;

    ObservableList<tableview.productlist_table_controller> TableList = FXCollections.observableArrayList();
    @FXML
    private TableColumn<tableview.productlist_table_controller, Double> col_price;
//    double sum;
    @FXML
    private TextField searchproduct;
    @FXML
    private JFXTextField proid;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        genaretinvoiceid();
        setcustomercombox();
        today.setText(date.format(d));
        paneloptin.setVisible(false);
        showpane.setVisible(false);
        cusidlist.setVisible(false);
        discount1.setEditable(false);
        setSell();
        timenow();
        cregittotal.setText("0.00");
        searchpane.setVisible(true);
        loadtable8();
        setcol8();
    }

    @FXML
    private void btnretailprice(ActionEvent event) {
        if (itemcount.getText().trim().equals("") || itemcount.getText().trim().equals("0")) {
            retailprice.setVisible(false);
            extraprice.setVisible(false);
        }
    }

    @FXML
    private void cliklist(MouseEvent event) {
        String Id = cusidlist.getSelectionModel().getSelectedItem().split("-")[0];
        cusid.setText(Id);
        String name = cusidlist.getSelectionModel().getSelectedItem().split("-")[1];
        cusname.setText(name);
        cusidlist.setVisible(false);
        cerfitcustome();
    }

    @FXML
    private void cliktable(MouseEvent event) {
        if (event.getClickCount() == 2) {
            price_table_controller p = pricetable.getSelectionModel().getSelectedItem();
            qty.setText(p.getProductqty().split("-")[0]);
            totalprice.setText(p.getProducttotal());
            discount1.setText(p.getProductdiscount());
            subtotal.setText(p.getSubtotal());
            price.setText(p.getUnitprice());
            proid.setText(p.getProductid());
            proname.setText(p.getProductname());
            ;
        }
    }

    @FXML
    private void cliktext(MouseEvent event) {
        cusidlist.setVisible(true);
    }

    @FXML
    private void btnwholesaleprice(ActionEvent event) {
        if (itemcount.getText().trim().equals("") || itemcount.getText().trim().equals("0")) {
            retailprice.setVisible(false);
            extraprice.setVisible(false);
        }
    }

    @FXML
    private void btnextraprice(ActionEvent event) {
    }

    @FXML
    private void btninvoiceoption(ActionEvent event) {
        paneloptin.setVisible(true);
        showpane.setVisible(true);
    }

    private void btnlistproduct(ActionEvent event) {
        try {
            searchpane.setVisible(true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btnclear(ActionEvent event) {
        int selectclear = pricetable.getSelectionModel().getSelectedIndex();
        price_table_controller p = pricetable.getSelectionModel().getSelectedItem();
        subtotal.setText(p.getSubtotal());
        double SUM = Double.parseDouble(subtotal.getText());
        double SUM2 = Double.parseDouble(newtotal.getText());
        double Value = SUM2 - SUM;
        newtotal.setText("" + Value);
        pricetable.getItems().remove(selectclear);
        checkrowandsumotal();
        subtotal.setText(null);
        subtotal.setText("0.00");
    }

    public void genaretinvoiceid() {
        try {
            ResultSet rs = dbclass.search("select count(*)as Invoice_Id from invoice");
            if (rs.next()) {
                int Count = rs.getInt("Invoice_Id");
                String Id = "INV" + (++Count);
                invno.setText(Id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setcustomercombox() {
        ObservableList<String> List = FXCollections.observableArrayList();
        try {
            ResultSet rs = dbclass.search("select * from add_customer");
            while (rs.next()) {
                List.add(rs.getString("customerid") + "-" + rs.getString("name"));
            }
            cusidlist.setItems(List);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void visibleoption(MouseEvent event) {
        showpane.setVisible(false);
        paneloptin.setVisible(false);
    }

    @FXML
    private void actionproid(ActionEvent event) {
        exdate();
    }

    public void productset() {
        try {
            ResultSet rs = dbclass.search("select * from instock where barcode = '" + barcodid.getText() + "'");
            if (rs.next()) {
                boolean Status = rs.getBoolean("status");
                if (Status) {
                    if (retailprice.isSelected()) {
                        double Rprice = rs.getDouble("retailprice");
                        price.setText("" + Rprice);
                    } else if (wholesaleprice.isSelected()) {
                        double Rprice = rs.getDouble("wholesaleprice");
                        price.setText("" + Rprice);
                    } else if (extraprice.isSelected()) {
                        double Rprice = rs.getDouble("retailprice");
                        price.setText("" + Rprice);
                    }
                    proname.setText(rs.getString("productname"));
                    qtyAvB.setText(rs.getString("qty"));
                    proid.setText(rs.getString("productid"));
                    Mashiment = qtyAvB.getText().split("-")[1];

                }else{
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Eroor Massage");
                alert.setHeaderText("Product is inactive");
                alert.setContentText("Please check data. Identify the cause.");
                alert.showAndWait();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    String Lavel;

    @FXML
    private void actionqty(ActionEvent event) {
        if (!qty.getText().trim().equals("") && !qtyAvB.getText().trim().equals("")) {
            RecoadLavel();
            double Unitprice = Double.parseDouble(price.getText());
            double Qty = Double.parseDouble(qty.getText());
            double SumValue = Unitprice * Qty;
            totalprice.setText("" + SumValue);
            String AQtmash = qtyAvB.getText().split("-")[0];
            System.out.println(AQtmash);
            double Chek1value = Double.parseDouble(AQtmash);
            double Check2value = Double.parseDouble(qty.getText());
            if (Chek1value >= Check2value) {
                int x = 5000;
                if (Double.parseDouble(totalprice.getText()) >= x) {
                    discount1.setText(null);
                    discount1.setEditable(true);
                } else {

                    subtotal.setText(totalprice.getText());
                    if (qtyAvB.getText().split("-")[1].equals("(Kg)")) {
                        qty.setText(qty.getText() + "-(Kg)");
                    } else if (qtyAvB.getText().split("-")[1].equals("(g)")) {
                        qty.setText(qty.getText() + "-(g)");
                    } else if (qtyAvB.getText().split("-")[1].equals("(l)")) {
                        qty.setText(qty.getText() + "-(l)");
                    } else if (qtyAvB.getText().split("-")[1].equals("(ml)")) {
                        qty.setText(qty.getText() + "-(ml)");
                    } else if (qtyAvB.getText().split("-")[1].equals("(Pcs)")) {
                        qty.setText(qty.getText() + "-(Pcs)");
                    }
                    loadTable();
                    Total();
                    clear();
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Eroor Information");
                alert.setHeaderText("" + qty.getText() + " is not available " + qtyAvB.getText() + " is too much");
                alert.setContentText("Please enter the maximum quantity stored");
                alert.showAndWait();
                clear();
            }

        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Text Filed");
            alert.setHeaderText("Text Filed Is empty");
            alert.setContentText("Please Enter information correctly");
            alert.showAndWait();
        }
    }

    @FXML
    private void actiondiscount(ActionEvent event) {
        double Total = Double.parseDouble(totalprice.getText());
        int disc1 = Integer.parseInt(discount1.getText());

        double Discount1 = Total * (100 - disc1) / 100;
        DecimalFormat df = new DecimalFormat("#.##");
        String Format = df.format(Discount1);
        subtotal.setText("" + Format);
        loadTable();
        Total();
        clear();
        discount1.setEditable(false);
    }

    public void loadTable() {
        price_table_controller price_table = new price_table_controller(
                proid.getText(),
                barcodid.getText(),
                proname.getText(),
                price.getText(),
                qty.getText(),
                totalprice.getText(),
                discount1.getText(),
                subtotal.getText());
        ObservableList<tableview.price_table_controller> List = pricetable.getItems();
        List.add(price_table);
        pricetable.setItems(List);
    }

    private void clear() {
        barcodid.setText(null);
        proid.setText(null);
        proname.setText(null);
        price.setText("0.00");
        qty.setText(null);
        qtyAvB.setText(null);
        totalprice.setText("0.00");
        discount1.setText("0.00");
        subtotal.setText("0.00");
    }

    public void setSell() {
        c_proid.setCellValueFactory(new PropertyValueFactory<>("Productid"));
        c_barid.setCellValueFactory(new PropertyValueFactory<>("Barcodeid"));
        c_proname.setCellValueFactory(new PropertyValueFactory<>("Productname"));
        c_unitprice.setCellValueFactory(new PropertyValueFactory<>("Unitprice"));
        c_qty.setCellValueFactory(new PropertyValueFactory<>("productqty"));
        c_prototal.setCellValueFactory(new PropertyValueFactory<>("Producttotal"));
        c_discount1.setCellValueFactory(new PropertyValueFactory<>("Productdiscount"));
        c_subtotal.setCellValueFactory(new PropertyValueFactory<>("Subtotal"));

    }

    public void Total() {
        pricetable.getSelectionModel().selectFirst();
        double sum = 0;
        int items = pricetable.getItems().size();

        for (int i = 0; i < items; i++) {
            pricetable.getSelectionModel().select(i);
            String selectedItem = pricetable.getSelectionModel().getSelectedItem().getSubtotal();
            double newFloat = Double.parseDouble(selectedItem);
            sum = sum + newFloat;
        }
        String totalCost = String.valueOf(sum);
        newtotal.setText(totalCost);
        System.out.println("Total:" + sum);
        String totalItem = String.valueOf(items);
        itemcount.setText(totalItem);
    }

    public void checkrowandsumotal() {
        int size = pricetable.getItems().size();
        itemcount.setText("" + size);
        if (itemcount.getText().equals("")) {
            newtotal.setText("0.00");
        }
        discount2.setText("0.00");
        nettotal.setText("0.00");

    }

    @FXML
    private void btndiscount2(ActionEvent event) {
        if (!newtotal.getText().trim().equals("")) {
            if (cash.isSelected()) {
                discount2();
            } else if (card.isSelected()) {
                discount2();
            } else if (credit.isSelected()) {
                discount2();
                payment.setEditable(false);
            } else {
                Alert aleet = new Alert(Alert.AlertType.ERROR);
                aleet.setTitle("Payment Error");
                aleet.setHeaderText("Payment Section");
                aleet.setContentText("Select Payment Method");
                aleet.showAndWait();
            }
        } else {
            Alert aleet = new Alert(Alert.AlertType.ERROR);
            aleet.setTitle("Qty Error");
            aleet.setHeaderText("There are " + qtyAvB.getText() + " items in Stockthere");
            aleet.setContentText("There are " + qty.getText() + " no items in Stock");
            aleet.showAndWait();
        }

    }

    @FXML
    private void btnpayment(ActionEvent event) {
        if (!nettotal.getText().trim().equals("") && !newtotal.getText().trim().equals("")) {
            if (cash.isSelected()) {
                Payment();
            } else if (card.isSelected()) {
                Payment();
            } else if (credit.isSelected()) {
                try {
                    Price = "Credit Seller";
                    double aDouble3 = Double.parseDouble(nettotal.getText());
                    double sum = aDouble2 + aDouble3;
                    if (sum < aDouble1) {
                        Payment();
                        dbclass.push("update add_customer set Availableamount = Availableamount + '" + Double.parseDouble(nettotal.getText()) + "' where customerid = '" + cusid.getText() + "'");
                    } else {
                        Alert aleet = new Alert(Alert.AlertType.ERROR);
                        aleet.setTitle("Credit Customer");
                        aleet.setHeaderText("Balance " + aDouble1 + " Now Balance " + sum);
                        aleet.setContentText("The loan amount has been exceeded");
                        aleet.showAndWait();
                        Clear2();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void Payment() {
        double Payment = Double.parseDouble(payment.getText());
        double NetTotal = Double.parseDouble(nettotal.getText());
        if (Payment >= NetTotal) {
            double sum = Payment - NetTotal;
            DecimalFormat df = new DecimalFormat("#.##");
            String format = df.format(sum);
            balans.setText("" + format);
            DataSave();
        } else {
            Alert aleet = new Alert(Alert.AlertType.ERROR);
            aleet.setTitle("Discount Error");
            aleet.setHeaderText("");
            aleet.setContentText("");
            aleet.showAndWait();
        }
    }

    public void discount2() {
        double Total = Double.parseDouble(newtotal.getText());
        int disc1 = Integer.parseInt(discount2.getText());

        double Discount2 = Total * (100 - disc1) / 100;
        DecimalFormat df = new DecimalFormat("#.##");
        String format = df.format(Discount2);
        nettotal.setText("" + format);

    }
    String name;
    String Price;
    String SQtytype;
    String SQty;
    double QtySum;

    public void DataSave() {
        try {

            Date d = new Date();
            SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
            if (retailprice.isSelected()) {
                name = "Retail Price";
            } else if (wholesaleprice.isSelected()) {
                name = "Wholesale Price";
            } else if (extraprice.isSelected()) {
                name = "Extra Price";
            } else {
                Alert aleet = new Alert(Alert.AlertType.ERROR);
                aleet.setTitle("Price Error");
                aleet.setHeaderText("Price Section");
                aleet.setContentText("Select Price Method");
                aleet.showAndWait();
            }
            if (cash.isSelected()) {
                Price = "Cash";
            } else if (card.isSelected()) {
                Price = "Card";
            } else {

            }
            dbclass.push("insert into invoice values ('" + invno.getText() + "','" + cusid.getText()
                    + "','" + date.format(d) + "','" + newtotal.getText()
                    + "','" + discount2.getText() + "','" + nettotal.getText()
                    + "','" + payment.getText() + "','" + balans.getText() + "','" + name
                    + "','" + Price + "','" + systemcomfigdata.getEmpid() + "','1')");

            int Items = pricetable.getItems().size();
            for (int x = 0; x < Items; x++) {
                pricetable.getSelectionModel().select(x);
                String productId = pricetable.getSelectionModel().getSelectedItem().getProductid();
                String barcodeid = pricetable.getSelectionModel().getSelectedItem().getBarcodeid();
                String productname = pricetable.getSelectionModel().getSelectedItem().getProductname();
                String unitprice = pricetable.getSelectionModel().getSelectedItem().getUnitprice();
                String productqty = pricetable.getSelectionModel().getSelectedItem().getProductqty();
                String producttotal = pricetable.getSelectionModel().getSelectedItem().getProducttotal();
                String productdiscount = pricetable.getSelectionModel().getSelectedItem().getProductdiscount();
                String subtotal1 = pricetable.getSelectionModel().getSelectedItem().getSubtotal();

                dbclass.push("insert into invoiceitem (invoicid,productid,barcode,itemname,unitprice,qty,itemtotal,discount1,subtotal) values ('" + invno.getText() + "','" + productId
                        + "','" + barcodeid + "','" + productname + "','" + unitprice + "','" + productqty + "','" + producttotal + "','" + productdiscount + "','" + subtotal1 + "')");
                String productQty = pricetable.getSelectionModel().getSelectedItem().getProductqty().split("-")[0];
                QtySum = Double.parseDouble(productQty);
                ResultSet rs = dbclass.search("select qty from instock where productid = '" + productId + "'");
                if (rs.next()) {
                    SQty = rs.getString("qty").split("-")[0];
                    SQtytype = rs.getString("qty").split("-")[1];
                    chekqtytype();
                    dbclass.push("update instock set qty ='" + type + "' where productid = '" + productId + "'");
                    retailprice.setVisible(true);
                    extraprice.setVisible(true);
                    retailprice.setSelected(true);
                }
            }
            Alert aleet = new Alert(Alert.AlertType.CONFIRMATION);
            aleet.setTitle("Pyament Success");
            aleet.setHeaderText("Balance " + balans.getText() + " ");
            aleet.setContentText("Payment  Success Massage");
            aleet.showAndWait();
            jsreport(invno.getText());
            genaretinvoiceid();

            Clear2();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void Clear2() {
        barcodid.setText(null);
        proname.setText(null);
        price.setText("0.00");
        qty.setText(null);
        qtyAvB.setText(null);
        totalprice.setText("0.00");
        discount1.setText("0.00");
        subtotal.setText("0.00");
        newtotal.setText("0.00");
        discount2.setText("0.00");
        newtotal.setText("0.00");
        nettotal.setText("0.00");
        payment.setText("0.00");
        balans.setText("0.00");
        itemcount.setText("0");
        cregittotal.setText("0.00");
        pricetable.getItems().clear();
        cusid.setText("CUS1");
        cusname.setText("CASH CUSTOMER");
    }

    @FXML
    private void btnprintinvoice(ActionEvent event) {
        try {
            if (credit.isSelected()) {
                Price = "Credit Seller";
                double aDouble3 = Double.parseDouble(nettotal.getText());
                double sum = aDouble2 + aDouble3;
                if (sum < aDouble1) {
                    dbclass.push("update add_customer set Availableamount = Availableamount + '" + Double.parseDouble(nettotal.getText()) + "' where customerid = '" + cusid.getText() + "'");
                    DataSave();

                } else {
                    Alert aleet = new Alert(Alert.AlertType.ERROR);
                    aleet.setTitle("Credit Customer");
                    aleet.setHeaderText("Balance " + aDouble1 + " Now Balance " + sum);
                    aleet.setContentText("The loan amount has been exceeded");
                    aleet.showAndWait();
                    Clear2();

                }
            }
            if (card.isSelected()) {
                DataSave();
            }
            if (cash.isSelected()) {
                DataSave();
            }
        } catch (Exception e) {
        }

    }

    @FXML
    private void cliktextsizo(MouseEvent event) {
        discount2.setText(null);
    }

    @FXML
    private void cliktextsizo2(MouseEvent event) {
        if (credit.isSelected()) {
            payment.setEditable(false);
            payment.setText("0.00");
        } else {
            payment.setText(null);
        }
    }
    String type;

    public void chekqtytype() {
        double SQTY = Double.parseDouble(SQty);
        double SumA = SQTY - QtySum;
        if (SQtytype.trim().equals("(Kg)")) {
            type = "" + SumA + "-(Kg)";
        } else if (SQtytype.trim().equals("(g)")) {
            type = "" + SumA + "-(g)";
        } else if (SQtytype.trim().equals("(l)")) {
            type = "" + SumA + "-(l)";
        } else if (SQtytype.trim().equals("(ml)")) {
            type = "" + SumA + "-(ml)";
        } else if (SQtytype.trim().equals("(Pcs)")) {
            type = "" + SumA + "-(Pcs)";
        }
    }
    double aDouble1;
    double aDouble2;

    public void cerfitcustome() {
        try {
            ResultSet rs = dbclass.search("select * from add_customer where customerid = '" + cusid.getText() + "' ");
            if (rs.next()) {
                String name = rs.getString("type");
                if (name.equals("CREDIT CUSTOMER")) {
                    aDouble1 = rs.getDouble("Amountlimit");
                    aDouble2 = rs.getDouble("Availableamount");
                    if (aDouble1 < aDouble2) {
                        Alert aleet = new Alert(Alert.AlertType.ERROR);
                        aleet.setTitle("Credit Customer");
                        aleet.setHeaderText("The loan amount has been exceeded");
                        aleet.showAndWait();
                    } else {
                        credit.setSelected(true);
                        payment.setEditable(false);
                        double aDouble = rs.getDouble("Availableamount");
                        cregittotal.setText("" + aDouble);
                    }

                } else {
                    cregittotal.setText("0.00");
                    payment.setEditable(true);
                    cash.setSelected(true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private volatile boolean stop;

    private void timenow() {
        Thread thread = new Thread(() -> {
            SimpleDateFormat Time = new SimpleDateFormat("hh:mm:ss a");
            while (!stop) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
                final String timenow = Time.format(new Date());
                Platform.runLater(() -> {
                    todaytime.setText(timenow);
                });
            }
        });
        thread.start();
    }

    public void RecoadLavel() {
        try {
            ResultSet rs = dbclass.search("select * from instock where productid = '" + proid.getText() + "'");
            System.out.println(proid.getText());
            if (rs.next()) {
                Lavel = rs.getString("recodelevel").split("-")[0];
                System.out.println(Lavel);
                double Lavel2 = Double.parseDouble(Lavel);
                double Qty = Double.parseDouble(qtyAvB.getText().split("-")[0]);
                if (Lavel2 >= Qty) {
                    Alert aleet = new Alert(Alert.AlertType.CONFIRMATION);
                    aleet.setTitle("Qty Lavel");
                    aleet.setHeaderText("Qty Lavel is " + Lavel);
                    aleet.setContentText("Qty Lavel Is Low");
                    aleet.showAndWait();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exdate() {
        try {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
            cal.add(Calendar.DATE, 6);
            String current = date.format(cal.getTime());
            ResultSet rs = dbclass.search("select * from instock where barcode = '" + barcodid.getText() + "'");
            if (rs.next()) {
                String Dtime = rs.getString("exdate");
                Date carrentdate = date.parse(current);
                Date exdate = date.parse(Dtime);
                if (carrentdate.after(exdate)) {
                    Alert aleet = new Alert(Alert.AlertType.CONFIRMATION);
                    aleet.setTitle("Expired Date ");
                    aleet.setHeaderText("Date to be frustrted  " + Dtime);
                    aleet.setContentText("If the product is delivared to the consumer, clik the ok button");
                    aleet.showAndWait().ifPresent(consumer -> {
                        if (consumer == ButtonType.OK) {
                            productset();
                        } else {

                        }
                    });
                } else {
                    productset();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void anchorepane(MouseEvent event) {
        if (event.getClickCount() == 2) {
            if (itemcount.getText().trim().equals("") || itemcount.getText().trim().equals("0")) {
                retailprice.setVisible(true);
                extraprice.setVisible(true);
                cusidlist.setVisible(false);
                retailprice.setSelected(true);
            } else {
            }
        }
    }

    public void loadtable8() {
        try {
            ResultSet rs = dbclass.search("select * from instock where status = 1");
            while (rs.next()) {
                TableList.add(new tableview.productlist_table_controller(
                        rs.getLong("barcode"),
                        rs.getString("productname"),
                        rs.getDouble("retailprice")));
            }
            table8.setItems(TableList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setcol8() {
        col_barno.setCellValueFactory(new PropertyValueFactory<>("Barcodeid"));
        col_barname.setCellValueFactory(new PropertyValueFactory<>("productname"));
        col_price.setCellValueFactory(new PropertyValueFactory<>("Price"));

    }

    public void Itemsearch() {
        try {
            ResultSet rs = dbclass.search("select * from instock where barcode = '" + barcodid.getText() + "'");
            if (rs.next()) {
                proname.setText(rs.getString("productname"));
                if (retailprice.isSelected()) {
                    price.setText(rs.getString("retailprice"));
                } else if (wholesaleprice.isSelected()) {
                    price.setText(rs.getString("wholesaleprice"));
                } else if (extraprice.isSelected()) {
                    price.setText(rs.getString("exterpirce"));
                }
                qtyAvB.setText(rs.getString("qty"));
                proid.setText(rs.getString("productid"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void table8mousecilik(MouseEvent event) {
        if (event.getClickCount() == 2) {
            productlist_table_controller p = table8.getSelectionModel().getSelectedItem();
            barcodid.setText("" + p.getBarcodeid());
            exdate2();
        }
    }

    @FXML
    private void table8search(KeyEvent event) {
        try {
            FilteredList<tableview.productlist_table_controller> filterlist = new FilteredList<>(TableList, b -> true);
            searchproduct.textProperty().addListener((observable, oldValue, newValue) -> {
                filterlist.setPredicate(product -> {
                    if (newValue.isEmpty() || newValue.isEmpty() || newValue == null) {
                        return true;
                    }
                    String lowercasefilter = newValue.toLowerCase();

                    if (product.getProductname().toLowerCase().indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(product.getPrice()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else if (String.valueOf(product.getBarcodeid()).indexOf(lowercasefilter) > -1) {
                        return true;
                    } else {
                        return false;
                    }

                });
            });
            SortedList<tableview.productlist_table_controller> sortedlist = new SortedList<>(filterlist);
            sortedlist.comparatorProperty().bind(table8.comparatorProperty());
            table8.setItems(sortedlist);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void exdate2() {
        try {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
            cal.add(Calendar.DATE, 6);
            String current = date.format(cal.getTime());
            ResultSet rs = dbclass.search("select * from instock where barcode = '" + barcodid.getText() + "'");
            if (rs.next()) {
                String Dtime = rs.getString("exdate");
                Date carrentdate = date.parse(current);
                Date exdate = date.parse(Dtime);
                if (carrentdate.after(exdate)) {
                    Alert aleet = new Alert(Alert.AlertType.CONFIRMATION);
                    aleet.setTitle("Expired Date ");
                    aleet.setHeaderText("Date to be frustrted  " + Dtime);
                    aleet.setContentText("If the product is delivared to the consumer, clik the ok button");
                    aleet.showAndWait().ifPresent(consumer -> {
                        if (consumer == ButtonType.OK) {
                            Itemsearch();
                        } else {

                        }
                    });
                } else {
                    Itemsearch();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btnselles(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/invoice_option/selles.fxml"));
            Stage stage = new Stage();
            stage.initStyle(StageStyle.UTILITY);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btncollection(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/invoice_option/collection.fxml"));
            Stage stage = new Stage();
            stage.initStyle(StageStyle.UTILITY);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btndallycollction(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/invoice_option/dally_collection.fxml"));
            Stage stage = new Stage();
            stage.initStyle(StageStyle.UTILITY);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btnspent(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/invoice_option/institutional_expenses.fxml"));
            Stage stage = new Stage();
            stage.initStyle(StageStyle.UTILITY);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_return_product(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/invoice_option/return_product.fxml"));
            Stage stage = new Stage();
            stage.initStyle(StageStyle.UTILITY);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_expired_product(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/invoice_option/expired_product.fxml"));
            Stage stage = new Stage();
            stage.initStyle(StageStyle.UTILITY);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void jsreport(String Invid) {
        try {
            String path = "D:\\report\\groceryShop\\invoice.jasper";
            InputStream fileInputStream = new FileInputStream(path);
            Map<String, Object> perams = new HashMap<>();
            perams.put("invid", Invid);
            perams.put("date&time", todaytime.getText());
            JasperPrint fillReport = JasperFillManager.fillReport(fileInputStream, perams, dbclass.getnewconnection());
            JasperViewer.viewReport(fillReport, false);
//            JasperPrintManager.printReport(fileInputStream, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_credit_seller(ActionEvent event) {

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Cerdit Seller");
        alert.setHeaderText("Select credit custome");
        alert.setContentText("");
        alert.showAndWait().ifPresent(consumer -> {

            if (consumer == ButtonType.OK) {
                cusidlist.setVisible(true);
                payment.setEditable(true);
            } else {
                cash.setSelected(true);
                payment.setEditable(false);
            }

        });
    }

}
