/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main_unit;

import DB.systemcomfigdata;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;


/**
 * FXML Controller class
 *
 * @author isuru
 */
public class Main_interfaceController implements Initializable {

    @FXML
    private BorderPane main_unit_boder;
    @FXML
    private JFXButton store;
    @FXML
    private JFXButton stocktranfer;
    @FXML
    private JFXButton grn;
    @FXML
    private JFXButton chashier;
    @FXML
    private JFXButton tableview;
    @FXML
    private JFXButton report;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
          AnchorPane root = FXMLLoader.load(getClass().getResource("add_store.fxml"));
          main_unit_boder.setCenter(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
        chacksection();
    }    

    @FXML
    private void btn_store(ActionEvent event) {
         try {
          AnchorPane root = FXMLLoader.load(getClass().getResource("add_store.fxml"));
          main_unit_boder.setCenter(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_cashier(ActionEvent event) {
         try {
          AnchorPane root = FXMLLoader.load(getClass().getResource("invoice.fxml"));
          main_unit_boder.setCenter(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_grn(ActionEvent event) {
         try {
          AnchorPane root = FXMLLoader.load(getClass().getResource("grn_viwe.fxml"));
          main_unit_boder.setCenter(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_trnfer_stock(ActionEvent event) {
         try {
          AnchorPane root = FXMLLoader.load(getClass().getResource("tranfer_stock.fxml"));
          main_unit_boder.setCenter(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_table_view(ActionEvent event) {
        try {
          AnchorPane root = FXMLLoader.load(getClass().getResource("table_viwe.fxml"));
          main_unit_boder.setCenter(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void chacksection(){
        if(systemcomfigdata.getUsertype().trim().equals("Admin")){
            
        }else if(systemcomfigdata.getUsertype().equals("Employee")){
            store.setDisable(true);
            stocktranfer.setDisable(true);
            grn.setDisable(true);
            chashier.setDisable(true);
            tableview.setDisable(true);
            report.setDisable(true);
        }else if(systemcomfigdata.getUsertype().equals("Stock Manager")){
            store.setDisable(false);
            stocktranfer.setDisable(false);
            grn.setDisable(false);
            chashier.setDisable(true);
            tableview.setDisable(false);
            report.setDisable(false);
        }else if(systemcomfigdata.getUsertype().equals("Cashier")){
            store.setDisable(true);
            stocktranfer.setDisable(true);
            grn.setDisable(true);
            chashier.setDisable(false);
            tableview.setDisable(false);
            report.setDisable(false);
        }
    }
    
}
