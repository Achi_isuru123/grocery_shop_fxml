/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main_unit;

import DB.dbclass;
import DB.systemcomfigdata;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLIntegrityConstraintViolationException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author isuru
 */
public class Tranfer_stockController implements Initializable {

    @FXML
    private Pane btnpanal1;
    @FXML
    private JFXTextField barcodid;
    @FXML
    private JFXTextField qty;
    @FXML
    private JFXDatePicker mfdate;
    @FXML
    private JFXDatePicker exdate;
    @FXML
    private JFXTextField recodlevel;
    @FXML
    private JFXTextField wprice;
    @FXML
    private JFXTextField rprice;
    @FXML
    private JFXTextField eprice;
    @FXML
    private JFXTextField unitprice;
    @FXML
    private JFXTextField productname;
    @FXML
    private JFXListView<String> searchlist;
    @FXML
    private JFXTextField stockqty;
    @FXML
    private JFXTextField storeqty;
    @FXML
    private JFXTextField deletproductid;
    @FXML
    private JFXTextField deletproductname;
    @FXML
    private JFXTextField R_peice;
    @FXML
    private JFXTextField W_price;
    @FXML
    private JFXTextField E_price;
    @FXML
    private JFXTextField search;
    @FXML
    private Pane panel1;
    @FXML
    private JFXListView<String> delectlist;
    @FXML
    private JFXTextField searchcode;
    @FXML
    private Pane pnel2;
    @FXML
    private JFXListView<String> changerice;
    @FXML
    private JFXTextField serchprice;

    /**
     * Initializes the controller class.
     */
    Date d = new Date();
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
    String productid;
    String stockqtyunit;
    String SetUnit;
    double Storeqty;
    double Stockqty;
    String Bracode;
    @FXML
    private Pane qtypanel;
    @FXML
    private JFXTextField searchqty;
    @FXML
    private JFXListView<String> qtylist;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        searchlist.setVisible(false);
        panel1.setVisible(false);
        pnel2.setVisible(false);
        W_price.setEditable(false);
        R_peice.setEditable(false);
        E_price.setEditable(false);
        qtypanel.setVisible(false);
        LoadList();
    }

    @FXML
    private void barcodesearch(ActionEvent event) {
        try {

            ResultSet rs = dbclass.search("select * from store where barcode = '" + barcodid.getText() + "'");
            if (rs.next()) {
                productid = rs.getString("code");
                barcodid.setText(rs.getString("barcode"));
                productname.setText(rs.getString("name"));
                mfdate.setValue(rs.getDate("Date_of_manufacture").toLocalDate());
                exdate.setValue(rs.getDate("Expired_date").toLocalDate());
                unitprice.setText(rs.getString("unit_price"));
                rprice.setText(rs.getString("unit_price"));
                qty.setText(rs.getString("qty"));
                recodlevel.setText(rs.getString("recoadlevel"));
                storeqty.setText(rs.getString("qty"));
            }
            searchlist.setVisible(false);
            search.setText(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btnsave(ActionEvent event) {
        sumvalue();
        try {
            if (Stockqty <= Storeqty) {
                if (!productid.trim().equals("") && !stockqty.getText().trim().equals("") && !rprice.getText().trim().equals("") && !wprice.getText().trim().equals("") && !eprice.getText().trim().equals("")) {
                    dbclass.push("insert into instock (productid,barcode,productname,mfdate,exdate,qty,recodelevel,wholesaleprice,retailprice,exterpirce,transerdate,empid) values ('" + productid + "','" + Long.parseLong(barcodid.getText()) + "','" + productname.getText() + "','" + mfdate.getValue() + "','" + exdate.getValue() + "','" + SetUnit + "','" + recodlevel.getText() + "','" + wprice.getText() + "','" + rprice.getText() + "','" + eprice.getText() + "','" + date.format(d) + "','" + systemcomfigdata.getEmpid() + "')");
                    dbclass.push("update store set qty = '" + stockqtyunit + "' where code = '" + productid + "'");

                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Confirmation Messages");
                    alert.setHeaderText("Look, a Confirmation Messages");
                    alert.setContentText("Save Success..!");
                    alert.showAndWait();
                    Clear();
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Confirmation Messages");
                alert.setHeaderText("Look, a Confirmation Messages");
                alert.setContentText("There are not " + stockqty.getText() + " in the Warehouse");
                alert.showAndWait();
                stockqty.setText(null);
            }
        } catch (SQLIntegrityConstraintViolationException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Confirmation Messages");
            alert.setHeaderText("Look, a Confirmation Messages");
            alert.setContentText("Barcode id " + barcodid.getText() + " is Duplicate entry");
            alert.showAndWait();
            Clear();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btnupdate(ActionEvent event) {

    }

    @FXML
    private void btnclear(ActionEvent event) {
        Clear();
    }

    @FXML
    private void listclik(MouseEvent event) {
        try {
            String getdata = searchlist.getSelectionModel().getSelectedItem();
            String name = getdata.split("-")[0];

            ResultSet rs = dbclass.search("select * from store where barcode = '" + name + "'");
            if (rs.next()) {
                productid = rs.getString("code");
                barcodid.setText(rs.getString("barcode"));
                productname.setText(rs.getString("name"));
                mfdate.setValue(rs.getDate("Date_of_manufacture").toLocalDate());
                exdate.setValue(rs.getDate("Expired_date").toLocalDate());
                unitprice.setText(rs.getString("unit_price"));
                rprice.setText(rs.getString("unit_price"));
                qty.setText(rs.getString("qty"));
                recodlevel.setText(rs.getString("recoadlevel"));
                storeqty.setText(rs.getString("qty"));
            }
            searchlist.setVisible(false);
            search.setText(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void deleteproduct(ActionEvent event) {
        try {
            dbclass.push("update instock set status= '0' where barcode = '" + deletproductid.getText() + "'");
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Confirmation Messages");
            alert.setHeaderText("Look, a Confirmation Messages");
            alert.setContentText("Barcode id " + deletproductid.getText() + " is Delete Success");
            alert.showAndWait();
            Clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void opanpenal1(MouseEvent event) {
        try {
            ObservableList<String> list = FXCollections.observableArrayList();
            ResultSet rs = dbclass.search("select * from instock");
            while (rs.next()) {
                list.add(rs.getString("barcode") + "-" + rs.getString("productname"));
            }
            delectlist.setItems(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        panel1.setVisible(true);
    }

    @FXML
    private void btnpice_update(ActionEvent event) {
        try {
            dbclass.push("update instock set wholesaleprice = '" + W_price.getText() + "', retailprice = '" + R_peice.getText() + "', exterpirce = '" + E_price.getText() + "' where barcode = '" + Bracode + "'");
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Confirmation Messages");
            alert.setHeaderText("Look, a Confirmation Messages");
            alert.setContentText("Barcode id " + Bracode + " is Update Success");
            alert.showAndWait();
            Clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void openpenal2(MouseEvent event) {
        try {
            ObservableList<String> list = FXCollections.observableArrayList();
            ResultSet rs = dbclass.search("select * from instock");
            while (rs.next()) {
                list.add(rs.getString("productid") + "-" + rs.getString("productname") + "-" + rs.getString("wholesaleprice"));
            }
            changerice.setItems(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        pnel2.setVisible(true);
    }

    @FXML
    private void searchkey(KeyEvent event) {
        searchlist.setVisible(true);
        try {
            ObservableList<String> list = FXCollections.observableArrayList();
            ResultSet rs = dbclass.search("SELECT * FROM store WHERE name LIKE '" + search.getText() + "%' and status = '1'");
            searchlist.getItems().clear();
            while (rs.next()) {
                list.add(rs.getString("barcode") + "-" + rs.getString("name"));
            }
            searchlist.setItems(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void fillbrcode(MouseEvent event) {
        try {
            String Price = delectlist.getSelectionModel().getSelectedItem();
            searchcode.setText(Price);
            ResultSet rs = dbclass.search("select * from instock where productid = '" + searchcode.getText().split("-")[0] + "'");
            if (rs.next()) {
                deletproductid.setText(rs.getString("barcode"));
                deletproductname.setText(rs.getString("productname"));

            }
            panel1.setVisible(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void deleteproduct(KeyEvent event) {
        try {
            ObservableList<String> list = FXCollections.observableArrayList();
            ResultSet rs = dbclass.search("SELECT * FROM instock WHERE productname LIKE '" + searchcode.getText() + "%' and status = '1'");
            delectlist.getItems().clear();
            while (rs.next()) {
                list.add(rs.getString("barcode") + "-" + rs.getString("productname"));
            }
            delectlist.setItems(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void fillprice(MouseEvent event) {
        try {
            String Price = changerice.getSelectionModel().getSelectedItem();
            serchprice.setText(Price);
            ResultSet rs = dbclass.search("select * from instock where productid = '" + serchprice.getText().split("-")[0] + "'");
            if (rs.next()) {
                Bracode = rs.getString("barcode");
                W_price.setText(rs.getString("wholesaleprice"));
                R_peice.setText(rs.getString("retailprice"));
                E_price.setText(rs.getString("exterpirce"));
                W_price.setEditable(true);
                R_peice.setEditable(true);
                E_price.setEditable(true);
            }
            pnel2.setVisible(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void chengeprice(KeyEvent event) {
        try {
            ObservableList<String> list = FXCollections.observableArrayList();
            ResultSet rs = dbclass.search("SELECT * FROM instock WHERE wholesaleprice LIKE '" + serchprice.getText() + "%' and status = '1'");
            changerice.getItems().clear();
            while (rs.next()) {
                list.add(rs.getString("barcode") + "-" + rs.getString("productname") + "-" + rs.getString("wholesaleprice"));
            }
            changerice.setItems(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sumvalue() {
        String Storeunit = storeqty.getText().split("-")[1];
        String StockQty = stockqty.getText();
        Storeqty = Double.parseDouble(storeqty.getText().split("-")[0]);
        Stockqty = Double.parseDouble(stockqty.getText());
        double Sum = Storeqty - Stockqty;
        if (Storeunit.equals("(Kg)")) {
            stockqtyunit = "" + Sum + " -(Kg)";
            SetUnit = StockQty + " -(Kg)";
        } else if (Storeunit.equals("(g)")) {
            stockqtyunit = "" + Sum + " -(g)";
            SetUnit = StockQty + " -(g)";
        } else if (Storeunit.equals("(l)")) {
            stockqtyunit = "" + Sum + " -(l)";
            SetUnit = StockQty + " -(l)";
        } else if (Storeunit.equals("(ml)")) {
            stockqtyunit = "" + Sum + " -(ml)";
            SetUnit = StockQty + " -(ml)";
        } else if (Storeunit.equals("(Pcs)")) {
            stockqtyunit = "" + Sum + " -(Pcs)";
            SetUnit = StockQty + " -(Pcs)";
        }

    }

    public void LoadList() {
        try {
            ObservableList<String> list = FXCollections.observableArrayList();
            ResultSet rs = dbclass.search("select * from store");
            while (rs.next()) {
                list.add(rs.getString("barcode") + "-" + rs.getString("name"));
            }
            searchlist.setItems(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Clear() {
        barcodid.setText(null);
        productname.setText(null);
        mfdate.setValue(null);
        exdate.setValue(null);
        unitprice.setText(null);
        storeqty.setText(null);
        stockqty.setText(null);
        wprice.setText(null);
        rprice.setText(null);
        eprice.setText(null);
        qty.setText(null);
        recodlevel.setText(null);
        deletproductid.setText(null);
        deletproductname.setText(null);
        W_price.setText(null);
        R_peice.setText(null);
        E_price.setText(null);
        W_price.setEditable(false);
        R_peice.setEditable(false);
        E_price.setEditable(false);
    }

    @FXML
    private void qty_fill(MouseEvent event) {
        try {
            String Qty = qtylist.getSelectionModel().getSelectedItem();
            searchqty.setText(Qty.split("-")[0]);
            Qqty_store(searchqty.getText());
            ResultSet rs = dbclass.search("select * from instock where productid = '" + searchqty.getText() + "'");
            if (rs.next()) {
                Stock_qty = rs.getString("Qty").split("-")[0];
                Stock_unit = rs.getString("Qty").split("-")[1];
                stockqty.setText(rs.getString("Qty").split("-")[0]);
                barcodid.setText(rs.getString("productid"));
                productname.setText(rs.getString("productname"));
                mfdate.setValue(rs.getDate("mfdate").toLocalDate());
                exdate.setValue(rs.getDate("exdate").toLocalDate());
            }
            qtypanel.setVisible(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void search_product(KeyEvent event) {
        try {
            ObservableList<String> list = FXCollections.observableArrayList();
            ResultSet rs = dbclass.search("SELECT * FROM instock WHERE productname LIKE '" + searchqty.getText() + "%' and status = '1'");
            qtylist.getItems().clear();
            while (rs.next()) {
                list.add(rs.getString("productid") + "-" + rs.getString("productname") + "-" + rs.getString("qty"));
            }
            qtylist.setItems(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void LoadList_qty() {
        try {
            ObservableList<String> list = FXCollections.observableArrayList();
            ResultSet rs = dbclass.search("select * from instock");
            while (rs.next()) {
                list.add(rs.getString("productid") + "-" + rs.getString("productname"));
            }
            qtylist.setItems(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void clik_qty(MouseEvent event) {
        qtypanel.setVisible(true);
        LoadList_qty();
    }

    private void Qqty_store(String text) {
        try {
            ResultSet rs = dbclass.search("select * from store where code = '" + text + "'");
            if (rs.next()) {
                storeqty.setText(rs.getString("Qty"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_update_qty(ActionEvent event) {
        try {
             double parseDouble = Double.parseDouble(storeqty.getText().split("-")[0]); 
             double parseDouble1 = Double.parseDouble(stockqty.getText());
            sumvalue();
            Stock_Qty_sum();
           
            if (parseDouble >= parseDouble1) {
                dbclass.push("update instock set qty = '" + SetUnit + "' where productid = '" + barcodid.getText() + "'");
                dbclass.push("update store set qty = '" + stockqtyunit + "' where code = '" + barcodid.getText() + "'");
                Clear();
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Information Messages");
                alert.setHeaderText("Product qty update success");
                alert.setContentText("Trander Qty " + stockqty.getText() + " is Update Success");
                alert.showAndWait();
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Eroor Messages");
                alert.setHeaderText("Insufficient stock in the store is insufficient");
                alert.setContentText("Store Qty " + storeqty.getText() + "");
                alert.showAndWait();
                stockqty.setText(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    String Stock_qty;
    String Stock_unit;

    public void Stock_Qty_sum() {
        double NewQty = Double.parseDouble(stockqty.getText());
        double OldQty = Double.parseDouble(Stock_qty);
        double StockQty = OldQty + NewQty;
        if (Stock_unit.equals("(Kg)")) {
            SetUnit = StockQty + " -(Kg)";
        } else if (Stock_unit.equals("(g)")) {
            SetUnit = StockQty + " -(g)";
        } else if (Stock_unit.equals("(l)")) {
            SetUnit = StockQty + " -(l)";
        } else if (Stock_unit.equals("(ml)")) {
            SetUnit = StockQty + " -(ml)";
        } else if (Stock_unit.equals("(Pcs)")) {
            SetUnit = StockQty + " -(Pcs)";
        }
    }
}
