/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main_unit;

import DB.dbclass;
import DB.systemcomfigdata;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.io.File;
import java.net.URL;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author isuru
 */
public class Add_storeController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private JFXTextField today;
    @FXML
    private Label todaytime;
    @FXML
    private JFXTextField emptype;
    @FXML
    private JFXTextField empname;

    private volatile boolean stop;
    Date d = new Date();
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
    @FXML
    private JFXTextField tranferid;
    @FXML
    private JFXTextField wenderdoc;
    @FXML
    private JFXTextField refno;
    @FXML
    private JFXTextField productname;
    @FXML
    private JFXTextField productcode;
    @FXML
    private JFXTextField standcost;
    @FXML
    private JFXTextField unitprice;
    @FXML
    private JFXTextField uintinstore;
    @FXML
    private JFXTextField recodelevel;
    @FXML
    private JFXDatePicker dom;
    @FXML
    private JFXDatePicker exdate;
    @FXML
    private JFXTextField qty;
    @FXML
    private JFXTextField catogory;
    @FXML
    private JFXTextField subcategory;
    @FXML
    private JFXTextField total;
    @FXML
    private JFXTextArea description;
    @FXML
    private Circle image;
    @FXML
    private TextField barcodeno;
    @FXML
    private ListView<String> categorylist;
    @FXML
    private ListView<String> subcategorylist;
    @FXML
    private JFXTextField rackno;
    @FXML
    private JFXListView<String> racklist;
    @FXML
    private JFXTextField purchaseprice;
    @FXML
    private JFXTextField search;
    @FXML
    private JFXListView<String> searchlist;
    @FXML
    private JFXListView<String> listproductadd;
    @FXML
    private CheckBox grnitemschack;
    @FXML
    private JFXTextField suppler;
    @FXML
    private ListView<String> measu_list;
    @FXML
    private ListView<String> sup_list;
    @FXML
    private JFXTextField measument;
    @FXML
    private AnchorPane anchorepane;

    String replacepath = "/image/item_image.jpg";

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        searchlist.setVisible(false);
        listproductadd.setVisible(false);
        measu_list.setVisible(false);
        sup_list.setVisible(false);
        categorylist.setVisible(false);
        subcategorylist.setVisible(false);
        racklist.setVisible(false);
        Product_id();
        Transection_id();
        empname.setText(systemcomfigdata.getFullname());
        emptype.setText(systemcomfigdata.getUsertype());
        Today();
        Clear();
    }

    @FXML
    private void btn_product_save(ActionEvent event) {
        saveproduct();
    }

    @FXML
    private void btn_update_product(ActionEvent event) {
        Update_product();
    }

    @FXML
    private void productnamekeyreleas(KeyEvent event) {
    }

    @FXML
    private void clikziro1(MouseEvent event) {
    }

    @FXML
    private void validat(ActionEvent event) {
    }

    @FXML
    private void clikziro5(MouseEvent event) {
    }

    @FXML
    private void clikziro3(MouseEvent event) {
    }

    @FXML
    private void clikziro4(MouseEvent event) {
    }

    @FXML
    private void keyreleasrecoder(KeyEvent event) {
    }

    @FXML
    private void qtyclikmouse(MouseEvent event) {
    }

    @FXML
    private void categorykeypress(KeyEvent event) {
    }

    @FXML
    private void subcatmouseclik(MouseEvent event) {
    }

    @FXML
    private void clikrackno(MouseEvent event) {
    }

    @FXML
    private void btnsearch(ActionEvent event) {
    }

    @FXML
    private void barcodenoganaret(ActionEvent event) {
    }

    @FXML
    private void btn_clear_textfelid(ActionEvent event) {
        Clear();
    }

    @FXML
    private void btnganaretbarcode(ActionEvent event) {
    }

    @FXML
    private void clikziro2(MouseEvent event) {
    }

    @FXML
    private void purchasekeypresssum(KeyEvent event) {
    }

    @FXML
    private void listproductaddmouseclik(MouseEvent event) {
    }

    @FXML
    private void actionproduct(ActionEvent event) {
    }

    @FXML
    private void action_product(ActionEvent event) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Search Item");
        alert.setHeaderText("If you wont store items, clik OK. If you want GRN items, click Cancel");
        alert.setContentText("");
        alert.showAndWait().ifPresent(consumer -> {
            if (consumer == ButtonType.OK) {
                store_product_search();
            } else {
                grn_product_search();
            }
        });
    }

    @FXML
    private void key_preased_search(KeyEvent event) {
        try {
            if (grnitemschack.isSelected()) {
                ObservableList<String> Search_List = FXCollections.observableArrayList();
                ResultSet rs = dbclass.search("select * from store where name LIKE '" + search.getText() + "%'");
                while (rs.next()) {
                    Search_List.add(rs.getString("code") + "-" + rs.getString("name"));
                }
                searchlist.setItems(Search_List);
            } else {
                ObservableList<String> Search_List = FXCollections.observableArrayList();
                ResultSet rs = dbclass.search("select * from grn where productname LIKE '" + search.getText() + "%'");
                while (rs.next()) {
                    Search_List.add(rs.getString("productid") + "-" + rs.getString("productname"));
                }
                searchlist.setItems(Search_List);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_grn_item(ActionEvent event) {
        if (grnitemschack.isSelected()) {
            searchlist.setVisible(true);
            Search_list();
        } else {
            searchlist.setVisible(true);
            Grn_list();
        }
    }

    @FXML
    private void Total_value(KeyEvent event) {
        double qtyvalue = Double.parseDouble(qty.getText());
        double purch = Double.parseDouble(purchaseprice.getText());
        double sum = qtyvalue * purch;
        DecimalFormat df = new DecimalFormat("#.##");
        String fromat = df.format(sum);
        total.setText(fromat);

    }

    @FXML
    private void qty_action(ActionEvent event) {
        double qtyvalue = Double.parseDouble(qty.getText());
        double purch = Double.parseDouble(purchaseprice.getText());
        double sum = qtyvalue * purch;
        DecimalFormat df = new DecimalFormat("#.##");
        String fromat = df.format(sum);
        total.setText(fromat);
        qty.setEditable(false);
    }

    @FXML
    private void search_mousec_lick(MouseEvent event) {
        ObservableList<String> Search_List = searchlist.getSelectionModel().getSelectedItems();
        search.setText((String) Search_List.get(0).split("-")[0]);
        searchlist.setVisible(false);
    }

    @FXML
    private void btn_save_image(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        Stage stage = (Stage) anchorepane.getScene().getWindow();
        File file = fileChooser.showOpenDialog(stage);
        String imagepath = file.getAbsolutePath();
        replacepath = imagepath.replace("\\", "/");
        System.out.println(imagepath);
        if (file != null) {
            Image img = new Image(file.toURI().toString(), 388, 991, false, true);
            image.setFill(new ImagePattern(img));
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Not Selected Image");
            alert.setHeaderText("Image Not Working");
            alert.showAndWait();
        }
    }

    @FXML
    private void Rack_item_set(MouseEvent event) {
        ObservableList<String> Rack_No = racklist.getSelectionModel().getSelectedItems();
        rackno.setText((String) Rack_No.get(0));
        racklist.setVisible(false);

    }

    @FXML
    private void Sub_Cat_item_set(MouseEvent event) {
        ObservableList<String> Sub_Cat = subcategorylist.getSelectionModel().getSelectedItems();
        subcategory.setText((String) Sub_Cat.get(0));
        subcategorylist.setVisible(false);
        racklist.setVisible(true);
        Rack_list();
    }

    @FXML
    private void Cat_clik(MouseEvent event) {
        categorylist.setVisible(true);
        Category_list();
    }

    @FXML
    private void Cat_item_set(MouseEvent event) {
        ObservableList<String> Cat = categorylist.getSelectionModel().getSelectedItems();
        catogory.setText((String) Cat.get(0));
        categorylist.setVisible(false);
        subcategorylist.setVisible(true);
        Sub_Category_list();
    }

    @FXML
    private void Sup_clik(MouseEvent event) {
        sup_list.setVisible(true);
        Supplier_list();
    }

    @FXML
    private void Sup_item_set(MouseEvent event) {
        ObservableList<String> Sup = sup_list.getSelectionModel().getSelectedItems();
        suppler.setText((String) Sup.get(0));
        sup_list.setVisible(false);
    }

    @FXML
    private void Measu_clik(MouseEvent event) {
        measu_list.setVisible(true);
        measu_list();
    }

    @FXML
    private void Measu_item_set(MouseEvent event) {
        ObservableList<String> Measument = measu_list.getSelectionModel().getSelectedItems();
        measument.setText((String) Measument.get(0));
        measu_list.setVisible(false);
    }

    @FXML
    private void store_pane(MouseEvent event) {
        searchlist.setVisible(false);
        listproductadd.setVisible(false);
        measu_list.setVisible(false);
        sup_list.setVisible(false);
        categorylist.setVisible(false);
        subcategorylist.setVisible(false);
        racklist.setVisible(false);
    }

    //--------------------------------------------------------Create Method------------------------------
    public void Search_list() {
        try {
            ObservableList<String> Search_List = FXCollections.observableArrayList();
            ResultSet rs = dbclass.search("select * from store where status = 1");
            while (rs.next()) {
                Search_List.add(rs.getString("code") + "-" + rs.getString("name"));
            }
            searchlist.setItems(Search_List);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Grn_list() {
        try {
            ObservableList<String> Grn_List = FXCollections.observableArrayList();
            ResultSet rs = dbclass.search("select * from grn");
            while (rs.next()) {
                String approvad = rs.getString("approvaed");
                if (approvad.trim().equals("Appovaed")) {
                    Grn_List.add(rs.getString("productid") + "-" + rs.getString("productname"));
                }
            }
            searchlist.setItems(Grn_List);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Product_id() {
        try {
            ResultSet rs = dbclass.search("select count(*) as product_id from store");
            if (rs.next()) {
                int Count = rs.getInt("product_id");
                productcode.setText("" + ++Count);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Transection_id() {
        try {
            ResultSet rs = dbclass.search("select count(*) as product_id from instock");
            if (rs.next()) {
                int Count = rs.getInt("product_id");
                tranferid.setText("" + Count);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void produc_search_list() {

    }

    public void measu_list() {
        try {
            ObservableList<String> Measumant_List = FXCollections.observableArrayList("(Kg)", "(g)", "(l)", "(ml)", "(Pcs)");
            measu_list.setItems(Measumant_List);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Supplier_list() {
        try {
            ObservableList<String> Supplier_List = FXCollections.observableArrayList();
            ResultSet rs = dbclass.search("select * from add_supplier where status = 1");
            while (rs.next()) {
                Supplier_List.add(rs.getString("supplierid") + "-" + rs.getString("companyname"));
            }
            sup_list.setItems(Supplier_List);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Category_list() {
        try {
            ObservableList<String> Category_List = FXCollections.observableArrayList();
            ResultSet rs = dbclass.search("select * from category where status = 1");
            while (rs.next()) {
                Category_List.add(rs.getString("category_id") + "-" + rs.getString("categoryname"));
            }
            categorylist.setItems(Category_List);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Sub_Category_list() {
        try {
            ObservableList<String> Sub_Category_List = FXCollections.observableArrayList();
            ResultSet rs = dbclass.search("select * from add_subcategory  where  categoryid = '" + catogory.getText().split("-")[0] + "'");
            while (rs.next()) {
                Sub_Category_List.add(rs.getString("subcategoryname"));
            }
            subcategorylist.setItems(Sub_Category_List);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Rack_list() {
        try {
            ObservableList<String> Rack_No = FXCollections.observableArrayList();
            ResultSet rs = dbclass.search("select * from category where  category_id = '" + catogory.getText().split("-")[0] + "'");
            while (rs.next()) {
                Rack_No.add(rs.getString("racknumber"));
            }
            racklist.setItems(Rack_No);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Unit_Chack() {
        if (measument.getText().trim().equals("(Kg)")) {
            type1 = "" + productname.getText() + "-(Kg)";
            type2 = "" + recodelevel.getText() + "-(Kg)";
            type3 = "" + qty.getText() + "-(Kg)";
        } else if (measument.getText().trim().equals("(g)")) {
            type1 = "" + productname.getText() + "-(g)";
            type2 = "" + recodelevel.getText() + "-(g)";
            type3 = "" + qty.getText() + "-(g)";
        } else if (measument.getText().trim().equals("(l)")) {
            type1 = "" + productname.getText() + "-(l)";
            type2 = "" + recodelevel.getText() + "-(l)";
            type3 = "" + qty.getText() + "-(l)";
        } else if (measument.getText().trim().equals("(ml)")) {
            type1 = "" + productname.getText() + "-(ml)";
            type2 = "" + recodelevel.getText() + "-(ml)";
            type3 = "" + qty.getText() + "-(ml)";
        } else if (measument.getText().trim().equals("(Pcs)")) {
            type1 = "" + productname.getText() + "-(Pcs)";
            type2 = "" + recodelevel.getText() + "-(Pcs)";
            type3 = "" + qty.getText() + "-(Pcs)";
        }
    }

    public void convatgram() {
        if (qty.getText().split("-")[1].equals("Kg")) {
            double Qty = Double.parseDouble(qty.getText().split("-")[0]);
            double sum = Qty * 1000;
            DecimalFormat df = new DecimalFormat("#.##");
            String fromat = df.format(sum);
            qty.setText(fromat + "-(g)");
        } else if (qty.getText().split("-")[1].equals("l")) {
            double Qty = Double.parseDouble(qty.getText().split("-")[0]);
            double sum = Qty * 1000;
            DecimalFormat df = new DecimalFormat("#.##");
            String fromat = df.format(sum);
            qty.setText(fromat + "-(ml)");
        }
    }
    String type1;
    String type2;
    String type3;

    public void saveproduct() {
        try {
            if (!barcodeno.getText().equals("") && !productname.getText().equals("") && !description.getText().equals("") && !measument.equals("") && !standcost.getText().equals("") && !purchaseprice.getText().equals("") && !unitprice.getText().equals("") && !recodelevel.getText().equals("") && !suppler.equals("") && !qty.getText().equals("") && !catogory.getText().equals("") && !subcategory.getText().equals("") && !dom.getEditor().getText().equals("") && !exdate.getEditor().getText().equals("") && !total.getText().equals("")) {
                Unit_Chack();
                System.out.println(type1);
                dbclass.push("insert into store (barcode,name,description,Measurements,purchase_price,standard_cost,unit_in_stock,recoadlevel,supplerid,category_id,store_subcategoryname,store_rackno,Date_of_manufacture,Expired_date,unit_price,qty,total,imagepath,enterdate,enterupdate,status,empid) values ('"
                        + barcodeno.getText() + "','" + type1
                        + "','" + description.getText()
                        + "','" + measument.getText() + "','" + standcost.getText() + "','" + purchaseprice.getText()
                        + "','" + uintinstore.getText() + "','" + type2 + "','" + suppler.getText().split("-")[0]
                        + "','" + catogory.getText().split("-")[0] + "','" + subcategory.getText()
                        + "','" + rackno.getText() + "','" + dom.getValue() + "','" + exdate.getValue()
                        + "','" + unitprice.getText() + "','" + type3 + "','" + total.getText()
                        + "','" + replacepath + "','" + date.format(d)
                        + "',(NULL),'1','" + systemcomfigdata.getEmpid() + "')");
             Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Item save information massage");
            alert.setHeaderText("item save success");
            alert.setContentText("");
            alert.showAndWait();
            Clear();
            }else{
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Item save Error massage");
            alert.setHeaderText("Text Field Is emty");
            alert.setContentText("");
            alert.showAndWait();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Update_product() {
        try {
            if (!barcodeno.getText().equals("") && !productname.getText().equals("") && !description.getText().equals("") && !measument.equals("") && !standcost.getText().equals("") && !purchaseprice.getText().equals("") && !unitprice.getText().equals("") && !recodelevel.getText().equals("") && !suppler.equals("") && !qty.getText().equals("") && !catogory.getText().equals("") && !subcategory.getText().equals("") && !dom.getEditor().getText().equals("") && !exdate.getEditor().getText().equals("") && !total.getText().equals("")) {
                Unit_Chack();
                System.out.println(type1);
                dbclass.push("update store set barcode = '" + barcodeno.getText()
                        + "',name = '" + type1 + "',description = '" + description.getText()
                        + "',Measurements = '" + measument.getText() + "',purchase_price = '" + purchaseprice.getText()
                        + "',standard_cost = '" + standcost.getText() + "',unit_in_stock = '" + uintinstore.getText()
                        + "',recoadlevel = '" + type2 + "',supplerid = '" + suppler.getText().split("-")[0]
                        + "',category_id = '" + catogory.getText().split("-")[0]
                        + "',store_subcategoryname = '" + subcategory.getText()
                        + "',store_rackno = '" + rackno.getText()
                        + "',Date_of_manufacture = '" + dom.getValue()
                        + "',Expired_date = '" + exdate.getValue()
                        + "',unit_price = '" + unitprice.getText()
                        + "',qty = '" + type3 + "', total = '" + total.getText()
                        + "',imagepath = '" + replacepath + "',enterupdate = '" + date.format(d)
                        + "',status = '1',empid = '" + systemcomfigdata.getEmpid() + "' where code = '" + productcode.getText() + "'");
           
            }
           
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void store_product_search() {
        try {
            ResultSet rs = dbclass.search("select * from store where code = '" + search.getText() + "'");
            Clear();
            if (rs.next()) {
                productcode.setText(rs.getString("code"));
                barcodeno.setText(rs.getString("barcode"));
                productname.setText(rs.getString("name").split("-")[0]);
                description.setText(rs.getString("description"));
                measument.setText(rs.getString("Measurements"));
                standcost.setText(rs.getString("standard_cost"));
                purchaseprice.setText(rs.getString("purchase_price"));
                uintinstore.setText(rs.getString("unit_in_stock"));
                recodelevel.setText(rs.getString("recoadlevel").split("-")[0]);
                String Supid = rs.getString("supplerid");
                Search_supplier(Supid);
                catogory.setText(rs.getString("category_id"));
                subcategory.setText(rs.getString("store_subcategoryname"));
                rackno.setText(rs.getString("store_rackno"));
                dom.setValue(rs.getDate("Date_of_manufacture").toLocalDate());
                exdate.setValue(rs.getDate("Expired_date").toLocalDate());
                unitprice.setText(rs.getString("unit_price"));
                qty.setText(rs.getString("qty").split("-")[0]);
                total.setText(rs.getString("total"));
                replacepath = rs.getString("imagepath");
                Image img = new Image(replacepath, 388, 991, false, true);
                image.setFill(new ImagePattern(img));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void grn_product_search() {
        try {
            ResultSet rs = dbclass.search("select * from grn where productid = '" + search.getText() + "'");
            if (rs.next()) {
                String Approvaed = rs.getString("approvaed");
                if (Approvaed.trim().equals("Appovaed")) {
                    String string = rs.getString("productid");
                    OldStoreqty(string);
                    
                    productname.setText(rs.getString("productname").split("-")[0]);
                    description.setText("New Product");
                    purchaseprice.setText(rs.getString("price"));
                    suppler.setText(rs.getString("supplerid") + "-" + rs.getString("supplername"));
                    qty.setText(rs.getString("qty").split("-")[0]);
                    total.setText(rs.getString("total"));
                    Image img = new Image(replacepath, 388, 991, false, true);
                    image.setFill(new ImagePattern(img));
                }
            } else {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("GRN Itemerror massage");
                alert.setHeaderText("GRN item not approvaed");
                alert.setContentText("");
                alert.showAndWait();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void Clear() {
        barcodeno.setText(null);
        productname.setText(null);
        description.setText("NEW PRODUCT");
        purchaseprice.setText(null);
        standcost.setText(null);
        uintinstore.setText(null);
        recodelevel.setText(null);
        suppler.setText(null);
        catogory.setText(null);
        subcategory.setText(null);
        rackno.setText(null);
        dom.setValue(null);
        exdate.setValue(null);
        unitprice.setText(null);
        qty.setText(null);
        total.setText(null);
        image.setFill(null);
        search.setText(null);
        Product_id();
        Transection_id();
        Image img = new Image("/image/item_image.jpg", 388, 991, false, true);
        image.setFill(new ImagePattern(img));

        qty.setEditable(true);

    }

    private void Search_supplier(String id) {
        try {
            ResultSet rs = dbclass.search("select * from add_supplier where supplierid = '" + id + "'");
            if (rs.next()) {
                suppler.setText(rs.getString("supplierid") + "-" + rs.getString("companyname"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void Today() {
        Date d = new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MMM-dd");
        today.setText(date.format(d));
    }

    private void OldStoreqty(String text) {
        try {
            ResultSet rs = dbclass.search("select * from store where code = '" + text + "'");
            if (rs.next()) {
                uintinstore.setText(rs.getString("qty"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
