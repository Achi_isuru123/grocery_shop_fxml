/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

/**
 * FXML Controller class
 *
 * @author isuru
 */
public class Common_sectionController implements Initializable {

   
    @FXML
    private BorderPane section_boder;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    @FXML
    private void btn_customer_add(ActionEvent event) {
        try {
            AnchorPane root = FXMLLoader.load(getClass().getResource("/add_secton/add_customer.fxml"));
            section_boder.setCenter(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_suppler_add(ActionEvent event) {
        try {
            AnchorPane root = FXMLLoader.load(getClass().getResource("/add_secton/add_supplier.fxml"));
            section_boder.setCenter(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_category_add(ActionEvent event) {
        try {
            AnchorPane root = FXMLLoader.load(getClass().getResource("/add_secton/caegory.fxml"));
            section_boder.setCenter(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_section_add(ActionEvent event) {
        try {
            AnchorPane root = FXMLLoader.load(getClass().getResource("/add_secton/section.fxml"));
            section_boder.setCenter(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btn_employee_add(ActionEvent event) {
        try {
            AnchorPane root = FXMLLoader.load(getClass().getResource("/add_secton/register.fxml"));
            section_boder.setCenter(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
       @FXML
     private void btn_report_add(ActionEvent event) {
//       try {
//          AnchorPane root = FXMLLoader.load(getClass().getResource("/dashbord/section.fxml"));
//          section_boder.setCenter(root);
//        } catch (Exception e) {
//            e.printStackTrace();
//        } 
    }
}
